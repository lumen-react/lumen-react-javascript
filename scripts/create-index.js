let fs = require('fs');
let globSync = require('glob').sync;

const exportRegexp = new RegExp(/export(?: declare)?( default)? (?:function|abstract class|class|type|enum|interface|const) ([a-zA-Z0-9_]*)/g);
const faultyExport = new RegExp(/export \{/);

const getExports = () => {
  return globSync('./src/**/*.*')
    .filter(filename => filename.match(/\.tsx?$/) && filename != './src/index.ts')
    .map(filename => {
      const contents = fs.readFileSync(filename, 'utf8');

      if (contents.match(faultyExport)) {
        throw Error("Exports as objects is not supported in " + filename);
      }

      let matches = [];
      exportRegexp.lastIndex = 0;
      let match = exportRegexp.exec(contents);

      while (match) {
        matches.push(match);
        match = exportRegexp.exec(contents);
      }

      return {
        filename,
        matches: matches.map(match => match[1] ? `default as ${match[2]}` : match[2])
      };
    })
    .reduce((acc, match) => {
      if (!!match.matches.length) {
        acc[match.filename.replace('./src/', './').replace(/\.tsx?$/, '')] = match.matches;
      }
      else {
        throw Error("No exports found in " + match.filename);
      }
      return acc;
    }, {});
};

let exportMatches = getExports();
let exportStrings = [];

for (let filename in exportMatches) {
  let matches = exportMatches[filename];
  let modules = matches.join(', ');
  exportStrings.push(`export { ${modules} } from '${filename}'`);
}

fs.writeFileSync('./src/index.ts', exportStrings.join("\n"));
