"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
var redux_form_1 = require("redux-form");
var react_redux_1 = require("react-redux");
var connect_loader_1 = require("../hoc/connect-loader");
var ajax_loader_1 = require("../hoc/ajax-loader");
var router_utils_1 = require("../utils/router-utils");
var _ = require("lodash");
var object_utils_1 = require("../utils/object-utils");
var bootstrap_table_utils_1 = require("../utils/bootstrap-table-utils");
var package_utils_1 = require("../utils/package-utils");
var event_loader_1 = require("../hoc/event-loader");
function parseHeadersToApiResponseHeaders(headers) {
    var apiResponseHeaders = {};
    var regexp = new RegExp(/x-rslr-(.*)/i);
    for (var key in headers) {
        var matches = regexp.exec(key);
        if (matches) {
            apiResponseHeaders[matches[1]] = JSON.parse(headers[key]);
        }
    }
    return apiResponseHeaders;
}
exports.parseHeadersToApiResponseHeaders = parseHeadersToApiResponseHeaders;
function callDispatcher(dispatch, defaultAction, promiseCallback, options) {
    var promise = promiseCallback;
    var type = defaultAction;
    if (options) {
        if (options.type) {
            type = options.type;
        }
        if (options.then) {
            promise.then(options.then);
        }
        if (options.catch) {
            promise.catch(options.catch);
        }
        if (options.promise) {
            options.promise(promise);
        }
        if (options.empty) {
            dispatch({
                type: type,
                payload: options.emptyValue || null,
            });
        }
    }
    return dispatch({
        type: type,
        payload: promise,
    });
}
exports.callDispatcher = callDispatcher;
function getQueryString(queryString) {
    if (typeof queryString === 'string') {
        return queryString;
    }
    else {
        var qs = [];
        for (var key in queryString) {
            qs.push(key + "=" + queryString[key]);
        }
        return qs.join('&');
    }
}
exports.getQueryString = getQueryString;
function getUrl(base, requestConfig) {
    requestConfig = __assign({}, requestConfig);
    var qs = requestConfig && requestConfig.queryString ? requestConfig.queryString : {};
    if (requestConfig) {
        if (requestConfig.with) {
            if (typeof requestConfig.with !== 'string') {
                requestConfig.with = requestConfig.with.join(';');
            }
            if (typeof qs === 'string') {
                qs = qs + "&with=" + requestConfig.with;
            }
            else {
                qs.with = requestConfig.with;
            }
        }
        if (requestConfig.filter) {
            if (typeof requestConfig.filter !== 'string') {
                var filters = [];
                for (var i = 0; i < requestConfig.filter.length; i++) {
                    if (typeof requestConfig.filter[i] !== 'string') {
                        filters.push(requestConfig.filter[i].join(','));
                    }
                    else {
                        filters.push(requestConfig.filter[i]);
                    }
                }
                requestConfig.filter = filters.join(';');
            }
            if (typeof qs === 'string') {
                qs = qs + "&filter=" + requestConfig.filter;
            }
            else {
                qs.filter = requestConfig.filter;
            }
        }
        if (requestConfig.limit) {
            if (typeof qs === 'string') {
                qs = qs + "&limit=" + requestConfig.limit;
            }
            else {
                qs.limit = requestConfig.limit;
            }
        }
        if (requestConfig.offset) {
            if (typeof qs === 'string') {
                qs = qs + "&offset=" + requestConfig.offset;
            }
            else {
                qs.offset = requestConfig.offset;
            }
        }
        if (requestConfig.order) {
            if (typeof requestConfig.order !== 'string') {
                var order = [];
                for (var key in requestConfig.order) {
                    order.push(key + "," + requestConfig.order[key]);
                }
                requestConfig.order = order.join(';');
            }
            if (typeof qs === 'string') {
                qs = qs + "&order=" + requestConfig.order;
            }
            else {
                qs.order = requestConfig.order;
            }
        }
    }
    return !_.isEmpty(qs) ? base + "?" + getQueryString(qs) : base;
}
exports.getUrl = getUrl;
function writeRequestConfigToUrl(name, config) {
    var _a;
    router_utils_1.writeToHash((_a = {},
        _a[name] = _.pick(config, ['filter', 'limit', 'offset', 'order']),
        _a));
}
exports.writeRequestConfigToUrl = writeRequestConfigToUrl;
function readRequestConfigFromUrl(name, defaultConfig) {
    if (defaultConfig === void 0) { defaultConfig = null; }
    var object = router_utils_1.readFromHash(null, {});
    if (name in object) {
        return object[name];
    }
    if (defaultConfig) {
        writeRequestConfigToUrl(name, defaultConfig);
        return defaultConfig;
    }
}
exports.readRequestConfigFromUrl = readRequestConfigFromUrl;
exports.operatorConversion = (_a = {},
    _a[bootstrap_table_utils_1.TableFilterOperator.LIKE] = 'like',
    _a[bootstrap_table_utils_1.TableFilterOperator.EQ] = 'eq',
    _a[bootstrap_table_utils_1.TableFilterOperator.NE] = 'neq',
    _a[bootstrap_table_utils_1.TableFilterOperator.GT] = 'gt',
    _a[bootstrap_table_utils_1.TableFilterOperator.GE] = 'gte',
    _a[bootstrap_table_utils_1.TableFilterOperator.LT] = 'lt',
    _a[bootstrap_table_utils_1.TableFilterOperator.LE] = 'lte',
    _a);
function apiResponseHeadersToTableState(apiResponseHeaders) {
    if (!apiResponseHeaders) {
        apiResponseHeaders = {};
    }
    var sortField;
    var sortOrder;
    var filters = {};
    if ('order' in apiResponseHeaders) {
        for (var k in apiResponseHeaders.order) {
            sortField = k;
            sortOrder = apiResponseHeaders.order[k];
            break;
        }
    }
    if ('filter' in apiResponseHeaders) {
        for (var f in apiResponseHeaders.filter) {
            var ff = apiResponseHeaders.filter[f];
            filters[ff[0]] = { filterVal: ff[2], comparator: object_utils_1.swap(exports.operatorConversion)[ff[1]] };
        }
    }
    if (_.isEmpty(filters)) {
        filters = undefined;
    }
    return object_utils_1.removeUndefinedKeys({
        totalSize: apiResponseHeaders.count,
        page: apiResponseHeaders.limit ?
            Math.floor((apiResponseHeaders.offset ? apiResponseHeaders.offset : 0) / apiResponseHeaders.limit) + 1 :
            undefined,
        sizePerPage: apiResponseHeaders.limit,
        sortField: sortField,
        sortOrder: sortOrder,
        filters: filters,
    });
}
exports.apiResponseHeadersToTableState = apiResponseHeadersToTableState;
function requestConfigMerge(base) {
    var merges = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        merges[_i - 1] = arguments[_i];
    }
    var merge = merges.pop();
    var copy = __assign({}, base);
    if (merge) {
        if (copy.with && _.isArray(copy.with) && _.isArray(merge.with)) {
            copy.with = copy.with.concat(merge.with);
        }
        else if (merge.with) {
            copy.with = merge.with;
        }
        if (merge.filter && _.isArray(merge.filter)) {
            var filter_1 = [];
            merge.filter.forEach(function (f) {
                if (_.isArray(f)) {
                    filter_1.push([f[0], exports.operatorConversion[f[1]], f[2]]);
                }
                else {
                    filter_1.push(f);
                }
            });
            if (copy.filter && _.isArray(copy.filter)) {
                copy.filter = copy.filter.concat(filter_1);
            }
            else {
                copy.filter = filter_1;
            }
        }
        if (copy.queryString && _.isArray(copy.queryString) && _.isArray(merge.queryString)) {
            copy.queryString = copy.queryString.concat(merge.queryString);
        }
        else if (merge.queryString) {
            copy.queryString = merge.queryString;
        }
        if (merge.limit) {
            copy.limit = merge.limit;
        }
        if (merge.offset) {
            copy.offset = merge.offset;
        }
        if (merge.order) {
            copy.order = merge.order;
        }
    }
    if (merges.length > 0) {
        return requestConfigMerge.apply(void 0, [copy].concat(merges));
    }
    else {
        return copy;
    }
}
exports.requestConfigMerge = requestConfigMerge;
function apiResponseHeadersToRequestConfig(apiResponseHeaders, queryString) {
    if (queryString === void 0) { queryString = null; }
    return __assign({}, _.pick(apiResponseHeaders, 'with', 'filter', 'limit', 'offset', 'order'), { queryString: queryString });
}
exports.apiResponseHeadersToRequestConfig = apiResponseHeadersToRequestConfig;
function requestConfigToTableState(requestConfig) {
    if (!requestConfig) {
        requestConfig = {};
    }
    var sortField;
    var sortOrder;
    var filters = {};
    if ('order' in requestConfig) {
        for (var k in requestConfig.order) {
            sortField = k;
            sortOrder = requestConfig.order[k];
            break;
        }
    }
    if ('filter' in requestConfig) {
        for (var f in requestConfig.filter) {
            var ff = requestConfig.filter[f];
            filters[ff[0]] = { filterVal: ff[2], comparator: object_utils_1.swap(exports.operatorConversion)[ff[1]] };
        }
    }
    if (_.isEmpty(filters)) {
        filters = undefined;
    }
    return object_utils_1.removeUndefinedKeys({
        page: 'limit' in requestConfig ?
            Math.floor(('offset' in requestConfig ? requestConfig.offset : 0) / requestConfig.limit) + 1 :
            undefined,
        sizePerPage: 'limit' in requestConfig ? requestConfig.limit : undefined,
        sortField: sortField,
        sortOrder: sortOrder,
        filters: filters,
    });
}
exports.requestConfigToTableState = requestConfigToTableState;
function tableStateToRequestConfig(tableState, queryString, cancelToken) {
    var _a;
    if (queryString === void 0) { queryString = undefined; }
    if (cancelToken === void 0) { cancelToken = undefined; }
    if (!tableState) {
        tableState = {};
    }
    var f = [];
    if ('filters' in tableState) {
        for (var fk in tableState.filters) {
            var push = null;
            if (_.isObject(tableState.filters[fk].filterVal)) {
                push = [
                    fk,
                    exports.operatorConversion[tableState.filters[fk].filterVal.comparator],
                    'date' in tableState.filters[fk].filterVal ? tableState.filters[fk].filterVal.date :
                        tableState.filters[fk].filterVal.number,
                ];
            }
            else {
                push = [
                    fk,
                    exports.operatorConversion[tableState.filters[fk].comparator],
                    tableState.filters[fk].filterVal,
                ];
            }
            if (push[2] != '') {
                f.push(push);
            }
        }
    }
    if (_.isEmpty(f)) {
        f = undefined;
    }
    return object_utils_1.removeUndefinedKeys({
        offset: tableState.page ? tableState.sizePerPage * (tableState.page - 1) : undefined,
        limit: tableState.sizePerPage,
        filter: f,
        order: (tableState.sortField && tableState.sortOrder) ? (_a = {}, _a[tableState.sortField] = tableState.sortOrder, _a) :
            undefined,
        cancelToken: cancelToken,
        queryString: queryString,
    });
}
exports.tableStateToRequestConfig = tableStateToRequestConfig;
function connector(component, connectorOptions) {
    if (connectorOptions === void 0) { connectorOptions = {}; }
    var connector = component;
    var formConfigProps = connectorOptions.formConfigProps, mapStateToProps = connectorOptions.mapStateToProps, mapDispatchToProps = connectorOptions.mapDispatchToProps, connectLoaderOptions = connectorOptions.connectLoaderOptions, ajaxLoaderOptions = connectorOptions.ajaxLoaderOptions, eventLoaderOptions = connectorOptions.eventLoaderOptions, mapOptions = connectorOptions.mapOptions;
    if (formConfigProps) {
        connector = redux_form_1.reduxForm(formConfigProps)(connector);
    }
    if (connectLoaderOptions) {
        connector = connect_loader_1.connectLoader(connectLoaderOptions)(connector);
    }
    if (mapStateToProps || mapDispatchToProps) {
        connector = react_redux_1.connect(mapStateToProps, mapDispatchToProps, undefined, mapOptions)(connector);
    }
    if (ajaxLoaderOptions) {
        connector = ajax_loader_1.ajaxLoader(ajaxLoaderOptions)(connector);
    }
    if (eventLoaderOptions) {
        connector = event_loader_1.eventLoader(eventLoaderOptions)(connector);
    }
    return connector;
}
exports.connector = connector;
function connect(component, mapStateToProps, mapDispatchToProps, dispatcherLoaders, ajaxLoaderPromises) {
    if (mapStateToProps === void 0) { mapStateToProps = null; }
    if (mapDispatchToProps === void 0) { mapDispatchToProps = null; }
    if (dispatcherLoaders === void 0) { dispatcherLoaders = null; }
    if (ajaxLoaderPromises === void 0) { ajaxLoaderPromises = null; }
    package_utils_1.deprecated('use connector instead');
    return (ajax_loader_1.ajaxLoader(ajaxLoaderPromises)(react_redux_1.connect(mapStateToProps, mapDispatchToProps)(connect_loader_1.connectLoader({ dispatchers: dispatcherLoaders })(component))));
}
exports.connect = connect;
function form(component, formConfigProps, mapStateToProps, mapDispatchToProps, dispatcherLoaders, ajaxLoaderPromises) {
    if (mapStateToProps === void 0) { mapStateToProps = null; }
    if (mapDispatchToProps === void 0) { mapDispatchToProps = null; }
    if (dispatcherLoaders === void 0) { dispatcherLoaders = null; }
    if (ajaxLoaderPromises === void 0) { ajaxLoaderPromises = null; }
    package_utils_1.deprecated('use connector instead');
    return (ajax_loader_1.ajaxLoader(ajaxLoaderPromises)(react_redux_1.connect(mapStateToProps, mapDispatchToProps)(connect_loader_1.connectLoader({ dispatchers: dispatcherLoaders })(redux_form_1.reduxForm(formConfigProps)(component)))));
}
exports.form = form;
function simpleForm(component, formConfigProps, ajaxLoaderPromises) {
    if (ajaxLoaderPromises === void 0) { ajaxLoaderPromises = null; }
    package_utils_1.deprecated('use connector instead');
    return (ajax_loader_1.ajaxLoader(ajaxLoaderPromises)(redux_form_1.reduxForm(formConfigProps)(component)));
}
exports.simpleForm = simpleForm;
function simpleLoader(component, ajaxLoaderPromises) {
    if (ajaxLoaderPromises === void 0) { ajaxLoaderPromises = null; }
    package_utils_1.deprecated('use connector instead');
    return ajax_loader_1.ajaxLoader(ajaxLoaderPromises)(component);
}
exports.simpleLoader = simpleLoader;
//# sourceMappingURL=type-definitions-helpers.js.map