"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
var Analytics = (function (_super) {
    __extends(Analytics, _super);
    function Analytics(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.sendPageChange(props.location.pathname, props.location.search);
        return _this;
    }
    Analytics.prototype.componentWillReceiveProps = function (nextProps) {
        if (this.props.location.pathname !== nextProps.location.pathname
            || this.props.location.search !== nextProps.location.search) {
            this.sendPageChange(nextProps.location.pathname, nextProps.location.search);
        }
    };
    Analytics.prototype.sendPageChange = function (pathname, search) {
        if (search === void 0) { search = ''; }
        var page = pathname + search;
        this.props.reactGa.set({ page: page });
        this.props.reactGa.pageview(page);
    };
    Analytics.prototype.render = function () {
        return null;
    };
    return Analytics;
}(__1.AbstractStatelessComponent));
exports.default = Analytics;
//# sourceMappingURL=analytics.js.map