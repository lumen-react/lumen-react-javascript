"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var react_hot_loader_1 = require("react-hot-loader");
var react_redux_1 = require("react-redux");
var ReduxPromise = require("redux-promise");
var redux_1 = require("redux");
var react_intl_1 = require("react-intl");
var axios_1 = require("axios");
var _ = require("lodash");
var auth_utils_1 = require("./auth-utils");
var empty_1 = require("../components/empty");
var window_utils_1 = require("./window-utils");
var storage_utils_1 = require("./storage-utils");
function run(Entry, options) {
    if (options === void 0) { options = {}; }
    options = __assign({
        appName: 'web',
        className: null,
        languages: { supported: { en: ['en'] }, default: 'en' },
        reducers: [],
        module: null,
        App: null,
        postBoot: null,
        ErrorBoundary: empty_1.default,
        globalLoader: '...',
        preRenderCallback: null,
        postRenderCallback: null,
        hotLoaderPath: './components/app',
        defaultStorageDriverId: storage_utils_1.StorageDriverId.LOCAL_STORAGE,
    }, options);
    var appName = options.appName, className = options.className, module = options.module, reducers = options.reducers, languages = options.languages, localeCallback = options.localeCallback, App = options.App, postBoot = options.postBoot, ErrorBoundary = options.ErrorBoundary, globalLoader = options.globalLoader, preRenderCallback = options.preRenderCallback, postRenderCallback = options.postRenderCallback, hotLoaderPath = options.hotLoaderPath, defaultStorageDriverId = options.defaultStorageDriverId, authStorageDriverId = options.authStorageDriverId;
    var runWithLocale = function (locale) {
        if (postBoot) {
            postBoot(locale);
        }
        var localeExists = false;
        for (var language in languages.supported) {
            if (_.includes(languages.supported[language], locale)) {
                localeExists = true;
                locale = language;
                break;
            }
        }
        locale = localeExists ? locale : languages.default;
        var createStoreWithMiddleware = redux_1.applyMiddleware(ReduxPromise)(redux_1.createStore);
        var axiosStaticInstance = axios_1.default.create({ baseURL: '/' });
        var render = function () {
            axiosStaticInstance.get("/locale/" + locale + ".json").then(function (data) {
                var dom = document.getElementById('root');
                dom.className = (dom.className + (" app-" + appName) + (className ? " " + className : '')).trim();
                preRenderCallback && preRenderCallback();
                ReactDOM.render(React.createElement(react_hot_loader_1.AppContainer, null,
                    React.createElement(ErrorBoundary, null,
                        React.createElement(react_intl_1.IntlProvider, { locale: locale, messages: data.data },
                            React.createElement(react_redux_1.Provider, { store: createStoreWithMiddleware(reducers) },
                                React.createElement(App, null,
                                    React.createElement(Entry, null)))))), dom);
                postRenderCallback && postRenderCallback();
            });
        };
        Promise.resolve().then(function () { return require("react-intl/locale-data/" + locale); }).then(function (data) {
            react_intl_1.addLocaleData(data);
            render();
        });
        if (module && module.hot) {
            module.hot.accept(hotLoaderPath, function () { return render(); });
        }
    };
    storage_utils_1.default.defaultStorageDriverId = defaultStorageDriverId;
    window_utils_1.setGlobal('globalLoader', globalLoader);
    auth_utils_1.default.boot(authStorageDriverId || defaultStorageDriverId);
    var locale = languages.default;
    if (localeCallback) {
        if (typeof localeCallback === 'string') {
            runWithLocale(localeCallback);
        }
        else {
            localeCallback.then(function (l) { return runWithLocale(l); });
        }
    }
    else {
        runWithLocale(locale);
    }
}
exports.run = run;
function resolveLoader(content) {
    var loader = window_utils_1.getGlobal('globalLoader');
    if (loader) {
        if (_.isFunction(loader)) {
            return loader(content);
        }
        else {
            return loader;
        }
    }
    else {
        return null;
    }
}
exports.resolveLoader = resolveLoader;
//# sourceMappingURL=boot-utils.js.map