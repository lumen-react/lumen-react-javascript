"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var event_utils_1 = require("./event-utils");
exports.ON_KEY_DOWN = 'onKeyDown';
exports.ON_KEY_UP = 'onKeyUp';
function getWindow() {
    return window.rs;
}
function setWindow(properties) {
    if (!window.rs) {
        window.rs = {};
    }
    for (var key in properties) {
        window.rs[key] = properties[key];
    }
}
function setGlobal(key, value) {
    var _a;
    setWindow((_a = {}, _a[key] = value, _a));
}
exports.setGlobal = setGlobal;
function getGlobal(key, defaultValue) {
    if (key === void 0) { key = null; }
    if (defaultValue === void 0) { defaultValue = null; }
    var windowObject = getWindow();
    if (windowObject) {
        if (key) {
            return key in windowObject ? windowObject[key] : defaultValue;
        }
        else {
            return windowObject;
        }
    }
    else {
        return defaultValue;
    }
}
exports.getGlobal = getGlobal;
exports.SHIFT_KEY = -1;
exports.ALT_KEY = -2;
exports.CTRL_KEY = -3;
exports.META_KEY = -4;
exports.OPT_KEY = -5;
function isKeyDown(key) {
    return getKeyRegister(key);
}
exports.isKeyDown = isKeyDown;
function setupKeyRegister() {
    window.onkeydown = function (e) {
        registerKey(e, true);
        event_utils_1.dispatch(exports.ON_KEY_DOWN, { key: e.key, shift: e.shiftKey, alt: e.altKey, ctrl: e.ctrlKey, meta: e.metaKey });
    };
    window.onkeyup = function (e) {
        registerKey(e, false);
        event_utils_1.dispatch(exports.ON_KEY_UP, { key: e.key, shift: e.shiftKey, alt: e.altKey, ctrl: e.ctrlKey, meta: e.metaKey });
    };
}
exports.setupKeyRegister = setupKeyRegister;
function getKeyRegister(key) {
    var register = getGlobal('keyRegister', {});
    return key in register ? register[key] : false;
}
exports.getKeyRegister = getKeyRegister;
function setKeyRegister(key, value) {
    var register = getGlobal('keyRegister', {});
    register[key] = value;
    setGlobal('keyRegister', register);
}
exports.setKeyRegister = setKeyRegister;
function registerKey(e, down) {
    if (!e) {
        e = window.event;
    }
    setKeyRegister(exports.SHIFT_KEY, e.shiftKey);
    setKeyRegister(exports.ALT_KEY, e.altKey);
    setKeyRegister(exports.CTRL_KEY, e.ctrlKey);
    setKeyRegister(exports.OPT_KEY, e.ctrlKey);
    setKeyRegister(exports.META_KEY, e.metaKey);
    setKeyRegister(exports.OPT_KEY, e.metaKey);
    if (e.key) {
        setKeyRegister(e.key, down);
    }
}
exports.registerKey = registerKey;
//# sourceMappingURL=window-utils.js.map