"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CssUtils = (function () {
    function CssUtils() {
    }
    CssUtils.get = function (selector) {
        if (CssUtils.cached[selector]) {
            return CssUtils.cached[selector];
        }
        for (var i = 0; i < document.styleSheets.length; i++) {
            try {
                var styleSheet = document.styleSheets[i];
                for (var j = 0; j < styleSheet.cssRules.length; j++) {
                    var cssRules = styleSheet.cssRules[j];
                    if (cssRules.selectorText == selector) {
                        var theRules = {};
                        for (var k = 0; k < cssRules.style.length; k++) {
                            theRules[cssRules.style[k]] = cssRules.style[cssRules.style[k]];
                        }
                        CssUtils.cached[selector] = theRules;
                        return theRules;
                    }
                }
            }
            catch (_a) {
            }
        }
        return null;
    };
    CssUtils.set = function (selector, rule) {
        if (CssUtils.cached[selector]) {
            delete CssUtils.cached[selector];
        }
        for (var i = 0; i < document.styleSheets.length; i++) {
            try {
                var styleSheet = document.styleSheets[i];
                for (var j = 0; j < styleSheet.cssRules.length; j++) {
                    var cssRules = styleSheet.cssRules[j];
                    if (cssRules.selectorText == selector) {
                        styleSheet.insertRule(selector + ' { ' + rule + ' }', 0);
                    }
                }
            }
            catch (_a) {
            }
        }
        console.warn("Warning: CSS " + selector + " not found");
    };
    CssUtils.rgb2hex = function (rgb) {
        var r = rgb.match(/^\s*rgb\((\d+)\s*(\d+)\s*(\d+)\s*\)\s*,$/i);
        if (r) {
            rgb = parseInt(r[1]) | (parseInt(r[2]) << 8) | (parseInt(r[3]) << 16);
            return '#' + (0x1000000 + rgb).toString(16).slice(1);
        }
        else {
            return rgb;
        }
    };
    CssUtils.hex2rgb = function (hex) {
        var r = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
        if (r) {
            return r.slice(1, 4).map(function (x) {
                return parseInt(x, 16);
            });
        }
        r = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
        if (r) {
            return r.slice(1, 4).map(function (x) {
                return 0x11 * parseInt(x, 16);
            });
        }
        return hex;
    };
    CssUtils.cached = {};
    return CssUtils;
}());
exports.default = CssUtils;
//# sourceMappingURL=css-utils.js.map