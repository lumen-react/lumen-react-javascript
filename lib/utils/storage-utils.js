"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Cookie = require("js-cookie");
var window_utils_1 = require("./window-utils");
var StorageDriverId;
(function (StorageDriverId) {
    StorageDriverId[StorageDriverId["COOKIES"] = 0] = "COOKIES";
    StorageDriverId[StorageDriverId["LOCAL_STORAGE"] = 1] = "LOCAL_STORAGE";
    StorageDriverId[StorageDriverId["SESSION_STORAGE"] = 2] = "SESSION_STORAGE";
    StorageDriverId[StorageDriverId["WINDOW"] = 3] = "WINDOW";
})(StorageDriverId = exports.StorageDriverId || (exports.StorageDriverId = {}));
var CookieDriver = (function () {
    function CookieDriver() {
        this.id = StorageDriverId.COOKIES;
    }
    CookieDriver.prototype.set = function (key, value, options) {
        Cookie.set(key, value, options);
        return true;
    };
    CookieDriver.prototype.get = function (key) {
        return Cookie.getJSON(key);
    };
    CookieDriver.prototype.remove = function (key, options) {
        Cookie.remove(key, options);
        return true;
    };
    CookieDriver.prototype.clear = function () {
        console.warn("Cookies does not support clear storage");
        return true;
    };
    CookieDriver.prototype.all = function () {
        console.warn("Cookies does not support fetch all");
        return {};
    };
    return CookieDriver;
}());
var LocalStorageDriver = (function () {
    function LocalStorageDriver() {
        this.id = StorageDriverId.LOCAL_STORAGE;
    }
    LocalStorageDriver.prototype.set = function (key, value, options) {
        localStorage.setItem(key, JSON.stringify(value));
        return true;
    };
    LocalStorageDriver.prototype.get = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };
    LocalStorageDriver.prototype.remove = function (key, options) {
        localStorage.removeItem(key);
        return true;
    };
    LocalStorageDriver.prototype.clear = function () {
        localStorage.clear();
        return true;
    };
    LocalStorageDriver.prototype.all = function () {
        var all = {};
        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            all[key] = this.get(key);
        }
        return all;
    };
    return LocalStorageDriver;
}());
var SessionStorageDriver = (function () {
    function SessionStorageDriver() {
        this.id = StorageDriverId.SESSION_STORAGE;
    }
    SessionStorageDriver.prototype.set = function (key, value, options) {
        sessionStorage.setItem(key, JSON.stringify(value));
        return true;
    };
    SessionStorageDriver.prototype.get = function (key) {
        return JSON.parse(sessionStorage.getItem(key));
    };
    SessionStorageDriver.prototype.remove = function (key, options) {
        sessionStorage.removeItem(key);
        return true;
    };
    SessionStorageDriver.prototype.clear = function () {
        sessionStorage.clear();
        return true;
    };
    SessionStorageDriver.prototype.all = function () {
        var all = {};
        for (var i = 0; i < sessionStorage.length; i++) {
            var key = sessionStorage.key(i);
            all[key] = this.get(key);
        }
        return all;
    };
    return SessionStorageDriver;
}());
var WindowDriver = (function () {
    function WindowDriver() {
        this.id = StorageDriverId.SESSION_STORAGE;
    }
    WindowDriver.prototype.set = function (key, value, options) {
        var currentStorage = JSON.parse(window_utils_1.getGlobal('storage')) || {};
        currentStorage[key] = value;
        window_utils_1.setGlobal('storage', JSON.stringify(currentStorage));
        return true;
    };
    WindowDriver.prototype.get = function (key) {
        var currentStorage = JSON.parse(window_utils_1.getGlobal('storage')) || {};
        return currentStorage[key];
    };
    WindowDriver.prototype.remove = function (key, options) {
        var currentStorage = JSON.parse(window_utils_1.getGlobal('storage')) || {};
        delete currentStorage[key];
        window_utils_1.setGlobal('storage', JSON.stringify(currentStorage));
        return true;
    };
    WindowDriver.prototype.clear = function () {
        window_utils_1.setGlobal('storage', {});
        return true;
    };
    WindowDriver.prototype.all = function () {
        return JSON.parse(window_utils_1.getGlobal('storage')) || {};
    };
    return WindowDriver;
}());
var cookieDriver = new CookieDriver();
var localStorageDriver = new LocalStorageDriver();
var sessionStorageDriver = new SessionStorageDriver();
var windowDriver = new WindowDriver();
var StorageUtils = (function () {
    function StorageUtils() {
    }
    StorageUtils.getStorageDriver = function (driverId) {
        switch (driverId) {
            case StorageDriverId.COOKIES:
                return cookieDriver;
            case StorageDriverId.LOCAL_STORAGE:
                return localStorageDriver;
            case StorageDriverId.SESSION_STORAGE:
                return sessionStorageDriver;
            case StorageDriverId.WINDOW:
                return windowDriver;
            default:
                return StorageUtils.getStorageDriver(StorageUtils.defaultStorageDriverId);
        }
    };
    StorageUtils.setWithOptions = function (key, value, options, driverId) {
        return StorageUtils.getStorageDriver(driverId).set(key, value, options);
    };
    StorageUtils.set = function (key, value, driverId) {
        return StorageUtils.getStorageDriver(driverId).set(key, value);
    };
    StorageUtils.get = function (key, driverId) {
        return StorageUtils.getStorageDriver(driverId).get(key);
    };
    StorageUtils.removeWithOptions = function (key, options, driverId) {
        return StorageUtils.getStorageDriver(driverId).remove(key, options);
    };
    StorageUtils.remove = function (key, driverId) {
        return StorageUtils.getStorageDriver(driverId).remove(key);
    };
    StorageUtils.clear = function (driverId) {
        return StorageUtils.getStorageDriver(driverId).clear();
    };
    StorageUtils.all = function (driverId) {
        return StorageUtils.getStorageDriver(driverId).all();
    };
    StorageUtils.defaultStorageDriverId = StorageDriverId.LOCAL_STORAGE;
    return StorageUtils;
}());
exports.default = StorageUtils;
//# sourceMappingURL=storage-utils.js.map