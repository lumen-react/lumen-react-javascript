"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_bootstrap_table2_filter_1 = require("react-bootstrap-table2-filter");
var react_bootstrap_table2_paginator_1 = require("react-bootstrap-table2-paginator");
var _ = require("lodash");
var router_utils_1 = require("./router-utils");
var __1 = require("..");
var window_utils_1 = require("./window-utils");
var EMPTY_FILTER_VALUE = 'EMPTY_FILTER_VALUE';
exports.paginationOptions = {
    paginationSize: 5,
    sizePerPage: 25,
    pageStartIndex: 1,
    alwaysShowAllBtns: true,
    withFirstAndLast: true,
    hideSizePerPage: false,
    hidePageListOnlyOnePage: true,
    firstPageText: 'First',
    prePageText: 'Back',
    nextPageText: 'Next',
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
};
function messagesToColumns(messages, intl, keyMap) {
    var columns = [];
    for (var i in messages) {
        var message = messages[i];
        var column = {
            dataField: (keyMap && typeof keyMap[message.id] === 'string') ? keyMap[message.id] : message.id,
            text: intl.formatMessage(message),
        };
        if (keyMap && typeof keyMap[message.id] !== 'string') {
            column.formatter = keyMap[message.id];
        }
        columns.push(column);
    }
    return columns;
}
exports.messagesToColumns = messagesToColumns;
var TableDefinitionType;
(function (TableDefinitionType) {
    TableDefinitionType["TEXT"] = "TEXT";
    TableDefinitionType["SELECT"] = "SELECT";
    TableDefinitionType["NUMBER"] = "NUMBER";
    TableDefinitionType["DATE"] = "DATE";
})(TableDefinitionType = exports.TableDefinitionType || (exports.TableDefinitionType = {}));
var TableFilterOperator;
(function (TableFilterOperator) {
    TableFilterOperator["LIKE"] = "LIKE";
    TableFilterOperator["EQ"] = "=";
    TableFilterOperator["NE"] = "!=";
    TableFilterOperator["GT"] = ">";
    TableFilterOperator["GE"] = ">=";
    TableFilterOperator["LT"] = "<";
    TableFilterOperator["LE"] = "<=";
})(TableFilterOperator = exports.TableFilterOperator || (exports.TableFilterOperator = {}));
function tableColumnSelectArrayToObject(array, value, key) {
    if (value === void 0) { value = 'name'; }
    if (key === void 0) { key = 'id'; }
    var object = {};
    for (var i = 0; i < array.length; i++) {
        object[array[i][key]] = array[i][value];
    }
    return object;
}
exports.tableColumnSelectArrayToObject = tableColumnSelectArrayToObject;
function createTableProps(data, tableDefinition, messages, intl, useWrapper) {
    if (useWrapper === void 0) { useWrapper = true; }
    var defaultOptions = {
        rowDefinitions: [],
        state: {},
        remote: false,
        keyField: 'id',
        paginationOptions: {
            paginationSize: 5,
            pageStartIndex: 1,
            alwaysShowAllBtns: true,
            withFirstAndLast: true,
            hideSizePerPage: false,
            hidePageListOnlyOnePage: true,
            firstPageText: 'first' in messages ? intl.formatMessage(messages.first) : 'First',
            prePageText: 'previous' in messages ? intl.formatMessage(messages.previous) : 'Previous',
            nextPageText: 'next' in messages ? intl.formatMessage(messages.next) : 'Next',
            lastPageText: 'last' in messages ? intl.formatMessage(messages.last) : 'Last',
            nextPageTitle: 'next' in messages ? intl.formatMessage(messages.next) : 'Next',
            prePageTitle: 'previous' in messages ? intl.formatMessage(messages.previous) : 'Previous',
            firstPageTitle: 'first' in messages ? intl.formatMessage(messages.first) : 'First',
            lastPageTitle: 'last' in messages ? intl.formatMessage(messages.last) : 'Last',
            showTotal: true,
            paginationTotalRenderer: function (from, to, size) { return (React.createElement("div", { className: "react-bootstrap-table-pagination-footer" }, (tableDefinition.paginationOptions && tableDefinition.paginationOptions.objectPluralMessageKey) ?
                ('tableShowingFooterWithObject' in messages ?
                    intl.formatMessage(messages.tableShowingFooterWithObject, {
                        from: from, to: to + 1, size: size,
                        objects: tableDefinition.paginationOptions.objectPluralMessageKey
                    }) : "Showing " + from + " to " + (to +
                    1) + " of " + size + " " + tableDefinition.paginationOptions.objectPluralMessageKey)
                :
                    ('tableShowingFooter' in messages ? intl.formatMessage(messages.tableShowingFooter, {
                        from: from, to: to + 1, size: size,
                    }) : "Showing " + from + " to " + (to + 1) + " of " + size + " results"))); },
            sizePerPageList: getDefaultSizePerPageList(),
        },
        dateFormatter: window_utils_1.getGlobal('defaultTableDateFormatter', function (date) { return date.toISOString(); }),
    };
    var options = __assign({}, defaultOptions, tableDefinition);
    var columns = [];
    for (var i = 0; i < options.rowDefinitions.length; i++) {
        columns.push(getRowOptions(options, options.rowDefinitions[i], messages, intl, !options.remote, useWrapper));
    }
    var paginationOptions = __assign({}, options.paginationOptions);
    var defaultSorted = [];
    if (options.state.totalSize) {
        paginationOptions.totalSize = options.state.totalSize;
    }
    if (options.state.page) {
        paginationOptions.page = options.state.page;
    }
    if (options.state.sizePerPage) {
        paginationOptions.sizePerPage = options.state.sizePerPage;
    }
    if (options.state.sortField) {
        var dataField = columns.find(function (c) { return c.id == options.state.sortField; }).dataField;
        var order = options.state.sortOrder ? options.state.sortOrder : 'desc';
        defaultSorted = [{ dataField: dataField, order: order }];
    }
    if (options.remote) {
        return {
            state: options.state,
            data: data,
            filter: react_bootstrap_table2_filter_1.default(),
            pagination: react_bootstrap_table2_paginator_1.default(paginationOptions),
            keyField: options.keyField,
            columns: columns,
            defaultSorted: defaultSorted,
            remote: {
                filter: true,
                pagination: true,
                sort: true,
                cellEdit: true,
            },
            onTableChange: options.onTableChange,
        };
    }
    else {
        var onPaginationChange = function (onTableChange) {
            return function (page, sizePerPage) {
                if (onTableChange) {
                    onTableChange('pagination', {
                        page: page,
                        sizePerPage: sizePerPage,
                    });
                }
            };
        };
        var onFilterChange = function (onTableChange) {
            return function (ref) {
                var _loop_1 = function (i) {
                    var dom = document.getElementsByClassName(options.id + "-" + columns[i].id)
                        .item(0);
                    dom.onchange = dom.onkeyup = dom.onblur =
                        function (e) {
                            var _a;
                            if (onTableChange) {
                                var filterVal = e.target.value;
                                var comparator = null;
                                var potentialNumberComparator = dom.getElementsByClassName('number-filter-comparator').item(0);
                                if (potentialNumberComparator) {
                                    filterVal =
                                        dom.getElementsByClassName('number-filter-input').item(0).value;
                                    comparator = potentialNumberComparator.value;
                                }
                                else {
                                    var potentialDateComparator = dom.getElementsByClassName('date-filter-comparator').item(0);
                                    if (potentialDateComparator) {
                                        filterVal =
                                            dom.getElementsByClassName('date-filter-input').item(0).value;
                                        comparator = potentialDateComparator.value;
                                    }
                                    else {
                                        if (dom.classList.contains('text-filter')) {
                                            comparator = TableFilterOperator.LIKE;
                                        }
                                        else {
                                            comparator = TableFilterOperator.EQ;
                                        }
                                    }
                                }
                                onTableChange('filter', {
                                    filters: (_a = {},
                                        _a[columns[i].id] = (filterVal != '' && comparator != '') ? {
                                            filterVal: filterVal,
                                            comparator: comparator,
                                        } : null,
                                        _a)
                                });
                            }
                        };
                };
                for (var i = 0; i < columns.length; i++) {
                    _loop_1(i);
                }
            };
        };
        if (useWrapper) {
            return {
                state: options.state,
                data: data,
                filter: react_bootstrap_table2_filter_1.default(),
                paginationOptions: paginationOptions,
                keyField: options.keyField,
                columns: columns,
                defaultSorted: defaultSorted,
                onPaginationChange: onPaginationChange,
                onFilterChange: onFilterChange,
                onTableChange: options.onTableChange,
            };
        }
        else {
            paginationOptions.onPageChange = onPaginationChange(options.onTableChange);
            paginationOptions.onSizePerPageChange = onPaginationChange(options.onTableChange);
            return {
                state: options.state,
                data: data,
                filter: react_bootstrap_table2_filter_1.default(),
                pagination: react_bootstrap_table2_paginator_1.default(paginationOptions),
                keyField: options.keyField,
                columns: columns,
                defaultSorted: defaultSorted,
                ref: onFilterChange(options.onTableChange),
            };
        }
    }
}
exports.createTableProps = createTableProps;
function getRowOptions(tableDefinition, rowDefinition, messages, intl, isLocal, useWrapper) {
    var onSortChange = function (onTableChange) {
        return function (sortField, sortOrder) {
            if (rowDefinition.onSort) {
                rowDefinition.onSort(sortField, sortOrder);
            }
            if (onTableChange) {
                onTableChange('sort', {
                    sortField: sortField,
                    sortOrder: sortOrder,
                });
            }
        };
    };
    var ret = __assign({
        text: rowDefinition.id in messages ? intl.formatMessage(messages[rowDefinition.id]) : rowDefinition.id,
        dataField: rowDefinition.id,
        type: TableDefinitionType.TEXT,
        sort: isLocal,
        onSort: isLocal ? (useWrapper ? onSortChange : onSortChange(tableDefinition.onTableChange)) :
            rowDefinition.onSort,
    }, _.omit(rowDefinition, 'filterValue', 'filterOptions', 'onSort', 'formatter'));
    ret.filter = ret.filter ? ret.filter :
        defaultFilterTypeObjectCreator(tableDefinition, ret, rowDefinition.filterOptions, messages, intl, isLocal);
    ret.formatter = function (cell, row, rowIndex, extraData) {
        var r = rowDefinition.formatter ? rowDefinition.formatter(cell, row, rowIndex, extraData) : cell;
        if (rowDefinition.filterOptions && rowDefinition.filterOptions.translate &&
            (!('skipFormatterTranslate' in rowDefinition.filterOptions) ||
                !rowDefinition.filterOptions.skipFormatterTranslate)) {
            r = r in messages ? intl.formatMessage(messages[r]) : r;
        }
        if (!rowDefinition.formatter && ret.type == TableDefinitionType.DATE) {
            r = tableDefinition.dateFormatter(_.isDate(r) ? r : new Date(r));
        }
        return r;
    };
    ret.filterValue = function (cell, row) {
        var r = rowDefinition.filterValue ? rowDefinition.filterValue(cell, row) : cell;
        if (ret.type == TableDefinitionType.DATE) {
            var d = _.isDate(r) ? r : new Date(r);
            r = (d.getTime() / 1000).toFixed(0).toString();
        }
        if (isLocal && _.isBoolean(r)) {
            r = r ? 1 : 0;
        }
        return r;
    };
    return ret;
}
exports.getRowOptions = getRowOptions;
function defaultFilterTypeObjectCreator(tableDefinition, rowDefinition, filterOptions, messages, intl, isLocal) {
    var _a;
    if (!rowDefinition.type && !isLocal) {
        return undefined;
    }
    var placeholder = rowDefinition.id in messages ? intl.formatMessage(messages[rowDefinition.id]) : rowDefinition.id;
    var filter = tableDefinition.state && tableDefinition.state.filters && rowDefinition.dataField in
        tableDefinition.state.filters ? tableDefinition.state.filters[rowDefinition.dataField] : null;
    var defaultValue = filter ? filter.filterVal : undefined;
    var comparator = filter ? filter.comparator : undefined;
    if (_.isBoolean(defaultValue)) {
        defaultValue = defaultValue ? 1 : 0;
    }
    switch (rowDefinition.type) {
        case TableDefinitionType.NUMBER:
            return react_bootstrap_table2_filter_1.numberFilter(__assign({
                placeholder: placeholder,
                defaultValue: defaultValue ? { number: defaultValue, comparator: comparator } :
                    { comparator: TableFilterOperator.EQ },
                className: tableDefinition.id + "-" + rowDefinition.id,
            }, filterOptions));
        case TableDefinitionType.SELECT:
            if (filterOptions.translate) {
                var translated = {};
                filterOptions = __assign({}, filterOptions);
                for (var key in filterOptions.options) {
                    translated[key] = filterOptions.options[key] in messages ?
                        intl.formatMessage(messages[filterOptions.options[key]]) : filterOptions.options[key];
                }
                filterOptions.options = translated;
            }
            if (!isLocal) {
                filterOptions.options = __assign((_a = {}, _a[EMPTY_FILTER_VALUE] = placeholder, _a), filterOptions.options);
                defaultValue = defaultValue || EMPTY_FILTER_VALUE;
            }
            return react_bootstrap_table2_filter_1.selectFilter(__assign({
                withoutEmptyOption: !isLocal,
                placeholder: placeholder,
                defaultValue: defaultValue,
                className: tableDefinition.id + "-" + rowDefinition.id,
            }, _.omit(filterOptions, 'translate')));
        case TableDefinitionType.DATE:
            return react_bootstrap_table2_filter_1.dateFilter(__assign({
                placeholder: placeholder,
                defaultValue: defaultValue ?
                    {
                        date: _.isDate(defaultValue) ? defaultValue :
                            new Date(/^[0-9]+$/.test(defaultValue) ? defaultValue * 1000 : defaultValue),
                        comparator: comparator ? comparator : TableFilterOperator.GT,
                    } : { comparator: TableFilterOperator.GT },
                className: tableDefinition.id + "-" + rowDefinition.id,
            }, filterOptions));
        default:
            return react_bootstrap_table2_filter_1.textFilter(__assign({
                placeholder: placeholder,
                defaultValue: defaultValue,
                className: tableDefinition.id + "-" + rowDefinition.id,
            }, filterOptions));
    }
}
exports.defaultFilterTypeObjectCreator = defaultFilterTypeObjectCreator;
function mergeTableState(oldState, newState, remote) {
    var filters = __assign({}, oldState.filters, newState.filters);
    for (var key in filters) {
        if (filters[key] === null || (remote && !(key in newState.filters)) ||
            ((_.isObject(filters[key].filterVal) && 'comparator' in filters[key].filterVal &&
                (filters[key].filterVal.comparator == '' ||
                    (('date' in filters[key].filterVal && filters[key].filterVal.date === EMPTY_FILTER_VALUE) ||
                        ('number' in filters[key].filterVal && filters[key].filterVal.number === EMPTY_FILTER_VALUE))))) || (_.isString(filters[key].filterVal) && filters[key].filterVal === EMPTY_FILTER_VALUE)) {
            delete filters[key];
        }
        else {
            if (_.isObject(filters[key].filterVal) && 'date' in filters[key].filterVal &&
                _.isDate(filters[key].filterVal.date)) {
                filters[key].filterVal.date = filters[key].filterVal.date.toISOString();
            }
        }
    }
    return __1.removeUndefinedKeys(__assign({}, oldState, newState, { filters: _.isEmpty(filters) ? undefined : filters }));
}
exports.mergeTableState = mergeTableState;
function writeTableStateToUrl(name, tableState) {
    var _a;
    router_utils_1.writeToHash((_a = {},
        _a[name] = _.omit(tableState, 'totalSize'),
        _a));
}
exports.writeTableStateToUrl = writeTableStateToUrl;
function readTableStateFromUrl(name, defaultTableState) {
    if (defaultTableState === void 0) { defaultTableState = null; }
    var object = router_utils_1.readFromHash(null, {});
    if (name in object) {
        return object[name];
    }
    if (defaultTableState) {
        setTimeout(function () { return writeTableStateToUrl(name, defaultTableState); });
        return defaultTableState;
    }
}
exports.readTableStateFromUrl = readTableStateFromUrl;
function getDefaultSizePerPageList() {
    return [
        { text: '25', value: 25 },
        { text: '50', value: 50 },
        { text: '100', value: 100 }
    ];
}
exports.getDefaultSizePerPageList = getDefaultSizePerPageList;
function resourcesToOptions(data, label, key, labelCallback, keyCallback) {
    if (label === void 0) { label = 'name'; }
    if (key === void 0) { key = 'id'; }
    if (labelCallback === void 0) { labelCallback = null; }
    if (keyCallback === void 0) { keyCallback = null; }
    var collection = {};
    data.forEach(function (v) {
        var l = v[name];
        if (labelCallback) {
            l = labelCallback(l, v);
        }
        var k = v[key];
        if (keyCallback) {
            k = keyCallback(k, v);
        }
        collection[k] = l;
    });
    return collection;
}
exports.resourcesToOptions = resourcesToOptions;
//# sourceMappingURL=bootstrap-table-utils.js.map