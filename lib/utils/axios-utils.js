"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var auth_utils_1 = require("./auth-utils");
var _ = require("lodash");
var window_utils_1 = require("./window-utils");
var string_utils_1 = require("./string-utils");
var __1 = require("..");
function createCancelToken(id, config) {
    if (config === void 0) { config = null; }
    var source = makeCancelToken();
    storeCancelToken(id, source, config);
    return source;
}
exports.createCancelToken = createCancelToken;
function makeCancelToken() {
    var source = axios_1.default.CancelToken.source();
    source.id = string_utils_1.makeId();
    return source;
}
exports.makeCancelToken = makeCancelToken;
function storeCancelToken(id, source, config) {
    if (config === void 0) { config = null; }
    var storedCancelTokens = window_utils_1.getGlobal('storedCancelTokens', []);
    storedCancelTokens.push({ id: id, source: source, config: config });
    window_utils_1.setGlobal('storedCancelTokens', storedCancelTokens);
}
exports.storeCancelToken = storeCancelToken;
function cancelRequest(tokenOrId, message) {
    if (message === void 0) { message = null; }
    var storedCancelTokens = window_utils_1.getGlobal('storedCancelTokens', []);
    var lastIndex = -1;
    var sourceIndex = storedCancelTokens.findIndex(function (s) { return s.source.id == tokenOrId || s.id == tokenOrId; });
    while (sourceIndex >= 0) {
        lastIndex = sourceIndex;
        storedCancelTokens[sourceIndex].source.cancel(JSON.stringify({ id: storedCancelTokens[sourceIndex].id, message: message }));
        sourceIndex = storedCancelTokens.findIndex(function (s, index) { return index > lastIndex && (s.source.id == tokenOrId || s.id == tokenOrId); });
    }
}
exports.cancelRequest = cancelRequest;
function bypassGlobalResponseHandlers(bypassGlobalResponseHandlersOption, status, response, expired) {
    if (_.isBoolean(bypassGlobalResponseHandlersOption)) {
        return bypassGlobalResponseHandlersOption;
    }
    else if (_.isArray(bypassGlobalResponseHandlersOption)) {
        return bypassGlobalResponseHandlersOption.findIndex(function (h) { return h == status; }) >= 0;
    }
    else {
        return bypassGlobalResponseHandlersOption(status, response, expired);
    }
}
exports.bypassGlobalResponseHandlers = bypassGlobalResponseHandlers;
function setupAxiosInstance(instance, config) {
    if (config === void 0) { config = {}; }
    instance.interceptors.request.use(function (_config) {
        _config = __assign({}, _config, (config || {}));
        var source = makeCancelToken();
        var tokenId = _config.cancelToken ? _config.cancelToken : (_config.method + " " + _config.url).toLowerCase();
        _config.cancelToken = source.token;
        _config.cancelTokenId = source.id;
        if (!_config.skipCancel) {
            cancelRequest(tokenId, _config.cancelMessage || 'Skipped because of ' + source.id);
        }
        if (auth_utils_1.default.isLoggedIn()) {
            _config.headers = __assign({}, _config.headers, { token: auth_utils_1.default.getToken() });
        }
        for (var key in _config.headers) {
            var header = _config.headers[key];
            if (_.isFunction(header)) {
                _config.headers[key] = header();
            }
        }
        storeCancelToken(tokenId, source, _config);
        if (config.globalHandlers && config.globalHandlers.onRequest) {
            config.globalHandlers.onRequest(_config, config);
        }
        return Promise.resolve(_config);
    }, function (error) {
        console.error(error);
        if (config.globalHandlers && config.globalHandlers.onRequest) {
            config.globalHandlers.onRequestError(error, config);
        }
        return Promise.reject(error);
    });
    instance.interceptors.response.use(function (response) {
        if (response.config.cancelToken) {
            var storedCancelTokens = window_utils_1.getGlobal('storedCancelTokens', []);
            var index = storedCancelTokens.findIndex(function (s) { return s.source.id == response.config.cancelTokenId; });
            if (index >= 0) {
                storedCancelTokens.splice(index, 1);
                window_utils_1.setGlobal('storedCancelTokens', storedCancelTokens);
            }
        }
        var config = response.config;
        var headers = __1.parseHeadersToApiResponseHeaders(response.headers);
        var bypass = 'bypassGlobalResponseHandlers' in config &&
            bypassGlobalResponseHandlers(config.bypassGlobalResponseHandlers, response.status, response.data, !!headers.expired);
        if (!bypass) {
            if (config.onResponse && response.status in config.onResponse) {
                config.onResponse[response.status]();
            }
            if (headers.expired && config.onExpired) {
                config.onExpired();
            }
        }
        if (config.globalHandlers && config.globalHandlers.onResponse) {
            config.globalHandlers.onResponse(response, config);
        }
        return response;
    }, function (error) {
        if (axios_1.default.isCancel(error)) {
            try {
                var cancelPayload_1 = JSON.parse(error.message);
                var storedCancelTokens = window_utils_1.getGlobal('storedCancelTokens', []);
                var sourceIndex = storedCancelTokens.findIndex(function (s) { return s.id == cancelPayload_1.id; });
                if (sourceIndex >= 0) {
                    console.warn(cancelPayload_1.message, storedCancelTokens[sourceIndex]);
                    if (config.globalHandlers && config.globalHandlers.onResponseError) {
                        config.globalHandlers.onResponseError({
                            message: cancelPayload_1.message,
                            config: storedCancelTokens[sourceIndex]
                        }, config);
                    }
                    storedCancelTokens.splice(sourceIndex, 1);
                }
                else {
                    console.warn(error);
                    if (config.globalHandlers && config.globalHandlers.onResponseError) {
                        config.globalHandlers.onResponseError(error, config);
                    }
                }
            }
            catch (_a) {
                console.warn(error);
                if (config.globalHandlers && config.globalHandlers.onResponseError) {
                    config.globalHandlers.onResponseError(error, config);
                }
            }
        }
        else {
            if (error.config.cancelToken) {
                var storedCancelTokens = window_utils_1.getGlobal('storedCancelTokens', []);
                var index = storedCancelTokens.findIndex(function (s) { return s.source.id == error.config.cancelTokenId; });
                if (index >= 0) {
                    storedCancelTokens.splice(index, 1);
                    window_utils_1.setGlobal('storedCancelTokens', storedCancelTokens);
                }
            }
            console.log('ERROR RESPONSE', error);
            var config_1 = error.config;
            var response = error.response;
            var headers = __1.parseHeadersToApiResponseHeaders(response.headers);
            var bypass = 'bypassGlobalResponseHandlers' in config_1 &&
                bypassGlobalResponseHandlers(config_1.bypassGlobalResponseHandlers, response.status, response.data, !!headers.expired);
            if (!bypass) {
                if (config_1.onResponse && response.status in config_1.onResponse) {
                    config_1.onResponse[response.status]();
                }
                if (headers.expired && config_1.onExpired) {
                    config_1.onExpired();
                }
            }
            if (config_1.globalHandlers && config_1.globalHandlers.onResponseError) {
                config_1.globalHandlers.onResponseError(error, config_1);
            }
        }
        return Promise.reject(error);
    });
}
exports.setupAxiosInstance = setupAxiosInstance;
function redirectOnFailConnect(history, redirects) {
    if (redirects === void 0) { redirects = {
        401: function (history) { return history.push('/login'); },
        403: function (history) { return history.push('/login'); },
        404: function (history) { return history.push('/notfound'); },
    }; }
    return {
        catch: redirectOnFail(history, redirects),
    };
}
exports.redirectOnFailConnect = redirectOnFailConnect;
function redirectOnFail(history, redirects) {
    if (redirects === void 0) { redirects = {
        401: function (history) { return history.push('/login'); },
        403: function (history) { return history.push('/login'); },
        404: function (history) { return history.push('/notfound'); },
    }; }
    return function (e) {
        if (e.response && e.response.status && redirects[e.response.status]) {
            if (e.response.status == 401) {
                auth_utils_1.default.logout();
                setTimeout(function () { return window.document.location.reload(true); }, 500);
            }
            redirects[e.response.status](history);
        }
    };
}
exports.redirectOnFail = redirectOnFail;
function backOnSuccessConnect(history, to) {
    if (to === void 0) { to = null; }
    return {
        then: backOnSuccess(history, to),
    };
}
exports.backOnSuccessConnect = backOnSuccessConnect;
function backOnSuccess(history, to) {
    if (to === void 0) { to = null; }
    return function (response) {
        if (to) {
            history.push(to);
        }
        else {
            history.goBack();
        }
    };
}
exports.backOnSuccess = backOnSuccess;
//# sourceMappingURL=axios-utils.js.map