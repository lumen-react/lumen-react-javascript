"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function addFullListener(event, listener, options) {
    window.addEventListener(event, listener, options);
}
exports.addFullListener = addFullListener;
function addListener(event, listener, options) {
    window.addEventListener(event, function (e) { return listener(e.detail); }, options);
}
exports.addListener = addListener;
function dispatch(event, detail) {
    window.dispatchEvent(new CustomEvent(event, { detail: detail }));
}
exports.dispatch = dispatch;
function removeListener(event, listener, options) {
    window.removeEventListener(event, listener, options);
}
exports.removeListener = removeListener;
//# sourceMappingURL=event-utils.js.map