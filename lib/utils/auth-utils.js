"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_utils_1 = require("./storage-utils");
var COOKIE_TOKEN = 'AUTH_COOKIE';
var AuthUtils = (function () {
    function AuthUtils() {
    }
    AuthUtils.boot = function (storageDriverId) {
        AuthUtils.storageDriverId = storageDriverId;
        var data = storage_utils_1.default.get(COOKIE_TOKEN, AuthUtils.storageDriverId);
        if (data) {
            AuthUtils.token = data.token;
            AuthUtils.user = data.user;
        }
    };
    AuthUtils.login = function (token, user) {
        AuthUtils.token = token;
        AuthUtils.user = user;
        storage_utils_1.default.set(COOKIE_TOKEN, { token: token, user: user }, AuthUtils.storageDriverId);
    };
    AuthUtils.logout = function () {
        AuthUtils.token = null;
        AuthUtils.user = null;
        storage_utils_1.default.remove(COOKIE_TOKEN, AuthUtils.storageDriverId);
    };
    AuthUtils.isLoggedIn = function () {
        return AuthUtils.token != null && this.user != null;
    };
    AuthUtils.authenticatedUser = function () {
        return AuthUtils.user;
    };
    AuthUtils.getToken = function () {
        return AuthUtils.token;
    };
    AuthUtils.token = null;
    AuthUtils.user = null;
    return AuthUtils;
}());
exports.default = AuthUtils;
//# sourceMappingURL=auth-utils.js.map