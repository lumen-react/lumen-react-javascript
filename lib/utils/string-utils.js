"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function makeId(length) {
    if (length === void 0) { length = 32; }
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
exports.makeId = makeId;
function lcfirst(string) {
    return string.charAt(0).toLowerCase() + string.substr(1);
}
exports.lcfirst = lcfirst;
function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.substr(1);
}
exports.ucfirst = ucfirst;
function snakeCaseToNormal(string) {
    return string.replace(/_/g, ' ').replace(/\w\S*/g, function (word) {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
}
exports.snakeCaseToNormal = snakeCaseToNormal;
//# sourceMappingURL=string-utils.js.map