"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
function writeToHash(object, clear) {
    if (clear === void 0) { clear = false; }
    if (clear) {
        window.location.hash = JSON.stringify(object);
    }
    else {
        var oldObject = readFromHash(null, {});
        var newObject = __assign({}, oldObject, object);
        window.location.hash = JSON.stringify(newObject);
    }
}
exports.writeToHash = writeToHash;
function readFromHash(key, defaultValue) {
    if (key === void 0) { key = null; }
    if (defaultValue === void 0) { defaultValue = null; }
    try {
        var parsed = JSON.parse(decodeURI(window.location.hash.substr(1)));
        if (key !== null) {
            return key in parsed ? parsed[key] : defaultValue;
        }
        else {
            return parsed;
        }
    }
    catch (_a) {
        return defaultValue;
    }
}
exports.readFromHash = readFromHash;
//# sourceMappingURL=router-utils.js.map