"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var window_utils_1 = require("./window-utils");
function goto(href, target, e) {
    if (target === void 0) { target = '_top'; }
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }
    if (typeof target === 'string') {
        var node = document.createElement('a');
        node.href = href;
        node.target = target;
        var a = document.body.appendChild(node);
        a.click();
        document.body.removeChild(a);
    }
    else {
        target.push(href);
    }
    return false;
}
exports.goto = goto;
function gotoAlt(href, blankTarget, e) {
    if (blankTarget === void 0) { blankTarget = '_top'; }
    return goto(href, window_utils_1.isKeyDown(window_utils_1.OPT_KEY) ? '_blank' : blankTarget, e);
}
exports.gotoAlt = gotoAlt;
//# sourceMappingURL=dom-utils.js.map