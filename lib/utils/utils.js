"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function classNames() {
    var classMap = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        classMap[_i] = arguments[_i];
    }
    var classes = [];
    for (var i = 0; i < classMap.length; i++) {
        var className = classMap[i];
        if (typeof className === 'string') {
            classes.push(className);
        }
        else {
            for (var key in className) {
                if (className[key] === true) {
                    if (className[key]) {
                        classes.push(key);
                    }
                }
                else if (className[key] !== false) {
                    var splitClasses = className[key].split(/ +/);
                    for (var j = 0; j < splitClasses.length; j++) {
                        classes.push(splitClasses[j] + "-" + key);
                    }
                }
            }
        }
    }
    return classes.join(" ");
}
exports.classNames = classNames;
//# sourceMappingURL=utils.js.map