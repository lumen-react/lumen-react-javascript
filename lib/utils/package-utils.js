"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function deprecated(message) {
    if (message === void 0) { message = ''; }
    console.trace('DEPRECATED', message);
}
exports.deprecated = deprecated;
//# sourceMappingURL=package-utils.js.map