"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function removeUndefinedKeys(object) {
    var o = {};
    for (var k in object) {
        if (object[k] != undefined) {
            o[k] = object[k];
        }
    }
    return o;
}
exports.removeUndefinedKeys = removeUndefinedKeys;
function swap(object) {
    var ret = {};
    for (var key in object) {
        ret[object[key]] = key;
    }
    return ret;
}
exports.swap = swap;
//# sourceMappingURL=object-utils.js.map