"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var react_intl_1 = require("react-intl");
var abstract_validator_rule_1 = require("./abstract-validator-rule");
var EqualsFieldValidatorRule = (function (_super) {
    __extends(EqualsFieldValidatorRule, _super);
    function EqualsFieldValidatorRule(otherField, message) {
        var _this = _super.call(this, message) || this;
        _this.otherField = otherField;
        return _this;
    }
    EqualsFieldValidatorRule.prototype.getDefaultMessage = function () {
        return react_intl_1.defineMessages({
            error: {
                id: 'validator.equals',
                defaultMessage: 'Should equal {otherField} field.',
            }
        }).error;
    };
    EqualsFieldValidatorRule.prototype.getMessageValues = function () {
        return { otherField: this.otherField };
    };
    EqualsFieldValidatorRule.prototype.isValid = function (value, name, values, props) {
        return value === values[this.otherField];
    };
    return EqualsFieldValidatorRule;
}(abstract_validator_rule_1.AbstractValidatorRule));
exports.EqualsFieldValidatorRule = EqualsFieldValidatorRule;
//# sourceMappingURL=equals-field-validator-rule.js.map