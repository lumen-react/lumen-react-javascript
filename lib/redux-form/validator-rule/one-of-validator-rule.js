"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var react_intl_1 = require("react-intl");
var abstract_validator_rule_1 = require("./abstract-validator-rule");
var OneOfValidatorRule = (function (_super) {
    __extends(OneOfValidatorRule, _super);
    function OneOfValidatorRule(pool) {
        var _this = _super.call(this) || this;
        _this.pool = pool;
        return _this;
    }
    OneOfValidatorRule.prototype.getDefaultMessage = function () {
        return react_intl_1.defineMessages({
            error: {
                id: 'validator.oneOf',
                defaultMessage: 'Should be a one of {values}.',
            }
        }).error;
    };
    OneOfValidatorRule.prototype.isValid = function (value) {
        return this.pool.findIndex(function (p) { return value == p; }) >= 0;
    };
    OneOfValidatorRule.prototype.getMessageValues = function () {
        return { values: this.pool.join(', ') };
    };
    return OneOfValidatorRule;
}(abstract_validator_rule_1.AbstractValidatorRule));
exports.OneOfValidatorRule = OneOfValidatorRule;
//# sourceMappingURL=one-of-validator-rule.js.map