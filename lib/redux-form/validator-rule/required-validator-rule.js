"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var react_intl_1 = require("react-intl");
var _ = require("lodash");
var abstract_validator_rule_1 = require("./abstract-validator-rule");
var RequiredValidatorRule = (function (_super) {
    __extends(RequiredValidatorRule, _super);
    function RequiredValidatorRule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RequiredValidatorRule.prototype.getDefaultMessage = function () {
        return react_intl_1.defineMessages({
            error: {
                id: 'validator.required',
                defaultMessage: 'Required.',
            }
        }).error;
    };
    RequiredValidatorRule.prototype.isValid = function (value, name, values, props) {
        return _.isArray(value) ? !!value.length : !!value;
    };
    return RequiredValidatorRule;
}(abstract_validator_rule_1.AbstractValidatorRule));
exports.RequiredValidatorRule = RequiredValidatorRule;
//# sourceMappingURL=required-validator-rule.js.map