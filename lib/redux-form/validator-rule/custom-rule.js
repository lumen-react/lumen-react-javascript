"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var react_intl_1 = require("react-intl");
var abstract_validator_rule_1 = require("./abstract-validator-rule");
var CustomRule = (function (_super) {
    __extends(CustomRule, _super);
    function CustomRule(callback) {
        var _this = _super.call(this) || this;
        _this.callback = callback;
        return _this;
    }
    CustomRule.prototype.getDefaultMessage = function () {
        return react_intl_1.defineMessages({
            error: {
                id: 'validator.custom',
                defaultMessage: 'Something went from with field {field}.',
            }
        }).error;
    };
    CustomRule.prototype.isValid = function (value, name, values, props) {
        return this.callback(value, name, values, props);
    };
    return CustomRule;
}(abstract_validator_rule_1.AbstractValidatorRule));
exports.CustomRule = CustomRule;
//# sourceMappingURL=custom-rule.js.map