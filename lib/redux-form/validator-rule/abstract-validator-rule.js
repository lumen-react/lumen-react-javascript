"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractValidatorRule = (function () {
    function AbstractValidatorRule(message) {
        this._message = message || this.getDefaultMessage();
    }
    Object.defineProperty(AbstractValidatorRule.prototype, "message", {
        get: function () {
            return this._message;
        },
        set: function (value) {
            this._message = value;
        },
        enumerable: true,
        configurable: true
    });
    AbstractValidatorRule.prototype.getMessageValues = function () {
        return {};
    };
    return AbstractValidatorRule;
}());
exports.AbstractValidatorRule = AbstractValidatorRule;
//# sourceMappingURL=abstract-validator-rule.js.map