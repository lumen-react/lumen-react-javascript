"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var react_intl_1 = require("react-intl");
var abstract_validator_rule_1 = require("./abstract-validator-rule");
var NumberValidatorRule = (function (_super) {
    __extends(NumberValidatorRule, _super);
    function NumberValidatorRule(options) {
        var _this = _super.call(this) || this;
        _this.options = {
            strict: false,
            integer: false,
        };
        if (options) {
            _this.options = options;
        }
        return _this;
    }
    NumberValidatorRule.prototype.getDefaultMessage = function () {
        return react_intl_1.defineMessages({
            error: {
                id: 'validator.invalidNumber',
                defaultMessage: 'Should be a valid number.',
            }
        }).error;
    };
    NumberValidatorRule.prototype.isValid = function (value) {
        if (typeof value === "string") {
            if (this.options.strict) {
                return false;
            }
            else if (!value.match(/[0-9]*(?=\.[0-9]*)?/)) {
                return false;
            }
            value = parseFloat(value);
        }
        if (this.options.integer) {
            if (Math.round(value) !== value) {
                return false;
            }
        }
        if (this.options.min !== undefined) {
            if (value < this.options.min) {
                return false;
            }
        }
        if (this.options.max !== undefined) {
            if (value > this.options.max) {
                return false;
            }
        }
        return true;
    };
    return NumberValidatorRule;
}(abstract_validator_rule_1.AbstractValidatorRule));
exports.NumberValidatorRule = NumberValidatorRule;
//# sourceMappingURL=number-validator-rule.js.map