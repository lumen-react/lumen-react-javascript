"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_router_dom_1 = require("react-router-dom");
var react_intl_1 = require("react-intl");
var FormCancel = (function (_super) {
    __extends(FormCancel, _super);
    function FormCancel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormCancel.prototype.render = function () {
        return (React.createElement(react_router_dom_1.Link, { to: this.props.to, className: "btn btn-default mr" }, this.props.label));
    };
    FormCancel.defaultProps = {
        to: '/',
        label: React.createElement(react_intl_1.FormattedMessage, { id: "cancel", defaultMessage: "Cancel" }),
    };
    return FormCancel;
}(React.Component));
exports.default = FormCancel;
//# sourceMappingURL=form-cancel.js.map