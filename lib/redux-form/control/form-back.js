"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_intl_1 = require("react-intl");
var FormBack = (function (_super) {
    __extends(FormBack, _super);
    function FormBack() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormBack.prototype.render = function () {
        return (React.createElement("button", { type: "button", className: "btn btn-default mr", onClick: this.props.history.goBack }, this.props.label));
    };
    FormBack.defaultProps = {
        label: React.createElement(react_intl_1.FormattedMessage, { id: "back", defaultMessage: "Back" }),
    };
    return FormBack;
}(React.Component));
exports.default = FormBack;
//# sourceMappingURL=form-back.js.map