"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_intl_1 = require("react-intl");
var FormSubmit = (function (_super) {
    __extends(FormSubmit, _super);
    function FormSubmit() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormSubmit.prototype.render = function () {
        return (React.createElement("button", { type: "submit", className: "btn btn-primary mr", disabled: this.props.pristine || this.props.submitting }, this.props.label));
    };
    FormSubmit.defaultProps = {
        label: React.createElement(react_intl_1.FormattedMessage, { id: "submit", defaultMessage: "Submit" }),
    };
    return FormSubmit;
}(React.Component));
exports.default = FormSubmit;
//# sourceMappingURL=form-submit.js.map