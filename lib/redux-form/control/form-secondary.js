"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_intl_1 = require("react-intl");
var FormSecondary = (function (_super) {
    __extends(FormSecondary, _super);
    function FormSecondary() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormSecondary.prototype.render = function () {
        var _this = this;
        return (React.createElement("button", { type: "button", className: "btn btn-danger mr", onClick: function () { return _this.props.confirm ? (confirm(_this.props.confirm) ? _this.props.onClick() : null) : _this.props.onClick(); }, disabled: this.props.disabled }, this.props.label));
    };
    FormSecondary.defaultProps = {
        label: React.createElement(react_intl_1.FormattedMessage, { id: "delete", defaultMessage: "Delete" }),
        disabled: false,
    };
    return FormSecondary;
}(React.Component));
exports.default = FormSecondary;
//# sourceMappingURL=form-secondary.js.map