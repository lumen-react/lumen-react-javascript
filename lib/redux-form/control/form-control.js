"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var form_cancel_1 = require("./form-cancel");
var form_submit_1 = require("./form-submit");
var form_secondary_1 = require("./form-secondary");
var form_reset_1 = require("./form-reset");
var form_back_1 = require("./form-back");
var react_bootstrap_1 = require("react-bootstrap");
var FormControl = (function (_super) {
    __extends(FormControl, _super);
    function FormControl() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormControl.prototype.renderError = function () {
        if (this.props.error) {
            if (this.props.showError === true) {
                return React.createElement(react_bootstrap_1.Alert, { variant: "danger" }, this.props.error);
            }
            else if (this.props.showError !== false) {
                return this.props.showError(this.props.error);
            }
        }
    };
    FormControl.prototype.render = function () {
        var _a = this.props, submitControl = _a.submitControl, secondaryControl = _a.secondaryControl, resetControl = _a.resetControl, cancelControl = _a.cancelControl, backControl = _a.backControl, injectedFormProps = __rest(_a, ["submitControl", "secondaryControl", "resetControl", "cancelControl", "backControl"]);
        return (React.createElement("div", null,
            this.renderError(),
            submitControl && React.createElement(form_submit_1.default, __assign({}, injectedFormProps, (submitControl !== true ? submitControl : {}))),
            secondaryControl && React.createElement(form_secondary_1.default, __assign({}, injectedFormProps, (secondaryControl !== true ? secondaryControl : {}))),
            resetControl && React.createElement(form_reset_1.default, __assign({}, injectedFormProps, (resetControl !== true ? resetControl : {}))),
            cancelControl && React.createElement(form_cancel_1.default, __assign({}, injectedFormProps, (cancelControl !== true ? cancelControl : {}))),
            backControl && React.createElement(form_back_1.default, __assign({}, injectedFormProps, backControl))));
    };
    FormControl.defaultProps = {
        submitControl: true,
        secondaryControl: false,
        resetControl: true,
        cancelControl: false,
        backControl: false,
        showError: true,
    };
    return FormControl;
}(React.Component));
exports.default = FormControl;
//# sourceMappingURL=form-control.js.map