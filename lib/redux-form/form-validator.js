"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_intl_1 = require("react-intl");
var redux_form_1 = require("redux-form");
var equals_field_validator_rule_1 = require("./validator-rule/equals-field-validator-rule");
var required_validator_rule_1 = require("./validator-rule/required-validator-rule");
var email_validator_rule_1 = require("./validator-rule/email-validator-rule");
var one_of_validator_rule_1 = require("./validator-rule/one-of-validator-rule");
var custom_rule_1 = require("./validator-rule/custom-rule");
var number_validator_rule_1 = require("./validator-rule/number-validator-rule");
function formValuesTransformer(handler, transformer) {
    return function (values) {
        values = transformer(values);
        return handler(values);
    };
}
exports.formValuesTransformer = formValuesTransformer;
function submitFormError(data) {
    throw new redux_form_1.SubmissionError(data);
}
exports.submitFormError = submitFormError;
function submitFormErrorHandler(handler) {
    return function (values) {
        return handler(values).catch(function (error) {
            if (error.response && error.response.data && error.response.data.fields) {
                submitFormError(error.response.data.fields);
            }
        });
    };
}
exports.submitFormErrorHandler = submitFormErrorHandler;
var FormValidator = (function () {
    function FormValidator(values, props) {
        var validators = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            validators[_i - 2] = arguments[_i];
        }
        this.validators = [];
        this.values = values;
        this.props = props;
        this.validators = validators;
    }
    FormValidator.make = function (values, props) {
        var validators = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            validators[_i - 2] = arguments[_i];
        }
        return new (FormValidator.bind.apply(FormValidator, [void 0, values, props].concat(validators)))();
    };
    FormValidator.prototype.validate = function () {
        var _this = this;
        var errors = {};
        this.validators.forEach(function (validator) {
            var error = validator.validate(_this.values[validator.name], _this.values, _this.props);
            if (error) {
                if (!errors[validator.name]) {
                    errors[validator.name] = [];
                }
                errors[validator.name].push(React.createElement(react_intl_1.FormattedMessage, { id: error.message.id, defaultMessage: error.message.defaultMessage, values: error.values }));
            }
        });
        return errors;
    };
    FormValidator.prototype.asyncValidate = function () {
        var _this = this;
        var promises = [];
        this.validators.forEach(function (validator) {
            promises.push(validator.asyncValidate(_this.values[validator.name], _this.values, _this.props));
        });
        return Promise.all(promises);
    };
    FormValidator.prototype.custom = function (name, callback) {
        this.validators.push(new Validator(name, new custom_rule_1.CustomRule(callback)));
        return this;
    };
    FormValidator.prototype.email = function (name) {
        this.validators.push(new Validator(name, new email_validator_rule_1.EmailValidatorRule()));
        return this;
    };
    FormValidator.prototype.equalsField = function (name, other) {
        this.validators.push(new Validator(name, new equals_field_validator_rule_1.EqualsFieldValidatorRule(other)));
        return this;
    };
    FormValidator.prototype.number = function (name, options) {
        this.validators.push(new Validator(name, new number_validator_rule_1.NumberValidatorRule(options)));
        return this;
    };
    FormValidator.prototype.oneOf = function (name, pool) {
        this.validators.push(new Validator(name, new one_of_validator_rule_1.OneOfValidatorRule(pool)));
        return this;
    };
    FormValidator.prototype.required = function (name) {
        this.validators.push(new Validator(name, new required_validator_rule_1.RequiredValidatorRule()));
        return this;
    };
    return FormValidator;
}());
exports.default = FormValidator;
var Validator = (function () {
    function Validator(name, validatorRule) {
        this._name = name;
        this._validatorRule = validatorRule;
    }
    Object.defineProperty(Validator.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Validator.prototype, "validatorRule", {
        get: function () {
            return this._validatorRule;
        },
        set: function (value) {
            this._validatorRule = value;
        },
        enumerable: true,
        configurable: true
    });
    Validator.prototype.getMessageValues = function () {
        return __assign({}, this.validatorRule.getMessageValues());
    };
    Validator.prototype.validate = function (value, values, props) {
        return this.validatorRule.isValid(value, this.name, values, props) ? null :
            new ErrorMessage(this.validatorRule.message, this.getMessageValues());
    };
    Validator.prototype.asyncValidate = function (value, values, props) {
        var _this = this;
        return new Promise((function (resolve, reject) {
            _this.validatorRule.isValid(value, _this.name, values, props) ? resolve(null) :
                reject(new ErrorMessage(_this.validatorRule.message, _this.validatorRule.getMessageValues()));
        }));
    };
    return Validator;
}());
exports.Validator = Validator;
var ErrorMessage = (function () {
    function ErrorMessage(message, values) {
        this.message = message;
        this.values = values;
    }
    return ErrorMessage;
}());
exports.ErrorMessage = ErrorMessage;
//# sourceMappingURL=form-validator.js.map