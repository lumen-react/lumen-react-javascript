"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_form_field_1 = require("./abstract-form-field");
var FormFieldCustom = (function (_super) {
    __extends(FormFieldCustom, _super);
    function FormFieldCustom() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormFieldCustom.prototype.renderInput = function (field) {
        return this.props.children(field);
    };
    return FormFieldCustom;
}(abstract_form_field_1.default));
exports.default = FormFieldCustom;
//# sourceMappingURL=form-field-custom.js.map