"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var redux_form_1 = require("redux-form");
var abstract_component_1 = require("../../components/abstract-component");
var _ = require("lodash");
var AbstractFormField = (function (_super) {
    __extends(AbstractFormField, _super);
    function AbstractFormField() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AbstractFormField.prototype.getInternalFormFieldRenderer = function () {
        return 'InternalFormField';
    };
    AbstractFormField.prototype.render = function () {
        var props = __assign({}, this.props, { renderInput: this.renderInput.bind(this) });
        return React.createElement(redux_form_1.Field, __assign({}, props, { component: FORM_FIELD_RENDERERS[this.getInternalFormFieldRenderer()] }));
    };
    AbstractFormField.defaultProps = {
        pure: false,
    };
    return AbstractFormField;
}(abstract_component_1.AbstractStatelessComponent));
var InternalFormField = (function (_super) {
    __extends(InternalFormField, _super);
    function InternalFormField() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    InternalFormField.prototype.getClasses = function () {
        return "form-group " + (this.props.meta.touched && this.props.meta.error ? 'has-error' : '') + " " + (this.props.className ? this.props.className : '');
    };
    InternalFormField.prototype.renderErrors = function (touched, error) {
        if (touched) {
            if (error) {
                if (_.isArray(error)) {
                    return error.map(function (e, i) {
                        if (React.isValidElement(e)) {
                            return e;
                        }
                        else {
                            return React.createElement("div", { key: i }, e.toString());
                        }
                    });
                }
                else if (_.isObject(error) && 'id' in error && 'message' in error) {
                    return React.createElement("div", null, this.intl.formatMessage(error.message, error.values));
                }
                else {
                    return React.createElement("div", null, error.toString());
                }
            }
        }
        else {
            return null;
        }
    };
    InternalFormField.prototype.render = function () {
        var className = this.getClasses();
        return this.props.pure ? this.props.renderInput(this.props) : (React.createElement("div", { className: className },
            this.props.renderLabel ? this.props.renderLabel(this.props) :
                this.props.renderLabel !== null ? (React.createElement("label", null, this.props.label)) : null,
            this.props.renderInput(this.props),
            React.createElement("div", { className: "text-danger" }, this.renderErrors(this.props.meta.touched, this.props.meta.error))));
    };
    return InternalFormField;
}(abstract_component_1.AbstractStatelessComponent));
var InternalFormFieldNoClasses = (function (_super) {
    __extends(InternalFormFieldNoClasses, _super);
    function InternalFormFieldNoClasses() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    InternalFormFieldNoClasses.prototype.getClasses = function () {
        return '';
    };
    return InternalFormFieldNoClasses;
}(InternalFormField));
var FORM_FIELD_RENDERERS = {
    'InternalFormField': InternalFormField,
    'InternalFormFieldNoClasses': InternalFormFieldNoClasses,
};
exports.default = AbstractFormField;
//# sourceMappingURL=abstract-form-field.js.map