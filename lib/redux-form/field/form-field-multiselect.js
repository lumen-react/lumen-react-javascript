"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var _ = require("lodash");
var abstract_form_field_1 = require("./abstract-form-field");
var react_intl_1 = require("react-intl");
var react_widgets_1 = require("react-widgets");
var FormFieldMultiselect = (function (_super) {
    __extends(FormFieldMultiselect, _super);
    function FormFieldMultiselect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.newlyGeneratedId = -1;
        return _this;
    }
    FormFieldMultiselect.prototype.renderInput = function (field) {
        var _this = this;
        var _a = this.props.multiselectProps, onCreate = _a.onCreate, multiselectProps = __rest(_a, ["onCreate"]);
        if (onCreate === true) {
            onCreate = function (name) { return name; };
        }
        return (React.createElement(react_widgets_1.Multiselect, __assign({}, multiselectProps, field.input, { onBlur: function () { return field.input.onBlur(); }, value: field.input.value || [], data: this.props.data, valueField: this.props.valueField, textField: this.props.textField, onChange: function (items) {
                if (_this.props.useObjectAsValue) {
                    field.input.onChange(items.map(function (item) { return item[_this.props.valueField]; }));
                }
                else {
                    field.input.onChange(items);
                }
            }, onCreate: function (item) {
                var _a;
                if (onCreate && 'allowCreate' in multiselectProps && multiselectProps.allowCreate !== false) {
                    var newValue = onCreate(item);
                    var pushedObject = null;
                    if (_.isObject(newValue) && _this.props.valueField in newValue) {
                        pushedObject = newValue;
                    }
                    else {
                        pushedObject = (_a = {},
                            _a[_this.props.valueField] = _this.newlyGeneratedId,
                            _a[_this.props.textField] = newValue,
                            _a);
                        _this.newlyGeneratedId--;
                    }
                    field.input.onChange((field.input.value || []).concat([pushedObject]), {
                        dataItem: pushedObject,
                        action: 'insert',
                        originalEvent: null,
                        lastValue: (field.input.value || []).slice(),
                        searchTerm: item,
                    });
                }
            } })));
    };
    FormFieldMultiselect.defaultProps = {
        variant: 'default',
        placeholder: React.createElement(react_intl_1.FormattedMessage, { id: "select", defaultMessage: "Select..." }),
        pure: false,
        multiselectProps: {},
        useObjectAsValue: false,
    };
    return FormFieldMultiselect;
}(abstract_form_field_1.default));
exports.default = FormFieldMultiselect;
//# sourceMappingURL=form-field-multiselect.js.map