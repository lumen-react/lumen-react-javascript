"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var abstract_form_field_1 = require("./abstract-form-field");
var react_intl_1 = require("react-intl");
var react_widgets_1 = require("react-widgets");
var _ = require("lodash");
var FormFieldDropdown = (function (_super) {
    __extends(FormFieldDropdown, _super);
    function FormFieldDropdown() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormFieldDropdown.prototype.renderInput = function (field) {
        var _this = this;
        var _a = this.props.dropdownProps, onCreate = _a.onCreate, dropdownProps = __rest(_a, ["onCreate"]);
        if (onCreate === true) {
            onCreate = function (name) { return name; };
        }
        return (React.createElement(react_widgets_1.DropdownList, __assign({}, dropdownProps, field.input, { data: this.props.data, valueField: this.props.valueField, textField: this.props.textField, onChange: function (item) {
                if (_this.props.useObjectAsValue) {
                    field.input.onChange(item);
                }
                else {
                    field.input.onChange(item[_this.props.valueField]);
                }
            }, onCreate: function (item) {
                var _a;
                if (onCreate) {
                    var newValue = onCreate(item);
                    var pushedObject = null;
                    if (_.isObject(newValue) && _this.props.valueField in newValue) {
                        pushedObject = newValue;
                    }
                    else {
                        pushedObject = (_a = {},
                            _a[_this.props.valueField] = -1,
                            _a[_this.props.textField] = newValue,
                            _a);
                    }
                    field.input.onChange(pushedObject, {
                        originalEvent: null,
                        lastValue: field.value,
                        searchTerm: item,
                    });
                }
            } })));
    };
    FormFieldDropdown.defaultProps = {
        variant: 'default',
        placeholder: React.createElement(react_intl_1.FormattedMessage, { id: "select", defaultMessage: "Select..." }),
        pure: false,
        dropdownProps: {},
        useObjectAsValue: false,
    };
    return FormFieldDropdown;
}(abstract_form_field_1.default));
exports.default = FormFieldDropdown;
//# sourceMappingURL=form-field-dropdown.js.map