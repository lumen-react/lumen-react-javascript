"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var event_utils_1 = require("../utils/event-utils");
var boot_utils_1 = require("../utils/boot-utils");
exports.eventLoader = function (options) {
    return function (Component) {
        var _a;
        return _a = (function (_super) {
                __extends(EventLoader, _super);
                function EventLoader(props) {
                    var _this = _super.call(this, props) || this;
                    _this.ready = [];
                    _this.loader = null;
                    _this.loader = options && options.loader !== undefined ? options.loader : undefined;
                    _this.state = {
                        ready: (!('requireTriggers' in options)) || options.requireTriggers === false,
                        passedProps: {},
                    };
                    if (typeof options.types === 'string') {
                        options.types = [options.types];
                    }
                    var _loop_1 = function (index) {
                        event_utils_1.addListener(options.types[index], function (data) {
                            _this.setState({
                                passedProps: __assign({}, _this.state.passedProps, options.mapEventDetailsToProps(data, options.types[index]))
                            });
                        });
                    };
                    for (var index in options.types) {
                        _loop_1(index);
                    }
                    if (!_this.state.ready) {
                        if (options.requireTriggers === true) {
                            options.requireTriggers = options.types;
                        }
                        else if (typeof options.requireTriggers === 'string') {
                            options.requireTriggers = [options.requireTriggers];
                        }
                        var _loop_2 = function (index) {
                            event_utils_1.addListener(options.requireTriggers[index], function () {
                                _this.ready.push(index);
                                if (_this.ready.length === options.requireTriggers.length) {
                                    _this.setState({ ready: true });
                                }
                            }, { once: true });
                        };
                        for (var index in options.requireTriggers) {
                            _loop_2(index);
                        }
                    }
                    return _this;
                }
                EventLoader.prototype.render = function () {
                    if (this.state.ready) {
                        return React.createElement(Component, __assign({}, this.props, this.state.passedProps));
                    }
                    else {
                        return this.loader !== undefined ? this.loader : boot_utils_1.resolveLoader(this);
                    }
                };
                return EventLoader;
            }(React.Component)),
            _a.displayName = "EventLoader(" + (Component.displayName || Component.name) + ")",
            _a;
    };
};
//# sourceMappingURL=event-loader.js.map