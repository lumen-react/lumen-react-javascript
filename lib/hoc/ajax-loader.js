"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var boot_utils_1 = require("../utils/boot-utils");
exports.ajaxLoader = function (options) {
    return function (Component) {
        var _a;
        return _a = (function (_super) {
                __extends(AjaxLoader, _super);
                function AjaxLoader(props) {
                    var _this = _super.call(this, props) || this;
                    _this.promises = {};
                    _this.loader = null;
                    _this.loader = options && options.loader !== undefined ? options.loader : undefined;
                    _this.state = {
                        isRefresh: false,
                        ready: !(options && options.promises),
                    };
                    if (!_this.state.ready) {
                        for (var key in options.promises) {
                            if (options.promises[key]) {
                                _this.promises[key] = {
                                    result: null,
                                    done: false,
                                    promise: options.promises[key](props),
                                };
                            }
                        }
                        _this.state = { isRefresh: false, ready: false };
                    }
                    else {
                        _this.promises = undefined;
                        _this.state = { isRefresh: false, ready: true };
                    }
                    if (_this.promises) {
                        var _loop_1 = function (key) {
                            if (this_1.promises[key].promise) {
                                this_1.promises[key].promise.then(function (data) {
                                    _this.promises[key].done = true;
                                    _this.promises[key].result = data;
                                    var ready = true;
                                    for (var check in _this.promises) {
                                        if (!_this.promises[check].done) {
                                            ready = false;
                                            break;
                                        }
                                    }
                                    if (ready) {
                                        _this.setState({ ready: true });
                                    }
                                });
                            }
                            else {
                                this_1.promises[key].done = true;
                                var ready = true;
                                for (var check in this_1.promises) {
                                    if (!this_1.promises[check].done) {
                                        ready = false;
                                        break;
                                    }
                                }
                                if (ready) {
                                    setTimeout(function () { return _this.setState({ ready: true }); });
                                }
                            }
                        };
                        var this_1 = this;
                        for (var key in _this.promises) {
                            _loop_1(key);
                        }
                    }
                    return _this;
                }
                AjaxLoader.prototype.refresh = function () {
                    var _this = this;
                    this.promises = {};
                    this.setState({
                        isRefresh: true,
                        ready: !(options && options.promises),
                    }, function () {
                        if (!_this.state.ready) {
                            for (var key in options.promises) {
                                if (options.promises[key]) {
                                    _this.promises[key] = {
                                        result: null,
                                        done: false,
                                        promise: options.promises[key](_this.props),
                                    };
                                }
                            }
                        }
                        else {
                            _this.promises = undefined;
                            _this.setState({ ready: true });
                        }
                        if (_this.promises) {
                            var _loop_2 = function (key) {
                                if (_this.promises[key].promise) {
                                    _this.promises[key].promise.then(function (data) {
                                        _this.promises[key].done = true;
                                        _this.promises[key].result = data;
                                        var ready = true;
                                        for (var check in _this.promises) {
                                            if (!_this.promises[check].done) {
                                                ready = false;
                                                break;
                                            }
                                        }
                                        if (ready) {
                                            _this.setState({ ready: true });
                                        }
                                    });
                                }
                                else {
                                    _this.promises[key].done = true;
                                    var ready = true;
                                    for (var check in _this.promises) {
                                        if (!_this.promises[check].done) {
                                            ready = false;
                                            break;
                                        }
                                    }
                                    if (ready) {
                                        setTimeout(function () { return _this.setState({ ready: true }); });
                                    }
                                }
                            };
                            for (var key in _this.promises) {
                                _loop_2(key);
                            }
                        }
                    });
                };
                AjaxLoader.prototype.render = function () {
                    var returned = [];
                    if (this.state.ready) {
                        this.promiseResults = {};
                        for (var key in this.promises) {
                            this.promiseResults[key] = this.promises[key].result;
                        }
                        returned.push(React.createElement(Component, __assign({ key: "loaded" }, this.props, { refresh: this.refresh.bind(this) }, this.promiseResults)));
                    }
                    else {
                        if (this.state.isRefresh) {
                            returned.push(React.createElement(Component, __assign({ key: "loaded" }, this.props, { refresh: this.refresh.bind(this) }, this.promiseResults)));
                            returned.push(this.loader !== undefined ? this.loader : boot_utils_1.resolveLoader(this));
                        }
                        else {
                            returned.push(this.loader !== undefined ? this.loader : boot_utils_1.resolveLoader(this));
                        }
                    }
                    return returned.length === 0 ? null : (returned.length === 1 ? returned[0] : returned);
                };
                return AjaxLoader;
            }(React.Component)),
            _a.displayName = "AjaxLoader(" + (Component.displayName || Component.name) + ")",
            _a;
    };
};
//# sourceMappingURL=ajax-loader.js.map