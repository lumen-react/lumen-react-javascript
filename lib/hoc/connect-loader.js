"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var boot_utils_1 = require("../utils/boot-utils");
exports.connectLoader = function (options) {
    return function (Component) {
        var _a;
        return _a = (function (_super) {
                __extends(ConnectLoader, _super);
                function ConnectLoader(props) {
                    var _this = _super.call(this, props) || this;
                    _this.dispatchers = {};
                    _this.loader = null;
                    _this.loader = options && options.loader !== undefined ? options.loader : undefined;
                    _this.state = {
                        ready: !(options && options.dispatchers),
                    };
                    if (!_this.state.ready) {
                        var _loop_1 = function (key) {
                            this_1.dispatchers[key] = {
                                advance: function () { return _this.advance(key); },
                                done: false,
                                dispatcher: options.dispatchers[key],
                            };
                        };
                        var this_1 = this;
                        for (var key in options.dispatchers) {
                            _loop_1(key);
                        }
                        _this.state = { ready: false };
                    }
                    else {
                        _this.dispatchers = undefined;
                        _this.state = { ready: true };
                    }
                    if (_this.dispatchers) {
                        for (var key in _this.dispatchers) {
                            _this.dispatchers[key].dispatcher(props, _this.dispatchers[key].advance);
                        }
                    }
                    return _this;
                }
                ConnectLoader.prototype.advance = function (key) {
                    var _this = this;
                    setTimeout(function () {
                        _this.dispatchers[key].done = true;
                        var ready = true;
                        for (var key_1 in _this.dispatchers) {
                            if (!_this.dispatchers[key_1].done) {
                                ready = false;
                                break;
                            }
                        }
                        if (ready) {
                            _this.setState({ ready: true });
                        }
                    });
                };
                ConnectLoader.prototype.render = function () {
                    if (this.state.ready) {
                        return React.createElement(Component, __assign({}, this.props));
                    }
                    else {
                        return this.loader !== undefined ? this.loader : boot_utils_1.resolveLoader(this);
                    }
                };
                return ConnectLoader;
            }(React.Component)),
            _a.displayName = "ConnectLoader(" + (Component.displayName || Component.name) + ")",
            _a;
    };
};
//# sourceMappingURL=connect-loader.js.map