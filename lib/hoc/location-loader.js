"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.locationLoader = function (options) {
    return function (Component) {
        var _a;
        return _a = (function (_super) {
                __extends(LocationLoader, _super);
                function LocationLoader() {
                    var _this = _super !== null && _super.apply(this, arguments) || this;
                    _this.locationReady = false;
                    _this.hasLocation = false;
                    _this.init = false;
                    _this.lastCoords = null;
                    _this.triggerCoords = false;
                    return _this;
                }
                LocationLoader.prototype.render = function () {
                    var _this = this;
                    this.init = false;
                    if (!this.locationReady) {
                        if (this.props.isGeolocationEnabled && this.props.isGeolocationAvailable && this.props.coords) {
                            if (options && options.onLocationEnabled) {
                                options.onLocationEnabled(this.props.coords);
                            }
                            if (this.props.onLocationEnabled) {
                                this.props.onLocationEnabled(this.props.coords);
                            }
                            if (options && options.onLocationReady) {
                                options.onLocationReady(true, this.props.coords);
                            }
                            if (this.props.onLocationReady) {
                                this.props.onLocationReady(true, this.props.coords);
                            }
                            this.init = true;
                            this.locationReady = true;
                            this.hasLocation = true;
                        }
                        else if (!this.props.isGeolocationAvailable || !this.props.isGeolocationEnabled) {
                            if (options && options.onLocationDisabled) {
                                options.onLocationDisabled();
                            }
                            if (this.props.onLocationDisabled) {
                                this.props.onLocationDisabled();
                            }
                            if (options && options.onLocationReady) {
                                options.onLocationReady(false, null);
                            }
                            if (this.props.onLocationReady) {
                                this.props.onLocationReady(false, null);
                            }
                            this.init = true;
                            this.locationReady = true;
                            this.hasLocation = false;
                        }
                    }
                    this.triggerCoords = false;
                    if (this.props.coords) {
                        if (this.lastCoords == null || this.lastCoords.latitude != this.props.coords.latitude || this.lastCoords.longitude != this.props.coords.longitude) {
                            this.triggerCoords = true;
                            this.lastCoords = this.props.coords;
                            if (options && options.onLocationUpdated) {
                                options.onLocationUpdated(this.props.coords);
                            }
                            if (this.props.onLocationUpdated) {
                                this.props.onLocationUpdated(this.props.coords);
                            }
                        }
                    }
                    var injectedProps = {
                        locationReady: this.locationReady,
                        hasLocation: this.hasLocation,
                        handleLocationCallbacks: function (callbacks) {
                            if (_this.init) {
                                if (_this.props.isGeolocationEnabled && _this.props.isGeolocationAvailable && _this.props.coords) {
                                    if (callbacks.onLocationEnabled) {
                                        callbacks.onLocationEnabled(_this.props.coords);
                                    }
                                    if (callbacks.onLocationReady) {
                                        callbacks.onLocationReady(true, _this.props.coords);
                                    }
                                }
                                else if (!_this.props.isGeolocationAvailable || !_this.props.isGeolocationEnabled) {
                                    if (callbacks.onLocationDisabled) {
                                        callbacks.onLocationDisabled();
                                    }
                                    if (callbacks.onLocationReady) {
                                        callbacks.onLocationReady(false, null);
                                    }
                                }
                                _this.init = false;
                            }
                            if (_this.triggerCoords) {
                                if (callbacks.onLocationUpdated) {
                                    callbacks.onLocationUpdated(_this.props.coords);
                                }
                                _this.triggerCoords = false;
                            }
                        }
                    };
                    return React.createElement(Component, __assign({}, this.props, injectedProps));
                };
                return LocationLoader;
            }(React.Component)),
            _a.displayName = "LocationLoader(" + (Component.displayName || Component.name) + ")",
            _a;
    };
};
//# sourceMappingURL=location-loader.js.map