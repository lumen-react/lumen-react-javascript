"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GlobalActionType;
(function (GlobalActionType) {
    GlobalActionType["REFRESH_STORE"] = "REFRESH_STORE";
})(GlobalActionType = exports.GlobalActionType || (exports.GlobalActionType = {}));
exports.refreshStore = function (dispatch) {
    return function () { return dispatch({ type: GlobalActionType.REFRESH_STORE }); };
};
function refreshStoreDispatcherBinder(dispatch) {
    return { refreshStoreDispatcher: exports.refreshStore(dispatch) };
}
exports.refreshStoreDispatcherBinder = refreshStoreDispatcherBinder;
;
//# sourceMappingURL=global-actions.js.map