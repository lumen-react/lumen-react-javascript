"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_bootstrap_table_next_1 = require("react-bootstrap-table-next");
var react_bootstrap_table2_paginator_1 = require("react-bootstrap-table2-paginator");
var __1 = require("..");
var BootstrapTableWrapperComponent = (function (_super) {
    __extends(BootstrapTableWrapperComponent, _super);
    function BootstrapTableWrapperComponent(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.tableState = {};
        _this.tableState = props.state;
        return _this;
    }
    BootstrapTableWrapperComponent.prototype.shouldComponentUpdate = function () {
        return this.isRemote();
    };
    BootstrapTableWrapperComponent.prototype.isRemote = function () {
        return !!this.props.remote;
    };
    BootstrapTableWrapperComponent.prototype.onTableChangeCallback = function (onTableChange) {
        var _this = this;
        return function (type, newState) {
            newState = __1.removeUndefinedKeys(newState);
            _this.lastTableState = newState;
            _this.tableState = __1.mergeTableState(_this.tableState, newState, _this.props.remote);
            if (onTableChange) {
                onTableChange(type, _this.tableState);
            }
        };
    };
    BootstrapTableWrapperComponent.prototype.render = function () {
        var props = __assign({}, this.props);
        props.onTableChange = this.onTableChangeCallback(props.onTableChange);
        if (!this.isRemote()) {
            props.paginationOptions.onPageChange = props.onPaginationChange(props.onTableChange);
            props.paginationOptions.onSizePerPageChange = props.onPaginationChange(props.onTableChange);
            props.pagination = react_bootstrap_table2_paginator_1.default(props.paginationOptions);
            for (var i = 0; i < props.columns.length; i++) {
                props.columns[i].onSort = props.columns[i].onSort(props.onTableChange);
            }
            props.ref = props.onFilterChange(props.onTableChange);
        }
        var rowEvents = {};
        ['Click', 'DoubleClick', 'MouseEnter', 'MouseLeave'].forEach(function (event) {
            var rowEvent = "onRow" + event;
            if (props[rowEvent]) {
                rowEvents["on" + event] = props[rowEvent];
                delete props[rowEvent];
            }
        });
        props.rowEvents = rowEvents;
        return (React.createElement(react_bootstrap_table_next_1.default, __assign({}, props)));
    };
    return BootstrapTableWrapperComponent;
}(__1.AbstractComponent));
exports.BootstrapTableWrapperComponent = BootstrapTableWrapperComponent;
var BootstrapTableWrapper = (function (_super) {
    __extends(BootstrapTableWrapper, _super);
    function BootstrapTableWrapper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BootstrapTableWrapper;
}(BootstrapTableWrapperComponent));
exports.BootstrapTableWrapper = BootstrapTableWrapper;
var BootstrapTableWrapperStateless = (function (_super) {
    __extends(BootstrapTableWrapperStateless, _super);
    function BootstrapTableWrapperStateless() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BootstrapTableWrapperStateless;
}(BootstrapTableWrapperComponent));
exports.BootstrapTableWrapperStateless = BootstrapTableWrapperStateless;
//# sourceMappingURL=bootstrap-table-wrapper.js.map