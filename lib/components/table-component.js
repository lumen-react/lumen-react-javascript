"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var _ = require("lodash");
var __1 = require("..");
var bootstrap_table_wrapper_1 = require("./bootstrap-table-wrapper");
var recompose_1 = require("recompose");
var axios_1 = require("axios");
var TableComponent = (function (_super) {
    __extends(TableComponent, _super);
    function TableComponent(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.state = {
            tableData: 'tableInitializer' in _this.props && _this.props.tableInitializer && 'tableData' in
                _this.props.tableInitializer ? _this.props.tableInitializer.tableData : _this.props.tableData,
            tableState: _this.props.remote ? _this.props.tableInitializer.tableState :
                _this.props.writeToUrl ? __1.readTableStateFromUrl(_this.props.id, _this.props.tableState) :
                    _this.props.tableState,
        };
        return _this;
    }
    TableComponent.prototype.renderTable = function () {
        var _this = this;
        var onTableChange = function (type, remoteProps) {
            if (_this.props.requestConfigCallback) {
                if (_this.props.remote) {
                    _this.props.requestConfigCallback(__1.tableStateToRequestConfig(remoteProps, {}, _this.props.id)).then(function (data) { return _this.setState(data, function () {
                        if (_this.props.writeToUrl) {
                            __1.writeTableStateToUrl(_this.props.id, _this.state.tableState);
                        }
                        if (_this.props.onTableChange) {
                            _this.props.onTableChange(type, remoteProps);
                        }
                    }); }).catch(function (error) {
                        if (!axios_1.default.isCancel(error)) {
                            return Promise.reject(error);
                        }
                    });
                }
                else {
                    if (_this.props.writeToUrl) {
                        setTimeout(function () {
                            __1.writeTableStateToUrl(_this.props.id, remoteProps);
                            if (_this.props.onTableChange) {
                                _this.props.onTableChange(type, remoteProps);
                            }
                        });
                    }
                    else {
                        if (_this.props.onTableChange) {
                            _this.props.onTableChange(type, remoteProps);
                        }
                    }
                }
            }
            else {
                _this.props.remote && console.warn('No request config callback set');
            }
        };
        var tableDefinition = __assign({}, this.props.tableDefinition, {
            id: this.props.id,
            state: this.state.tableState,
            remote: this.props.remote,
            onTableChange: onTableChange,
            rowDefinitions: _.isFunction(this.props.rowDefinitions) ? this.props.rowDefinitions(this.props) :
                this.props.rowDefinitions,
        });
        var tableProps = __1.createTableProps(this.state.tableData, tableDefinition, this.props.messages, this.intl);
        var onRowClick = function (e, row, rowIndex) {
            if (_this.props.onRowClick) {
                _this.props.onRowClick({
                    id: row[tableProps.keyField],
                    history: _this.history,
                    e: e,
                    row: row,
                    rowIndex: rowIndex,
                    props: _this.props,
                });
            }
        };
        var props = __assign({}, _.omit(this.props, 'id', 'remote', 'tableData', 'tableState', 'messages', 'rowDefinitions', 'onTableChange', 'tableInitializer', 'requestConfigCallback', 'tableDefinition', 'writeToUrl'), { onRowClick: onRowClick });
        return React.createElement(bootstrap_table_wrapper_1.BootstrapTableWrapper, __assign({}, tableProps, props));
    };
    TableComponent.prototype.render = function () {
        return this.renderTable();
    };
    TableComponent.defaultProps = {
        id: 'default',
        messages: {},
        rowDefinitions: [],
        remote: true,
        tableData: [],
        tableState: {},
        writeToUrl: true,
    };
    return TableComponent;
}(__1.AbstractComponent));
exports.TableComponent = TableComponent;
var TableComponentTable = (function (_super) {
    __extends(TableComponentTable, _super);
    function TableComponentTable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TableComponentTable;
}(TableComponent));
exports.TableComponentTable = TableComponentTable;
function tableLoader(remoteTable, tableProps) {
    if (tableProps === void 0) { tableProps = { id: 'default' }; }
    return recompose_1.compose(recompose_1.defaultProps({
        messages: {},
        rowDefinitions: [],
        remote: true,
        tableState: {},
        writeToUrl: true,
    }), recompose_1.withProps(tableProps))(__1.connector(remoteTable, {
        ajaxLoaderOptions: {
            promises: {
                tableInitializer: function (props) {
                    return props.requestConfigCallback ? props.requestConfigCallback(props.remote ? __1.tableStateToRequestConfig(props.writeToUrl ? __1.readTableStateFromUrl(props.id, props.tableState) :
                        props.tableState, {}, props.id) : {}) : props.remote && console.warn('No request config callback set');
                }
            }
        }
    }));
}
exports.tableLoader = tableLoader;
//# sourceMappingURL=table-component.js.map