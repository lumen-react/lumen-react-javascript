"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var abstract_component_1 = require("./abstract-component");
var __1 = require("..");
var empty_1 = require("./empty");
var _ = require("lodash");
var LoaderWrapper = (function (_super) {
    __extends(LoaderWrapper, _super);
    function LoaderWrapper() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            loadingPromise: false,
        };
        return _this;
    }
    LoaderWrapper.prototype.render = function () {
        var _this = this;
        var _a = this.props, showLoader = _a.showLoader, asOverlay = _a.asOverlay, children = _a.children;
        if (_.isBoolean(showLoader)) {
            return (React.createElement(empty_1.default, null,
                (!showLoader || asOverlay) && children,
                showLoader && this.renderLoader()));
        }
        else if (_.isFunction(showLoader)) {
            var promiseCallback = function () {
                return new Promise(function (resolve, reject) {
                    _this.setState({ loadingPromise: true }, function () {
                        var passedPromise = showLoader();
                        passedPromise.then(function (d) {
                            _this.setState({ loadingPromise: false }, function () { return resolve(d); });
                        }).catch(function (e) {
                            _this.setState({ loadingPromise: false }, function () { return reject(e); });
                        });
                    });
                });
            };
            return (React.createElement(empty_1.default, null,
                (!this.state.loadingPromise || asOverlay) && children(promiseCallback),
                this.state.loadingPromise && this.renderLoader()));
        }
        else {
            return _.isFunction(children) ? children(function () { return undefined; }) : children;
        }
    };
    LoaderWrapper.prototype.renderLoader = function () {
        return (React.createElement("div", { style: {
                zIndex: 1,
                position: 'absolute',
                left: 0,
                top: 0,
                display: 'flex',
                width: '100%',
                height: '100%',
            } }, __1.resolveLoader(this)));
    };
    LoaderWrapper.defaultProps = {
        asOverlay: false,
    };
    return LoaderWrapper;
}(abstract_component_1.AbstractComponent));
exports.default = LoaderWrapper;
//# sourceMappingURL=loader-wrapper.js.map