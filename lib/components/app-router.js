"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var PropTypes = require("prop-types");
var react_router_dom_1 = require("react-router-dom");
var react_router_1 = require("react-router");
var AppRouter = (function (_super) {
    __extends(AppRouter, _super);
    function AppRouter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppRouter.prototype.render = function () {
        var _a = this.props, children = _a.children, props = __rest(_a, ["children"]);
        var AppRouterInjectorComponent = react_router_1.withRouter(AppRouterInjector);
        return (React.createElement(react_router_dom_1.BrowserRouter, __assign({}, props),
            React.createElement(AppRouterInjectorComponent, null, children)));
    };
    return AppRouter;
}(React.Component));
exports.default = AppRouter;
var AppRouterInjector = (function (_super) {
    __extends(AppRouterInjector, _super);
    function AppRouterInjector() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppRouterInjector.prototype.getChildContext = function () {
        return {
            match: this.props.match,
            location: this.props.location,
            history: this.props.history,
        };
    };
    AppRouterInjector.prototype.render = function () {
        return this.props.children;
    };
    AppRouterInjector.childContextTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };
    return AppRouterInjector;
}(React.Component));
//# sourceMappingURL=app-router.js.map