"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var PropTypes = require("prop-types");
var AbstractComponent = (function (_super) {
    __extends(AbstractComponent, _super);
    function AbstractComponent(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.intl = _this.context.intl;
        _this.match = _this.context.match;
        _this.location = _this.context.location;
        _this.history = _this.context.history;
        var preparedState = _this.prepareState();
        if (preparedState) {
            _this.state = preparedState;
        }
        return _this;
    }
    AbstractComponent.prototype.prepareState = function () {
        return null;
    };
    AbstractComponent.prototype.setDelayedState = function (clearState) {
        var _this = this;
        if (clearState === void 0) { clearState = {}; }
        var currentState = __assign({}, this.state);
        this.state = __assign({}, currentState, clearState);
        setTimeout(function () { return _this.setState(currentState); });
    };
    AbstractComponent.contextTypes = {
        intl: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };
    return AbstractComponent;
}(React.Component));
exports.AbstractComponent = AbstractComponent;
var AbstractStatelessComponent = (function (_super) {
    __extends(AbstractStatelessComponent, _super);
    function AbstractStatelessComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractStatelessComponent;
}(AbstractComponent));
exports.AbstractStatelessComponent = AbstractStatelessComponent;
var AbstractSimpleComponent = (function (_super) {
    __extends(AbstractSimpleComponent, _super);
    function AbstractSimpleComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractSimpleComponent;
}(AbstractComponent));
exports.AbstractSimpleComponent = AbstractSimpleComponent;
var AbstractPureComponent = (function (_super) {
    __extends(AbstractPureComponent, _super);
    function AbstractPureComponent(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.intl = _this.context.intl;
        _this.match = _this.context.match;
        _this.location = _this.context.location;
        _this.history = _this.context.history;
        var preparedState = _this.prepareState();
        if (preparedState) {
            _this.state = preparedState;
        }
        return _this;
    }
    AbstractPureComponent.prototype.prepareState = function () {
        return null;
    };
    AbstractPureComponent.prototype.setDelayedState = function (clearState) {
        var _this = this;
        if (clearState === void 0) { clearState = {}; }
        var currentState = __assign({}, this.state);
        this.state = __assign({}, currentState, clearState);
        setTimeout(function () { return _this.setState(currentState); });
    };
    AbstractPureComponent.contextTypes = {
        intl: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };
    return AbstractPureComponent;
}(React.PureComponent));
exports.AbstractPureComponent = AbstractPureComponent;
var AbstractStatelessPureComponent = (function (_super) {
    __extends(AbstractStatelessPureComponent, _super);
    function AbstractStatelessPureComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractStatelessPureComponent;
}(AbstractPureComponent));
exports.AbstractStatelessPureComponent = AbstractStatelessPureComponent;
var AbstractSimplePureComponent = (function (_super) {
    __extends(AbstractSimplePureComponent, _super);
    function AbstractSimplePureComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AbstractSimplePureComponent;
}(AbstractPureComponent));
exports.AbstractSimplePureComponent = AbstractSimplePureComponent;
//# sourceMappingURL=abstract-component.js.map