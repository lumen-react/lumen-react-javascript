"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var abstract_component_1 = require("./abstract-component");
var _ = require("lodash");
var __1 = require("..");
var TabsComponent = (function (_super) {
    __extends(TabsComponent, _super);
    function TabsComponent(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.initialRenderers = {};
        _this.contentRenderers = {};
        _this.loaders = {};
        _this.renderedContent = {};
        _this.renderedLoader = {};
        _this.handleSelect = _this.handleSelect.bind(_this);
        _this.state = { key: null };
        return _this;
    }
    TabsComponent.prototype.componentDidMount = function () {
        var _this = this;
        var defaultKey = null;
        if (this.props.writeToUrl) {
            defaultKey = __1.readFromHash(this.props.id);
        }
        var defaultTabDef = this.props.tabsDefinition.find(function (tabDefinition) { return tabDefinition.isDefault; });
        if (defaultTabDef) {
            defaultKey = defaultTabDef.eventKey;
        }
        if (!defaultKey && this.props.defaultOpen && this.props.tabsDefinition[0]) {
            defaultKey = this.props.tabsDefinition[0].eventKey;
        }
        this.props.tabsDefinition
            .filter(function (tabDefinition) { return !('enabled' in tabDefinition) || tabDefinition.enabled; })
            .forEach(function (tabDefinition) {
            var eventKey = tabDefinition.eventKey;
            _this.initialRenderers[eventKey] = tabDefinition.contentRenderer;
            _this.contentRenderers[eventKey] = tabDefinition.contentRenderer;
            _this.loaders[eventKey] = tabDefinition.loader;
            if (defaultKey != eventKey) {
                var preload = 'preloadTab' in tabDefinition ? tabDefinition.preloadTab :
                    ('preloadTabs' in _this.props ? _this.props.preloadTabs : false);
                if (preload) {
                    _this.loadTab(tabDefinition);
                }
            }
        });
        var defaultTab = this.props.tabsDefinition
            .filter(function (tabDefinition) { return tabDefinition.eventKey == defaultKey; })[0];
        this.handleSelect(defaultTab.eventKey);
    };
    TabsComponent.prototype.componentDidUpdate = function () {
        var _this = this;
        this.props.tabsDefinition
            .filter(function (tabDefinition) { return !('enabled' in tabDefinition) || tabDefinition.enabled; })
            .forEach(function (tabDefinition) {
            var eventKey = tabDefinition.eventKey;
            if (!_this.initialRenderers[eventKey]) {
                _this.initialRenderers[eventKey] = tabDefinition.contentRenderer;
            }
            if (!_this.contentRenderers[eventKey]) {
                _this.contentRenderers[eventKey] = tabDefinition.contentRenderer;
            }
            if (!_this.loaders[eventKey]) {
                _this.loaders[eventKey] = tabDefinition.loader;
            }
            var preload = 'preloadTab' in tabDefinition ? tabDefinition.preloadTab :
                ('preloadTabs' in _this.props ? _this.props.preloadTabs : false);
            if (preload) {
                _this.loadTab(tabDefinition);
            }
        });
    };
    TabsComponent.prototype.handleSelect = function (key) {
        var _this = this;
        var tabDefinition = this.props.tabsDefinition.find(function (t) { return t.eventKey == key; });
        this.loadTab(tabDefinition);
        this.setState({ key: key }, function () {
            var _a;
            if (_this.props.writeToUrl) {
                __1.writeToHash((_a = {}, _a[_this.props.id] = tabDefinition.eventKey, _a));
            }
        });
    };
    TabsComponent.prototype.loadTab = function (tabDefinition, fromRender) {
        var _this = this;
        if (fromRender === void 0) { fromRender = false; }
        var key = tabDefinition.eventKey;
        var cache = !('cache' in tabDefinition) ? false : tabDefinition.cache;
        var contentPromise = !('promise' in tabDefinition) ? false : tabDefinition.promise;
        var loaderPromise = !('loaderPromise' in tabDefinition) ? false : tabDefinition.loaderPromise;
        var nullOnDisabled = !('nullOnDisabled' in tabDefinition) ? true : tabDefinition.nullOnDisabled;
        if (tabDefinition.disabled && nullOnDisabled) {
            this.renderedContent[key] = null;
            return;
        }
        var content = this.contentRenderers[key];
        var loader = this.loaders[key];
        if (_.isFunction(content)) {
            if (!contentPromise || (contentPromise && !fromRender)) {
                content = content();
                if (_.isObject(content) && 'then' in content) {
                    this.renderedContent[key] = null;
                    this.contentRenderers[key] = null;
                    content.then(function (renderer) {
                        _this.contentRenderers[key] = renderer;
                        setTimeout(function () { return _this.loadTab(tabDefinition, fromRender); });
                    });
                }
                else {
                    if (!_.isEqual(this.renderedContent[key], content)) {
                        this.renderedContent[key] = content;
                        if (!fromRender) {
                            this.forceUpdate(function () {
                                if (!cache) {
                                    _this.contentRenderers[key] = _this.initialRenderers[key];
                                }
                            });
                        }
                    }
                }
            }
        }
        else if (!(_.isObject(content) && 'then' in content)) {
            if (!_.isEqual(this.renderedContent[key], content)) {
                this.renderedContent[key] = content;
                if (!fromRender) {
                    this.forceUpdate(function () {
                        if (!cache) {
                            _this.contentRenderers[key] = _this.initialRenderers[key];
                        }
                    });
                }
            }
        }
        if (_.isFunction(loader)) {
            if (!loaderPromise || (loaderPromise && !fromRender)) {
                loader = loader();
                if (_.isObject(loader) && 'then' in loader) {
                    this.renderedLoader[key] = null;
                    this.loaders[key] = null;
                    loader.then(function (renderer) {
                        _this.loaders[key] = renderer;
                        setTimeout(function () { return _this.loadTab(tabDefinition, fromRender); });
                    });
                }
                else {
                    if (!_.isEqual(this.renderedLoader[key], loader)) {
                        this.renderedLoader[key] = loader;
                        if (!fromRender) {
                            this.forceUpdate();
                        }
                    }
                }
            }
        }
        else if (!(_.isObject(loader) && 'then' in loader)) {
            if (!_.isEqual(this.renderedLoader[key], loader)) {
                this.renderedLoader[key] = loader;
                if (!fromRender) {
                    this.forceUpdate();
                }
            }
        }
    };
    TabsComponent.prototype.render = function () {
        return this.renderTabs();
    };
    TabsComponent.prototype.renderTabs = function () {
        var _this = this;
        var props = _.omit(this.props, 'children', 'tabsDefinition', 'writeToUrl', 'defaultOpen', 'preloadTabs');
        var children = [];
        this.props.tabsDefinition
            .filter(function (tabDefinition) { return !('enabled' in tabDefinition) || tabDefinition.enabled; })
            .forEach(function (tabDefinition) {
            var eventKey = tabDefinition.eventKey;
            var tabProps = _.omit(tabDefinition, 'cache', 'loader', 'contentRenderer', 'isDefault', 'nullOnDisabled', 'preloadTab', 'promise', 'loaderPromise', 'enabled');
            _this.loadTab(tabDefinition, true);
            var content = _this.renderedContent[eventKey] ||
                _this.renderedLoader[eventKey];
            children.push(React.cloneElement(React.createElement(react_bootstrap_1.Tab, __assign({ key: eventKey }, tabProps), content)));
        });
        return (React.createElement(react_bootstrap_1.Tabs, __assign({}, props, { activeKey: this.state.key, onSelect: this.handleSelect }), children));
    };
    TabsComponent.defaultProps = {
        writeToUrl: true,
        defaultOpen: true,
    };
    return TabsComponent;
}(abstract_component_1.AbstractComponent));
exports.TabsComponent = TabsComponent;
//# sourceMappingURL=tabs-component.js.map