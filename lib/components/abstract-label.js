"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
var React = require("react");
var abstract_component_1 = require("./abstract-component");
var LabelType;
(function (LabelType) {
    LabelType[LabelType["SIMPLE"] = 0] = "SIMPLE";
    LabelType[LabelType["SMALL"] = 1] = "SMALL";
    LabelType[LabelType["MEDIUM"] = 2] = "MEDIUM";
    LabelType[LabelType["LARGE"] = 3] = "LARGE";
    LabelType[LabelType["SEMIFULL"] = 4] = "SEMIFULL";
    LabelType[LabelType["FULL"] = 5] = "FULL";
})(LabelType = exports.LabelType || (exports.LabelType = {}));
var AbstractLabel = (function (_super) {
    __extends(AbstractLabel, _super);
    function AbstractLabel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AbstractLabel.prototype.getCleanedProps = function () {
        return this.props;
    };
    AbstractLabel.prototype.renderThumbnail = function () {
        return this.hasThumbnail() ?
            React.createElement("img", { src: this.getThumbnail(), title: this.getName() }) :
            this.renderMissingThumbnal();
    };
    AbstractLabel.prototype.renderSimpleLabel = function (elementProps, avatarProps, descriptionProps) {
        return this.props.thumbnailOnly ? null : React.createElement("div", __assign({}, elementProps), this.getName());
    };
    AbstractLabel.prototype.renderSmallLabel = function (elementProps, avatarProps, descriptionProps) {
        return (React.createElement("div", __assign({}, elementProps),
            this.props.renderThumbnail && (React.createElement("div", __assign({}, avatarProps), this.renderThumbnail())),
            this.props.thumbnailOnly ? null : (React.createElement("div", __assign({}, descriptionProps),
                React.createElement("div", null, this.getName()))),
            this.props.children));
    };
    AbstractLabel.prototype.renderMediumLabel = function (elementProps, avatarProps, descriptionProps) {
        return (React.createElement("div", __assign({}, elementProps),
            this.props.renderThumbnail && (React.createElement("div", __assign({}, avatarProps), this.renderThumbnail())),
            this.props.thumbnailOnly ? null : (React.createElement("div", __assign({}, descriptionProps),
                React.createElement("div", null, this.getName()))),
            this.props.children));
    };
    AbstractLabel.prototype.renderLargeLabel = function (elementProps, avatarProps, descriptionProps) {
        return (React.createElement("div", __assign({}, elementProps),
            this.props.renderThumbnail && (React.createElement("div", { className: "avatar-container" },
                React.createElement("div", __assign({}, avatarProps), this.renderThumbnail()))),
            this.props.thumbnailOnly ? null : (React.createElement("div", { className: "description-container" },
                React.createElement("div", __assign({}, descriptionProps),
                    React.createElement("div", null, this.getName()),
                    React.createElement("div", { className: "sub" }, this.getSub())))),
            this.props.children));
    };
    AbstractLabel.prototype.renderSemiFullLabel = function (elementProps, avatarProps, descriptionProps) {
        return (React.createElement("div", __assign({}, elementProps),
            this.props.renderThumbnail && (React.createElement("div", { className: "avatar-container" },
                React.createElement("div", __assign({}, avatarProps), this.renderThumbnail()))),
            this.props.thumbnailOnly ? null : (React.createElement("div", { className: "description-container" },
                React.createElement("div", __assign({}, descriptionProps),
                    React.createElement("h4", null, this.getName()),
                    React.createElement("div", { className: "content" }, this.getContent())))),
            this.props.children));
    };
    AbstractLabel.prototype.renderFullLabel = function (elementProps, avatarProps, descriptionProps) {
        return (React.createElement("div", __assign({}, elementProps),
            this.props.renderThumbnail && (React.createElement("div", { className: "avatar-container" },
                React.createElement("div", __assign({}, avatarProps), this.renderThumbnail()))),
            this.props.thumbnailOnly ? null : (React.createElement("div", { className: "description-container" },
                React.createElement("div", __assign({}, descriptionProps),
                    React.createElement("h4", null, this.getName()),
                    React.createElement("div", { className: "sub" }, this.getSub()),
                    React.createElement("div", { className: "content" }, this.getContent())))),
            this.props.children));
    };
    AbstractLabel.prototype.render = function () {
        var cleanedProps = this.getCleanedProps();
        var _a = cleanedProps, labelType = _a.labelType, avatarProps = _a.avatarProps, descriptionProps = _a.descriptionProps, thumbnailOnly = _a.thumbnailOnly, renderThumbnail = _a.renderThumbnail, elementProps = __rest(_a, ["labelType", "avatarProps", "descriptionProps", "thumbnailOnly", "renderThumbnail"]);
        elementProps.className = elementProps.className ? elementProps.className + " object-label" : 'object-label';
        var avatarObject = __assign({}, avatarProps);
        var descriptionObject = __assign({}, descriptionProps);
        var sizeClassName = "";
        switch (labelType) {
            case LabelType.FULL:
            case LabelType.SEMIFULL:
                sizeClassName = "full";
                break;
            case LabelType.LARGE:
                sizeClassName = "large";
                break;
            case LabelType.MEDIUM:
                sizeClassName = "medium";
                break;
            case LabelType.SMALL:
                sizeClassName = "small";
                break;
            case LabelType.SIMPLE:
            default:
                sizeClassName = "simple";
                break;
        }
        avatarObject.className = avatarObject.className ? avatarObject.className + " thumbnail avatar avatar-" + sizeClassName : "thumbnail avatar avatar-" + sizeClassName;
        descriptionObject.className = descriptionObject.className ? descriptionObject.className + " description description-" + sizeClassName + (renderThumbnail ? '' : ' no-avatar') : "description description-" + sizeClassName + (renderThumbnail ? '' : ' no-avatar');
        switch (labelType) {
            case LabelType.FULL:
                return this.renderFullLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.SEMIFULL:
                return this.renderSemiFullLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.LARGE:
                return this.renderLargeLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.MEDIUM:
                return this.renderMediumLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.SMALL:
                return this.renderSmallLabel(elementProps, avatarObject, descriptionObject);
            default:
                return this.renderSimpleLabel(elementProps, avatarObject, descriptionObject);
        }
    };
    AbstractLabel.defaultProps = {
        labelType: LabelType.SIMPLE,
        thumbnailOnly: false,
        renderThumbnail: true,
    };
    AbstractLabel.sizeKeyMap = (_a = {},
        _a[LabelType.SMALL] = '20x20',
        _a[LabelType.MEDIUM] = '40x40',
        _a[LabelType.LARGE] = '60x60',
        _a[LabelType.SEMIFULL] = '100x100',
        _a[LabelType.FULL] = '100x100',
        _a);
    return AbstractLabel;
}(abstract_component_1.AbstractComponent));
exports.default = AbstractLabel;
//# sourceMappingURL=abstract-label.js.map