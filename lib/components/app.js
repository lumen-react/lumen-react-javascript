"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var PropTypes = require("prop-types");
var react_intl_1 = require("react-intl");
var recompose_1 = require("recompose");
var axios_utils_1 = require("../utils/axios-utils");
var window_utils_1 = require("../utils/window-utils");
var App = (function (_super) {
    __extends(App, _super);
    function App(props, context) {
        var _this = _super.call(this, props, context) || this;
        window_utils_1.setupKeyRegister();
        axios_utils_1.setupAxiosInstance(_this.props.axios, _this.props.axiosDefaultRequestConfig);
        return _this;
    }
    App.prototype.getChildContext = function () {
        return {
            intl: this.props.intl,
        };
    };
    App.prototype.render = function () {
        return this.props.children;
    };
    App.childContextTypes = {
        intl: PropTypes.object.isRequired,
    };
    return App;
}(React.Component));
;
function appComposer() {
    var functions = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        functions[_i] = arguments[_i];
    }
    return recompose_1.compose.apply(void 0, [react_intl_1.injectIntl].concat(functions))(App);
}
exports.appComposer = appComposer;
//# sourceMappingURL=app.js.map