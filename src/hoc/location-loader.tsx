import * as React from 'react';
import {GeolocatedProps} from "react-geolocated";

// State of the HOC you need to compute the InjectedProps
export interface LocationLoaderState {
}

// Props you want the resulting component to take (besides the props of the wrapped component)
export interface LocationLoaderExternalProps extends LocationLoaderOptions, GeolocatedProps {
}

// Props the HOC adds to the wrapped component
export interface LocationLoaderInjectedProps {
    locationReady?: boolean;
    hasLocation?: boolean;
    handleLocationCallbacks?: (
        callbacks: LocationLoaderOptions
    ) => void;
}

// Options for the HOC factory that are not dependent on props values
export interface LocationLoaderOptions {
    onLocationEnabled?: (coords: Coordinates) => void;
    onLocationUpdated?: (coords: Coordinates) => void;
    onLocationDisabled?: () => void;
    onLocationReady?: (enabled: boolean, coords: Coordinates) => void;
}

export const locationLoader = (options?: LocationLoaderOptions) =>
    <T extends {}>(Component: React.ComponentType<T & LocationLoaderInjectedProps>) => {
        // Do something with the options here or some side effects

        type Props = T & LocationLoaderExternalProps;

        return class LocationLoader extends React.Component<Props, LocationLoaderState> {
            // Define how your HOC is shown in ReactDevTools
            static displayName = `LocationLoader(${Component.displayName || Component.name})`;

            locationReady = false;
            hasLocation = false;
            init = false;
            lastCoords = null;
            triggerCoords = false;

            // Implement other methods here

            render() {
                this.init = false;
                if (!this.locationReady) {
                    if (this.props.isGeolocationEnabled && this.props.isGeolocationAvailable && this.props.coords) {
                        if (options && options.onLocationEnabled) {
                            options.onLocationEnabled(this.props.coords);
                        }
                        if (this.props.onLocationEnabled) {
                            this.props.onLocationEnabled(this.props.coords);
                        }
                        if (options && options.onLocationReady) {
                            options.onLocationReady(true, this.props.coords);
                        }
                        if (this.props.onLocationReady) {
                            this.props.onLocationReady(true, this.props.coords);
                        }
                        this.init = true;
                        this.locationReady = true;
                        this.hasLocation = true;
                    } else if (!this.props.isGeolocationAvailable || !this.props.isGeolocationEnabled) {
                        if (options && options.onLocationDisabled) {
                            options.onLocationDisabled();
                        }
                        if (this.props.onLocationDisabled) {
                            this.props.onLocationDisabled();
                        }
                        if (options && options.onLocationReady) {
                            options.onLocationReady(false, null);
                        }
                        if (this.props.onLocationReady) {
                            this.props.onLocationReady(false, null);
                        }
                        this.init = true;
                        this.locationReady = true;
                        this.hasLocation = false;
                    }
                }

                this.triggerCoords = false;
                if (this.props.coords) {
                    if (this.lastCoords == null || this.lastCoords.latitude != this.props.coords.latitude || this.lastCoords.longitude != this.props.coords.longitude) {
                        this.triggerCoords = true;
                        this.lastCoords = this.props.coords;

                        if (options && options.onLocationUpdated) {
                            options.onLocationUpdated(this.props.coords);
                        }
                        if (this.props.onLocationUpdated) {
                            this.props.onLocationUpdated(this.props.coords);
                        }
                    }
                }

                let injectedProps: LocationLoaderInjectedProps = {
                    locationReady: this.locationReady,
                    hasLocation: this.hasLocation,
                    handleLocationCallbacks: (callbacks: {
                        onLocationEnabled?: (coords: Coordinates) => void,
                        onLocationUpdated?: (coords: Coordinates) => void,
                        onLocationDisabled?: () => void,
                        onLocationReady?: (enabled: boolean, coords: Coordinates) => void
                    }) => {
                        if (this.init) {
                            if (this.props.isGeolocationEnabled && this.props.isGeolocationAvailable && this.props.coords) {
                                if (callbacks.onLocationEnabled) {
                                    callbacks.onLocationEnabled(this.props.coords);
                                }
                                if (callbacks.onLocationReady) {
                                    callbacks.onLocationReady(true, this.props.coords);
                                }
                            } else if (!this.props.isGeolocationAvailable || !this.props.isGeolocationEnabled) {
                                if (callbacks.onLocationDisabled) {
                                    callbacks.onLocationDisabled();
                                }
                                if (callbacks.onLocationReady) {
                                    callbacks.onLocationReady(false, null);
                                }
                            }
                            this.init = false;
                        }

                        if (this.triggerCoords) {
                            if (callbacks.onLocationUpdated) {
                                callbacks.onLocationUpdated(this.props.coords);
                            }
                            this.triggerCoords = false;
                        }

                    }
                };

                return <Component {...this.props} {...injectedProps}/>;
            }
        }
    };