import * as React from 'react';
import {addListener} from '../utils/event-utils';
import {resolveLoader} from '../utils/boot-utils';

// State of the HOC you need to compute the InjectedProps
export interface EventLoaderState {
    ready: boolean;
    passedProps: {};
}

// Props you want the resulting component to take (besides the props of the wrapped component)
export interface EventLoaderExternalProps {
}

// Props the HOC adds to the wrapped component
export interface EventLoaderInjectedProps {
}

// Options for the HOC factory that are not dependent on props values
export interface EventLoaderOptions {
    requireTriggers?: boolean | Array<string> | string;
    types: Array<string> | string;
    mapEventDetailsToProps: <T = { [key: string]: any }>(detail: T, type: string) => { [key: string]: any };
    loader?: any;
}

export const eventLoader = (options: EventLoaderOptions) =>
    <T extends {}>(Component: React.ComponentType<T & EventLoaderInjectedProps>) => {
        // Do something with the options here or some side effects

        type Props = T & EventLoaderExternalProps;

        return class EventLoader extends React.Component<Props, EventLoaderState> {
            // Define how your HOC is shown in ReactDevTools
            static displayName = `EventLoader(${Component.displayName || Component.name})`;

            readonly ready = [];
            readonly loader = null;

            constructor(props: Props) {
                super(props);
                this.loader = options && options.loader !== undefined ? options.loader : undefined;

                this.state = {
                    ready: (!('requireTriggers' in options)) || options.requireTriggers === false,
                    passedProps: {},
                };

                if (typeof options.types === 'string') {
                    options.types = [options.types];
                }

                for (let index in options.types) {
                    addListener(options.types[index], (data) => {
                        this.setState({
                            passedProps: {
                                ...this.state.passedProps, ...options.mapEventDetailsToProps(data, options.types[index])
                            }
                        });
                    });
                }

                if (!this.state.ready) {
                    if (options.requireTriggers === true) {
                        options.requireTriggers = options.types;
                    }
                    else if (typeof options.requireTriggers === 'string') {
                        options.requireTriggers = [options.requireTriggers];
                    }

                    for (let index in options.requireTriggers as Array<string>) {
                        addListener(options.requireTriggers[index], () => {
                            this.ready.push(index);
                            if (this.ready.length === (options.requireTriggers as Array<string>).length) {
                                this.setState({ready: true});
                            }
                        }, {once: true});
                    }
                }
            }

            // Implement other methods here

            render() {
                if (this.state.ready) {
                    return <Component {...this.props} {...this.state.passedProps}/>;
                }
                else {
                    return this.loader !== undefined ? this.loader : resolveLoader(this);
                }
            }
        };
    };