import * as React from 'react';
import {resolveLoader} from '../utils/boot-utils';

// State of the HOC you need to compute the InjectedProps
export interface AjaxLoaderState {
    ready: boolean;
    isRefresh: boolean;
}

// Props you want the resulting component to take (besides the props of the wrapped component)
export interface AjaxLoaderExternalProps {
}

// Props the HOC adds to the wrapped component
export interface AjaxLoaderInjectedProps {
    refresh?: () => void;
}

// Options for the HOC factory that are not dependent on props values
export interface AjaxLoaderOptions {
    promises: { [key: string]: (props) => PromiseLike<any> };
    loader?: any;
}

export const ajaxLoader = (options: AjaxLoaderOptions) =>
    <T extends {}>(Component: React.ComponentType<T & AjaxLoaderInjectedProps>) => {
        // Do something with the options here or some side effects

        type Props = T & AjaxLoaderExternalProps;

        return class AjaxLoader extends React.Component<Props, AjaxLoaderState> {
            // Define how your HOC is shown in ReactDevTools
            static displayName = `AjaxLoader(${Component.displayName || Component.name})`;

            promises: { [key: string]: { promise: PromiseLike<any>, result: any, done: boolean } } = {};
            promiseResults: { [key: string]: any };
            readonly loader = null;

            constructor(props: Props) {
                super(props);
                this.loader = options && options.loader !== undefined ? options.loader : undefined;

                this.state = {
                    isRefresh: false,
                    ready: !(options && options.promises),
                };

                if (!this.state.ready) {
                    for (let key in options.promises) {
                        if (options.promises[key]) {
                            this.promises[key] = {
                                result: null,
                                done: false,
                                promise: options.promises[key](props),
                            };
                        }
                    }
                    this.state = {isRefresh: false, ready: false};
                }
                else {
                    this.promises = undefined;
                    this.state = {isRefresh: false, ready: true};
                }

                if (this.promises) {
                    for (let key in this.promises) {
                        if (this.promises[key].promise) {
                            this.promises[key].promise.then((data) => {
                                this.promises[key].done = true;
                                this.promises[key].result = data;
                                let ready = true;
                                for (let check in this.promises) {
                                    if (!this.promises[check].done) {
                                        ready = false;
                                        break;
                                    }
                                }
                                if (ready) {
                                    this.setState({ready: true});
                                }
                            });
                        }
                        else {
                            this.promises[key].done = true;
                            let ready = true;
                            for (let check in this.promises) {
                                if (!this.promises[check].done) {
                                    ready = false;
                                    break;
                                }
                            }
                            if (ready) {
                                setTimeout(() => this.setState({ready: true}));
                            }
                        }
                    }
                }
            }

            // Implement other methods here

            refresh() {
                this.promises = {};
                this.setState({
                    isRefresh: true,
                    ready: !(options && options.promises),
                }, () => {
                    if (!this.state.ready) {
                        for (let key in options.promises) {
                            if (options.promises[key]) {
                                this.promises[key] = {
                                    result: null,
                                    done: false,
                                    promise: options.promises[key](this.props),
                                };
                            }
                        }
                    }
                    else {
                        this.promises = undefined;
                        this.setState({ready: true});
                    }

                    if (this.promises) {
                        for (let key in this.promises) {
                            if (this.promises[key].promise) {
                                this.promises[key].promise.then((data) => {
                                    this.promises[key].done = true;
                                    this.promises[key].result = data;
                                    let ready = true;
                                    for (let check in this.promises) {
                                        if (!this.promises[check].done) {
                                            ready = false;
                                            break;
                                        }
                                    }
                                    if (ready) {
                                        this.setState({ready: true});
                                    }
                                });
                            }
                            else {
                                this.promises[key].done = true;
                                let ready = true;
                                for (let check in this.promises) {
                                    if (!this.promises[check].done) {
                                        ready = false;
                                        break;
                                    }
                                }
                                if (ready) {
                                    setTimeout(() => this.setState({ready: true}));
                                }
                            }
                        }
                    }
                });
            }

            render() {
                let returned = [];
                if (this.state.ready) {
                    this.promiseResults = {};
                    for (let key in this.promises) {
                        this.promiseResults[key] = this.promises[key].result;
                    }
                    returned.push(<Component key="loaded" {...this.props}
                                             refresh={this.refresh.bind(this)} {...this.promiseResults}/>);
                }
                else {
                    if (this.state.isRefresh) {
                        returned.push(<Component key="loaded" {...this.props}
                                                 refresh={this.refresh.bind(this)} {...this.promiseResults}/>);
                        returned.push(this.loader !== undefined ? this.loader : resolveLoader(this));
                    }
                    else {
                        returned.push(this.loader !== undefined ? this.loader : resolveLoader(this));
                    }
                }
                return returned.length === 0 ? null : (returned.length === 1 ? returned[0] : returned);
            }
        };
    };