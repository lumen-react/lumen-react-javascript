import * as React from 'react';
import {resolveLoader} from '../utils/boot-utils';

// State of the HOC you need to compute the InjectedProps
export interface ConnectLoaderState {
    ready: boolean;
}

// Props you want the resulting component to take (besides the props of the wrapped component)
export interface ConnectLoaderExternalProps {
}

// Props the HOC adds to the wrapped component
export interface ConnectLoaderInjectedProps {
}

// Options for the HOC factory that are not dependent on props values
export interface ConnectLoaderOptions {
    dispatchers: { [key: string]: (props, advance: () => void) => any };
    loader?: any;
}

export const connectLoader = (options: ConnectLoaderOptions) =>
    <T extends {}>(Component: React.ComponentType<T & ConnectLoaderInjectedProps>) => {
        // Do something with the options here or some side effects

        type Props = T & ConnectLoaderExternalProps;

        return class ConnectLoader extends React.Component<Props, ConnectLoaderState> {
            // Define how your HOC is shown in ReactDevTools
            static displayName = `ConnectLoader(${Component.displayName || Component.name})`;

            readonly dispatchers: { [key: string]: any } = {};
            readonly loader = null;

            constructor(props: Props) {
                super(props);
                this.loader = options && options.loader !== undefined ? options.loader : undefined;

                this.state = {
                    ready: !(options && options.dispatchers),
                };

                if (!this.state.ready) {
                    for (let key in options.dispatchers) {
                        this.dispatchers[key] = {
                            advance: () => this.advance(key),
                            done: false,
                            dispatcher: options.dispatchers[key],
                        };
                    }
                    this.state = {ready: false};
                } else {
                    this.dispatchers = undefined;
                    this.state = {ready: true};
                }

                if (this.dispatchers) {
                    for (let key in this.dispatchers) {
                        this.dispatchers[key].dispatcher(props, this.dispatchers[key].advance);
                    }
                }
            }

            advance(key) {
                setTimeout(() => {
                    this.dispatchers[key].done = true;
                    let ready = true;
                    for (let key in this.dispatchers) {
                        if (!this.dispatchers[key].done) {
                            ready = false;
                            break;
                        }
                    }
                    if (ready) {
                        this.setState({ready: true});
                    }
                });
            }

            // Implement other methods here

            render() {
                if (this.state.ready) {
                    return <Component {...this.props} />;
                } else {
                    return this.loader !== undefined ? this.loader : resolveLoader(this);
                }
            }
        };
    };