import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from "./abstract-form-field";

export interface FormFieldInputProps extends FormFieldBaseProps {
    placeholder?: string;
    type?: string;
}

export default class FormFieldInput extends AbstractFormField<FormFieldInputProps> {

    static defaultProps = {
        type: "text",
        pure: false,
    };

    protected renderInput(field): JSX.Element {
        return <input {...field.input} type={this.props.type} className="form-control"
                      placeholder={this.props.placeholder || this.props.label}/>;
    }

}