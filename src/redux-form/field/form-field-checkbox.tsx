import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';

export interface FormFieldCheckboxProps extends FormFieldBaseProps {
    placeholder?: string;
}

export default class FormFieldCheckbox extends AbstractFormField<FormFieldCheckboxProps> {

    static defaultProps = {
        ...AbstractFormField.defaultProps,
        renderLabel: null,
    };

    protected renderInput(field): JSX.Element {
        let {onChange, ...input} = field.input;
        return (
            <div>
                <input
                    {...input}
                    type="checkbox"
                    onClick={() => onChange(!field.input.value)}
                    checked={!!field.input.value}/>
                <span className="ml">{this.props.placeholder || this.props.label}</span>
            </div>
        );
    }

}