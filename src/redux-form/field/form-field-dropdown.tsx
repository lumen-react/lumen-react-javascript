import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import {FormattedMessage} from 'react-intl';
import {DropdownList} from 'react-widgets';
import * as _ from 'lodash';

export interface FormFieldDropdownProps extends FormFieldBaseProps {
    data?: any[];
    valueField?: string;
    textField?: string | ((dataItem: any) => string);
    useObjectAsValue?: boolean;
    dropdownProps?: any;
}

export default class FormFieldDropdown extends AbstractFormField<FormFieldDropdownProps> {

    static defaultProps = {
        variant: 'default',
        placeholder: <FormattedMessage id="select" defaultMessage="Select..."/>,
        pure: false,
        dropdownProps: {},
        useObjectAsValue: false,
    };

    protected renderInput(field): JSX.Element {
        let {onCreate, ...dropdownProps} = this.props.dropdownProps;
        if (onCreate === true) {
            onCreate = name => name;
        }
        return (
            <DropdownList {...dropdownProps}
                          {...field.input}
                          data={this.props.data}
                          valueField={this.props.valueField}
                          textField={this.props.textField}
                          onChange={item => {
                              if (this.props.useObjectAsValue) {
                                  field.input.onChange(item);
                              }
                              else {
                                  field.input.onChange(item[this.props.valueField]);
                              }
                          }}
                          onCreate={item => {
                              if (onCreate) {
                                  let newValue = onCreate(item);
                                  let pushedObject = null;
                                  if (_.isObject(newValue) && this.props.valueField in newValue) {
                                      pushedObject = newValue;
                                  }
                                  else {
                                      pushedObject = {
                                          [this.props.valueField]: -1,
                                          [this.props.textField as string]: newValue
                                      };
                                  }
                                  field.input.onChange(pushedObject, {
                                      originalEvent: null,
                                      lastValue: field.value,
                                      searchTerm: item,
                                  });
                              }
                          }}/>
        );
    }

}