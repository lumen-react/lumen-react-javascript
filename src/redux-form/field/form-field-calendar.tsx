import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import {Calendar} from 'react-widgets';

export interface FormFieldCalendarProps extends FormFieldBaseProps {
    calendarProps?: any;
}

export default class FormFieldCalendar extends AbstractFormField<FormFieldCalendarProps> {

    static defaultProps = {
        ...AbstractFormField.defaultProps,
        calendarProps: {},
    };

    protected renderInput(field): JSX.Element {
        return (
            <Calendar {...this.props.calendarProps}
                      {...field.input}
            />
        );
    }

}