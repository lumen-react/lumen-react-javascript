import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import ReactMDE from 'redux-forms-markdown-editor';

export interface FormFieldMdeProps extends FormFieldBaseProps {
    textAreaStyle?: CSSStyleDeclaration,
    buttonStyle?: CSSStyleDeclaration,
    buttonContainerStyle?: CSSStyleDeclaration,
    iconSize?: number;
}

export default class FormFieldMde extends AbstractFormField<FormFieldMdeProps> {

    protected renderInput(field): JSX.Element {
        return (
            <ReactMDE {...field} {...this.props}/>
        );
    }

}