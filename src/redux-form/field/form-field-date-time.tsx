import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import {DateTimePicker} from 'react-widgets';

export interface FormFieldDateTimeProps extends FormFieldBaseProps {
    dateTimeProps?: any;
}

export default class FormFieldDateTime extends AbstractFormField<FormFieldDateTimeProps> {

    static defaultProps = {
        ...AbstractFormField.defaultProps,
        dateTimeProps: {},
    };

    protected renderInput(field): JSX.Element {
        return (
            <DateTimePicker {...this.props.dateTimeProps}
                            {...field.input}
            />
        );
    }

}