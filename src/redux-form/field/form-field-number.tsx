import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import {NumberPicker} from 'react-widgets';

export interface FormFieldNumberProps extends FormFieldBaseProps {
    numberProps?: any;
}

export default class FormFieldNumber extends AbstractFormField<FormFieldNumberProps> {

    static defaultProps = {
        ...AbstractFormField.defaultProps,
        numberProps: {},
    };

    protected renderInput(field): JSX.Element {
        return (
            <NumberPicker {...this.props.numberProps}
                          {...field.input}
            />
        );
    }

}