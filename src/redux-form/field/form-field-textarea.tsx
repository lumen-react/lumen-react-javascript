import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';

export interface FormFieldTextareaProps extends FormFieldBaseProps {
    placeholder?: string;
}

export default class FormFieldTextarea extends AbstractFormField<FormFieldTextareaProps> {

    static defaultProps = {
        pure: false,
    };

    protected renderInput(field): JSX.Element {
        return <textarea {...field.input} className="form-control"
                         placeholder={this.props.placeholder || this.props.label}/>;
    }

}