import * as React from 'react';
import {BaseFieldProps, Field} from "redux-form";
import {GenericFieldHTMLAttributes} from "redux-form/lib/Field";
import {AbstractStatelessComponent} from "../../components/abstract-component";
import * as _ from 'lodash';

export interface FormFieldBaseProps {
    renderLabel?: (field) => JSX.Element;
    pure?: boolean;
    label?: string;
}

export type FormFieldProps<T extends FormFieldBaseProps> =
    T
    & (BaseFieldProps<GenericFieldHTMLAttributes | BaseFieldProps> & GenericFieldHTMLAttributes | BaseFieldProps);


abstract class AbstractFormField<T extends FormFieldBaseProps> extends AbstractStatelessComponent<FormFieldProps<T>> {

    static defaultProps = {
        pure: false,
    };

    protected abstract renderInput(field): JSX.Element;

    protected getInternalFormFieldRenderer():string {
        return 'InternalFormField';
    }

    render() {
        let props: any = {...this.props as any, ...{renderInput: this.renderInput.bind(this)}};
        return <Field
            {...props}
            component={FORM_FIELD_RENDERERS[this.getInternalFormFieldRenderer()]}
        />
    }

}

class InternalFormField extends AbstractStatelessComponent<FormFieldProps<any>> {

    protected getClasses() {
        return `form-group ${this.props.meta.touched && this.props.meta.error ? 'has-error' : ''} ${this.props.className ? this.props.className : ''}`
    }
    
    renderErrors(touched, error) {
        if (touched) {
            if (error) {
                if (_.isArray(error)) {
                    return error.map((e, i) => {
                        if (React.isValidElement(e)) {
                            return e;
                        } else {
                            return <div key={i}>{e.toString()}</div>
                        }
                    });
                } else if (_.isObject(error) && 'id' in error && 'message' in error) {
                    return <div>{this.intl.formatMessage(error.message, error.values)}</div>
                } else {
                    return <div>{error.toString()}</div>
                }
            }
        } else {
            return null
        }
    }

    render() {
        const className = this.getClasses();
        return this.props.pure ? this.props.renderInput(this.props) : (
            <div className={className}>
                {this.props.renderLabel ? this.props.renderLabel(this.props) :
                    this.props.renderLabel !== null ? (
                        <label>{this.props.label}</label>
                    ) : null
                }
                {this.props.renderInput(this.props)}
                <div className="text-danger">
                    {this.renderErrors(this.props.meta.touched, this.props.meta.error)}
                </div>
            </div>
        );
    }
}

class InternalFormFieldNoClasses extends InternalFormField {

    protected getClasses() {
        return '';
    }

}

const FORM_FIELD_RENDERERS = {
    'InternalFormField': InternalFormField,
    'InternalFormFieldNoClasses': InternalFormFieldNoClasses,
};

export default AbstractFormField;
