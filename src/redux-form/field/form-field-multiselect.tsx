import * as React from 'react';
import {SyntheticEvent} from 'react';
import * as _ from 'lodash';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';
import {FormattedMessage} from 'react-intl';
import {Multiselect} from 'react-widgets';

export interface FormFieldMultiselectProps extends FormFieldBaseProps {
    data?: any[];
    valueField?: string;
    textField?: string | ((dataItem: any) => string);
    useObjectAsValue?: boolean;
    multiselectProps?: any;
    onChange?: (dataItems?: any[], metadata?: {
        dataItem: any,
        action: 'insert' | 'remove',
        originalEvent: SyntheticEvent<any>,
        lastValue?: any[],
        searchTerm?: string
    }) => void
}

export default class FormFieldMultiselect extends AbstractFormField<FormFieldMultiselectProps> {

    private newlyGeneratedId: number = -1;

    static defaultProps = {
        variant: 'default',
        placeholder: <FormattedMessage id="select" defaultMessage="Select..."/>,
        pure: false,
        multiselectProps: {},
        useObjectAsValue: false,
    };

    protected renderInput(field): JSX.Element {
        let {onCreate, ...multiselectProps} = this.props.multiselectProps;
        if (onCreate === true) {
            onCreate = name => name;
        }
        return (
            <Multiselect
                {...multiselectProps}
                {...field.input}
                onBlur={() => field.input.onBlur()}
                value={field.input.value || []}
                data={this.props.data}
                valueField={this.props.valueField}
                textField={this.props.textField}
                onChange={items => {
                    if (this.props.useObjectAsValue) {
                        field.input.onChange(items.map(item => item[this.props.valueField]));
                    }
                    else {
                        field.input.onChange(items);
                    }
                }}
                onCreate={item => {
                    if (onCreate && 'allowCreate' in multiselectProps && multiselectProps.allowCreate !== false) {
                        let newValue = onCreate(item);
                        let pushedObject = null;
                        if (_.isObject(newValue) && this.props.valueField in newValue) {
                            pushedObject = newValue;
                        }
                        else {
                            pushedObject = {
                                [this.props.valueField]: this.newlyGeneratedId,
                                [this.props.textField as string]: newValue
                            };
                            this.newlyGeneratedId--;
                        }
                        field.input.onChange([...(field.input.value || []), ...[pushedObject]], {
                            dataItem: pushedObject,
                            action: 'insert',
                            originalEvent: null,
                            lastValue: [...(field.input.value || [])],
                            searchTerm: item,
                        });
                    }
                }}
            />
        );
    }

}