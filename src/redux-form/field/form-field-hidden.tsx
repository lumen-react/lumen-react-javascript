import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';

export interface FormFieldHiddenProps extends FormFieldBaseProps {
}

export default class FormFieldHidden extends AbstractFormField<FormFieldHiddenProps> {

    static defaultProps = {
        renderLabel: null,
        pure: false,
    };

    protected getInternalFormFieldRenderer():string {
        return 'InternalFormFieldNoClasses';
    }

    protected renderInput(field): JSX.Element {
        return <input {...field.input} type="hidden"/>;
    }

}