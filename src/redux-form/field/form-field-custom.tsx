import * as React from 'react';
import AbstractFormField, {FormFieldBaseProps} from './abstract-form-field';

export interface FormFieldCustomProps extends FormFieldBaseProps {
    children: (field) => JSX.Element;
}

export default class FormFieldCustom extends AbstractFormField<FormFieldCustomProps> {

    protected renderInput(field): JSX.Element {
        return this.props.children(field);
    }

}