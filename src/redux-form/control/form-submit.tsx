import * as React from 'react';
import {FormattedMessage} from "react-intl";
import {InjectedFormProps} from "redux-form";

export interface FormSubmitProps {
    label?: string | JSX.Element;
}

export interface FormSubmitPropsImpl extends FormSubmitProps, InjectedFormProps<{}> {
}

export default class FormSubmit extends React.Component<FormSubmitPropsImpl> {

    static defaultProps = {
        label: <FormattedMessage id="submit" defaultMessage="Submit"/>,
    };

    render() {
        return (
            <button type="submit" className="btn btn-primary mr"
                    disabled={this.props.pristine || this.props.submitting}>{this.props.label}</button>
        );
    }

}