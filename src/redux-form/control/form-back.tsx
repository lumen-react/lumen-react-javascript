import * as React from 'react';
import {FormattedMessage} from "react-intl";
import {InjectedFormProps} from "redux-form";
import {History} from "history";

export interface FormBackProps {
    history: History
    label?: string | JSX.Element;
}

export interface FormBackPropsImpl extends FormBackProps, InjectedFormProps<{}> {
}

export default class FormBack extends React.Component<FormBackPropsImpl> {

    static defaultProps = {
        label: <FormattedMessage id="back" defaultMessage="Back"/>,
    };

    render() {
        return (
            <button type="button" className="btn btn-default mr"
                    onClick={this.props.history.goBack}>{this.props.label}</button>
        );
    }

}