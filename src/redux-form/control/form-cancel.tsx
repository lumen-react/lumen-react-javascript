import * as React from 'react';
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import {InjectedFormProps} from "redux-form";

export interface FormCancelProps {
    to?: string;
    label?: string | JSX.Element;
}

export interface FormCancelPropsImpl extends FormCancelProps, InjectedFormProps<{}> {
}

export default class FormCancel extends React.Component<FormCancelPropsImpl> {

    static defaultProps = {
        to: '/',
        label: <FormattedMessage id="cancel" defaultMessage="Cancel"/>,
    };

    render() {
        return (
            <Link to={this.props.to} className="btn btn-default mr">{this.props.label}</Link>
        );
    }

}