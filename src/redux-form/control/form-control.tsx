import * as React from 'react';
import FormCancel, {FormCancelProps} from "./form-cancel";
import FormSubmit, {FormSubmitProps} from "./form-submit";
import {default as FormSecondary, FormSecondaryProps} from "./form-secondary";
import {default as FormReset, FormResetProps} from "./form-reset";
import {InjectedFormProps} from "redux-form";
import FormBack, {FormBackProps} from "./form-back";
import {Alert} from 'react-bootstrap';

export interface FormControlProps extends InjectedFormProps<{}> {
    submitControl?: FormSubmitProps | boolean;
    secondaryControl?: FormSecondaryProps | boolean;
    resetControl?: FormResetProps | boolean;
    cancelControl?: FormCancelProps | boolean;
    backControl?: FormBackProps | boolean;
    showError?: ((error: string) => any) | boolean;
}

export default class FormControl extends React.Component<FormControlProps> {

    static defaultProps = {
        submitControl: true,
        secondaryControl: false,
        resetControl: true,
        cancelControl: false,
        backControl: false,
        showError: true,
    };

    private renderError() {
        if (this.props.error) {
            if (this.props.showError === true) {
                return <Alert variant="danger">{this.props.error}</Alert>
            }
            else if (this.props.showError !== false) {
                return this.props.showError(this.props.error);
            }
        }
    }

    render() {
        let {submitControl, secondaryControl, resetControl, cancelControl, backControl, ...injectedFormProps} = this.props;
        return (
            <div>
                {this.renderError()}
                {submitControl && <FormSubmit {...injectedFormProps} {...(submitControl !== true ? submitControl : {})} />}
                {secondaryControl && <FormSecondary {...injectedFormProps} {...(secondaryControl !== true ? secondaryControl : {})}/>}
                {resetControl && <FormReset {...injectedFormProps} {...(resetControl !== true ? resetControl : {})}/>}
                {cancelControl && <FormCancel {...injectedFormProps} {...(cancelControl !== true ? cancelControl : {})}/>}
                {backControl && <FormBack {...injectedFormProps} {...backControl as FormBackProps}/>}
            </div>
        );
    }

}