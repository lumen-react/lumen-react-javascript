import * as React from 'react';
import {FormattedMessage} from "react-intl";
import {InjectedFormProps} from "redux-form";

export interface FormResetProps {
    label?: string | JSX.Element;
}

export interface FormResetPropsImpl extends FormResetProps, InjectedFormProps<{}> {
}

export default class FormReset extends React.Component<FormResetPropsImpl> {

    static defaultProps = {
        label: <FormattedMessage id="reset" defaultMessage="Reset"/>,
    };

    render() {
        return (
            <button type="button" className="btn btn-info mr"
                    disabled={this.props.pristine || this.props.submitting}
                    onClick={this.props.reset}>{this.props.label}</button>
        );
    }

}