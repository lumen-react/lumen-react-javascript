import * as React from 'react';
import {FormattedMessage} from "react-intl";
import {InjectedFormProps} from "redux-form";

export interface FormSecondaryProps {
    onClick?: () => void;
    confirm?: string;
    label?: string | JSX.Element;
    disabled?: boolean;
}

export interface FormSecondaryPropsImpl extends FormSecondaryProps, InjectedFormProps<{}> {
}

export default class FormSecondary extends React.Component<FormSecondaryPropsImpl> {

    static defaultProps = {
        label: <FormattedMessage id="delete" defaultMessage="Delete"/>,
        disabled: false,
    };

    render() {
        return (
            <button type="button" className="btn btn-danger mr"
                    onClick={() => this.props.confirm ? (confirm(this.props.confirm) ? this.props.onClick() : null) : this.props.onClick()}
                    disabled={this.props.disabled}>{this.props.label}</button>
        );
    }

}