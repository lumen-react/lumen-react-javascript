import {defineMessages} from 'react-intl';
import {AbstractValidatorRule} from './abstract-validator-rule';

export class EmailValidatorRule extends AbstractValidatorRule {

    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor {
        return defineMessages({
            error: {
                id: 'validator.invalidEmail',
                defaultMessage: 'Should be a valid email address.',
            }
        }).error;
    }

    public isValid(value): boolean {
        return !value || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);
    }

}