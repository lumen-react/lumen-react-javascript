import {FormattedMessage} from 'react-intl';

export abstract class AbstractValidatorRule {

    private _message: FormattedMessage.MessageDescriptor;

    public constructor(message?: FormattedMessage.MessageDescriptor) {
        this._message = message || this.getDefaultMessage();
    }

    get message(): FormattedMessage.MessageDescriptor {
        return this._message;
    }

    set message(value: FormattedMessage.MessageDescriptor) {
        this._message = value;
    }

    protected abstract getDefaultMessage(): FormattedMessage.MessageDescriptor;

    public getMessageValues() {
        return {};
    }

    abstract isValid(value, name, values, props): boolean;
}