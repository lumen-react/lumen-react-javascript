import {defineMessages} from 'react-intl';
import {AbstractValidatorRule} from './abstract-validator-rule';

export class CustomRule extends AbstractValidatorRule {

    private callback;

    public constructor(callback) {
        super();
        this.callback = callback;
    }

    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor {
        return defineMessages({
            error: {
                id: 'validator.custom',
                defaultMessage: 'Something went from with field {field}.',
            }
        }).error;
    }

    public isValid(value, name, values, props): boolean {
        return this.callback(value, name, values, props);
    }

}