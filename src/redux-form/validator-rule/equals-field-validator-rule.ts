import {defineMessages, FormattedMessage} from 'react-intl';
import {AbstractValidatorRule} from './abstract-validator-rule';

export class EqualsFieldValidatorRule extends AbstractValidatorRule {

    private otherField: string;

    public constructor(otherField: string, message?: FormattedMessage.MessageDescriptor) {
        super(message);
        this.otherField = otherField;
    }

    protected getDefaultMessage(): FormattedMessage.MessageDescriptor {
        return defineMessages({
            error: {
                id: 'validator.equals',
                defaultMessage: 'Should equal {otherField} field.',
            }
        }).error;
    }

    public getMessageValues() {
        return {otherField: this.otherField};
    }

    public isValid(value, name, values, props): boolean {
        return value === values[this.otherField];
    }

}