import {defineMessages, FormattedMessage} from 'react-intl';
import * as _ from 'lodash';
import {AbstractValidatorRule} from './abstract-validator-rule';

export class RequiredValidatorRule extends AbstractValidatorRule {

    protected getDefaultMessage(): FormattedMessage.MessageDescriptor {
        return defineMessages({
            error: {
                id: 'validator.required',
                defaultMessage: 'Required.',
            }
        }).error;
    }

    public isValid(value, name, values, props): boolean {
        return _.isArray(value) ? !!value.length : !!value;
    }

}