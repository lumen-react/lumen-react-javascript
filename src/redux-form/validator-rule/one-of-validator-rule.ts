import {defineMessages} from 'react-intl';
import {AbstractValidatorRule} from './abstract-validator-rule';

export class OneOfValidatorRule extends AbstractValidatorRule {

    private pool: Array<any>;

    public constructor(pool: Array<any>) {
        super();
        this.pool = pool;
    }

    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor {
        return defineMessages({
            error: {
                id: 'validator.oneOf',
                defaultMessage: 'Should be a one of {values}.',
            }
        }).error;
    }

    public isValid(value): boolean {
        return this.pool.findIndex(p => value == p) >= 0;
    }

    public getMessageValues() {
        return {values: this.pool.join(', ')};
    }

}