import {ActionCreator, Dispatch} from "redux";

export enum GlobalActionType {
    REFRESH_STORE = "REFRESH_STORE",
}

export interface GlobalAction {
    type: GlobalActionType;
}

export type GlobalActionCallback = () => GlobalAction;

export const refreshStore: ActionCreator<GlobalActionCallback> =
    (dispatch: Dispatch<void>): GlobalActionCallback =>
        () => dispatch({type: GlobalActionType.REFRESH_STORE});

export function refreshStoreDispatcherBinder(dispatch: Dispatch<GlobalAction>) {
    return {refreshStoreDispatcher: refreshStore(dispatch)}
};

export interface RefreshStoreDispatcher {
    refreshStoreDispatcher: GlobalActionCallback;
}