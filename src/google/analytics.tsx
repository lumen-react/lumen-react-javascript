import {AbstractStatelessComponent} from '..';

export interface AnalyticsProps {
    location: {
        pathname:string,
        search:string,
    },
    reactGa: {
        set: ({page: string}) => void,
        pageview: (page: string) => void,
    }
}

export default class Analytics extends AbstractStatelessComponent<AnalyticsProps> {

    constructor(props, context) {
        super(props, context);

        // Initial page load - only fired once
        this.sendPageChange(props.location.pathname, props.location.search);
    }

    componentWillReceiveProps(nextProps) {
        // When props change, check if the URL has changed or not
        if (this.props.location.pathname !== nextProps.location.pathname
            || this.props.location.search !== nextProps.location.search) {
            this.sendPageChange(nextProps.location.pathname, nextProps.location.search);
        }
    }

    sendPageChange(pathname, search = '') {
        const page = pathname + search;
        this.props.reactGa.set({page});
        this.props.reactGa.pageview(page);
    }

    render() {
        return null;
    }

}