import * as React from 'react';
import {Action, Dispatch} from 'redux';
import {AxiosInstance, AxiosRequestConfig} from 'axios';
import {ConfigProps, reduxForm} from 'redux-form';
import {connect as reduxConnect} from 'react-redux';
import {connectLoader, ConnectLoaderOptions} from '../hoc/connect-loader';
import {ajaxLoader, AjaxLoaderOptions} from '../hoc/ajax-loader';
import {readFromHash, writeToHash} from '../utils/router-utils';
import * as _ from 'lodash';
import {EventLoaderOptions, TableState} from '..';
import {removeUndefinedKeys, swap} from '../utils/object-utils';
import {TableFilterOperator} from '../utils/bootstrap-table-utils';
import {deprecated} from '../utils/package-utils';
import {eventLoader} from '../hoc/event-loader';

/*
 * ACTION HELPERS
 */

export interface GenericCallbackOptions<T, U> {
    type?: T,
    empty?: boolean;
    emptyValue?: any;
    promise?: (promise: Promise<U>) => any,
    then?: (data: U) => any;
    catch?: (e: any) => any;
    requestConfig?: AxiosRequestConfiguration;
    meta?: any;
}

export interface BaseAction<T> extends Action {
    payload: T;
    meta: any;
}

export interface ApiResponseHeaders {
    user?: number;
    count?: number;
    unscoped?: Array<string>;
    filter?: Array<Array<string>>;
    limit?: number;
    offset?: number;
    order?: { [key: string]: 'desc' | 'asc' };
    with?: Array<string>;
    expired?: number;
}

export interface ExtendedResponseData<T> {
    data: T;
    apiResponseHeaders: ApiResponseHeaders;
    allHeaders: { [key: string]: string };
    code: number;
}

export function parseHeadersToApiResponseHeaders(headers: { [key: string]: string }): ApiResponseHeaders {
    let apiResponseHeaders = {};
    let regexp = new RegExp(/x-rslr-(.*)/i);
    for (let key in headers) {
        let matches = regexp.exec(key);
        if (matches) {
            apiResponseHeaders[matches[1]] = JSON.parse(headers[key]);
        }
    }
    return apiResponseHeaders as ApiResponseHeaders;
}

export function callDispatcher<T, U extends GenericCallbackOptions<V, T>, V>(dispatch: Dispatch<Promise<T>>,
                                                                             defaultAction: V,
                                                                             promiseCallback: Promise<T>,
                                                                             options?: U): any {
    let promise = promiseCallback;
    let type = defaultAction;
    if (options) {
        if (options.type) {
            type = options.type;
        }

        if (options.then) {
            promise.then(options.then);
        }
        if (options.catch) {
            promise.catch(options.catch);
        }
        if (options.promise) {
            options.promise(promise);
        }

        if (options.empty) {
            dispatch({
                type,
                payload: options.emptyValue || null,
            });
        }
    }
    return dispatch({
        type,
        payload: promise,
    });
}

/*
 * ENDPOINT HELPERS
 */

export type AxiosRequestConfiguration =
    {
        axios?: AxiosInstance,
        with?: string | Array<string>,
        filter?: string | Array<string | Array<string>>,
        queryString?: string | { [key: string]: any },
        limit?: number;
        offset?: number;
        order?: string | { [key: string]: 'desc' | 'asc' };
        skipDateConversion?: boolean;
        bypassGlobalResponseHandlers?: (status: number, response: any,
                                        expired: boolean) => boolean | boolean | Array<number>;
    }
    & AxiosRequestConfig;

export function getQueryString(queryString: string | { [key: string]: any }) {
    if (typeof queryString === 'string') {
        return queryString;
    }
    else {
        let qs = [];
        for (let key in queryString) {
            qs.push(`${key}=${queryString[key]}`);
        }
        return qs.join('&');
    }
}

export function getUrl(base: string, requestConfig: AxiosRequestConfiguration) {
    requestConfig = {...requestConfig};
    let qs = requestConfig && requestConfig.queryString ? requestConfig.queryString : {};

    if (requestConfig) {
        if (requestConfig.with) {
            if (typeof requestConfig.with !== 'string') {
                requestConfig.with = requestConfig.with.join(';');
            }

            if (typeof qs === 'string') {
                qs = `${qs}&with=${requestConfig.with}`;
            }
            else {
                qs.with = requestConfig.with;
            }
        }
        if (requestConfig.filter) {
            if (typeof requestConfig.filter !== 'string') {
                let filters = [];
                for (let i = 0; i < (requestConfig.filter as Array<string | Array<string>>).length; i++) {
                    if (typeof requestConfig.filter[i] !== 'string') {
                        filters.push((requestConfig.filter[i] as Array<string>).join(','));
                    }
                    else {
                        filters.push(requestConfig.filter[i]);
                    }
                }
                requestConfig.filter = filters.join(';');
            }

            if (typeof qs === 'string') {
                qs = `${qs}&filter=${requestConfig.filter}`;
            }
            else {
                qs.filter = requestConfig.filter;
            }
        }
        if (requestConfig.limit) {
            if (typeof qs === 'string') {
                qs = `${qs}&limit=${requestConfig.limit}`;
            }
            else {
                qs.limit = requestConfig.limit;
            }
        }
        if (requestConfig.offset) {
            if (typeof qs === 'string') {
                qs = `${qs}&offset=${requestConfig.offset}`;
            }
            else {
                qs.offset = requestConfig.offset;
            }
        }
        if (requestConfig.order) {
            if (typeof requestConfig.order !== 'string') {
                let order = [];
                for (let key in (requestConfig.order as { [key: string]: 'desc' | 'asc' })) {
                    order.push(`${key},${requestConfig.order[key]}`);
                }
                requestConfig.order = order.join(';');
            }

            if (typeof qs === 'string') {
                qs = `${qs}&order=${requestConfig.order}`;
            }
            else {
                qs.order = requestConfig.order;
            }
        }
    }

    return !_.isEmpty(qs) ? `${base}?${getQueryString(qs)}` : base;
}

export function writeRequestConfigToUrl(name: string, config: Partial<AxiosRequestConfiguration>) {
    writeToHash({
        [name]: _.pick(config, ['filter', 'limit', 'offset', 'order']),
    });
}

export function readRequestConfigFromUrl(name: string,
                                         defaultConfig: Partial<AxiosRequestConfiguration> = null): Partial<AxiosRequestConfiguration> {
    let object = readFromHash(null, {});
    if (name in object) {
        return object[name];
    }

    if (defaultConfig) {
        writeRequestConfigToUrl(name, defaultConfig);
        return defaultConfig;
    }
}

export const operatorConversion = {
    [TableFilterOperator.LIKE]: 'like',
    [TableFilterOperator.EQ]: 'eq',
    [TableFilterOperator.NE]: 'neq',
    [TableFilterOperator.GT]: 'gt',
    [TableFilterOperator.GE]: 'gte',
    [TableFilterOperator.LT]: 'lt',
    [TableFilterOperator.LE]: 'lte',
};

export function apiResponseHeadersToTableState(apiResponseHeaders: ApiResponseHeaders): TableState {
    if (!apiResponseHeaders) {
        apiResponseHeaders = {};
    }

    let sortField;
    let sortOrder;
    let filters = {};

    if ('order' in apiResponseHeaders) {
        for (let k in apiResponseHeaders.order as any) {
            sortField = k;
            sortOrder = apiResponseHeaders.order[k];
            break;
        }
    }

    if ('filter' in apiResponseHeaders) {
        for (let f in apiResponseHeaders.filter as any) {
            let ff = apiResponseHeaders.filter[f];
            filters[ff[0]] = {filterVal: ff[2], comparator: swap(operatorConversion)[ff[1]]};
        }
    }

    if (_.isEmpty(filters)) {
        filters = undefined;
    }

    return removeUndefinedKeys({
        totalSize: apiResponseHeaders.count,
        page: apiResponseHeaders.limit ?
            Math.floor((apiResponseHeaders.offset ? apiResponseHeaders.offset : 0) / apiResponseHeaders.limit) + 1 :
            undefined,
        sizePerPage: apiResponseHeaders.limit,
        sortField,
        sortOrder,
        filters,
    });
}

export function requestConfigMerge(base: Partial<AxiosRequestConfiguration>,
                                   ...merges: Array<Partial<AxiosRequestConfiguration>>) {
    let merge = merges.pop();
    let copy: Partial<AxiosRequestConfiguration> = {...base};
    if (merge) {
        if (copy.with && _.isArray(copy.with) && _.isArray(merge.with)) {
            copy.with = [...copy.with, ...merge.with];
        }
        else if (merge.with) {
            copy.with = merge.with;
        }
        if (merge.filter && _.isArray(merge.filter)) {
            let filter = [];
            merge.filter.forEach(f => {
                if (_.isArray(f)) {
                    filter.push([f[0], operatorConversion[f[1]], f[2]]);
                }
                else {
                    filter.push(f);
                }
            });
            if (copy.filter && _.isArray(copy.filter)) {
                copy.filter = [...copy.filter, ...filter];
            }
            else {
                copy.filter = filter;
            }
        }
        if (copy.queryString && _.isArray(copy.queryString) && _.isArray(merge.queryString)) {
            copy.queryString = [...copy.queryString, ...merge.queryString];
        }
        else if (merge.queryString) {
            copy.queryString = merge.queryString;
        }
        if (merge.limit) {
            copy.limit = merge.limit;
        }
        if (merge.offset) {
            copy.offset = merge.offset;
        }
        if (merge.order) {
            copy.order = merge.order;
        }
    }
    if (merges.length > 0) {
        return requestConfigMerge(copy, ...merges);
    } else {
        return copy;
    }
}

export function apiResponseHeadersToRequestConfig(apiResponseHeaders: ApiResponseHeaders,
                                                  queryString = null): Partial<AxiosRequestConfiguration> {
    return {..._.pick(apiResponseHeaders, 'with', 'filter', 'limit', 'offset', 'order'), ...{queryString}};
}

export function requestConfigToTableState(requestConfig: AxiosRequestConfiguration): TableState {
    if (!requestConfig) {
        requestConfig = {};
    }

    let sortField;
    let sortOrder;
    let filters = {};

    if ('order' in requestConfig) {
        for (let k in requestConfig.order as any) {
            sortField = k;
            sortOrder = requestConfig.order[k];
            break;
        }
    }

    if ('filter' in requestConfig) {
        for (let f in requestConfig.filter as any) {
            let ff = requestConfig.filter[f];
            filters[ff[0]] = {filterVal: ff[2], comparator: swap(operatorConversion)[ff[1]]};
        }
    }

    if (_.isEmpty(filters)) {
        filters = undefined;
    }

    return removeUndefinedKeys({
        page: 'limit' in requestConfig ?
            Math.floor(('offset' in requestConfig ? requestConfig.offset : 0) / requestConfig.limit) + 1 :
            undefined,
        sizePerPage: 'limit' in requestConfig ? requestConfig.limit : undefined,
        sortField,
        sortOrder,
        filters,
    });
}

export function tableStateToRequestConfig(tableState: TableState, queryString = undefined,
                                          cancelToken: string = undefined): Partial<AxiosRequestConfiguration> {
    if (!tableState) {
        tableState = {};
    }

    let f = [];

    if ('filters' in tableState) {
        for (let fk in tableState.filters) {
            let push = null;
            if (_.isObject(tableState.filters[fk].filterVal)) {
                push = [
                    fk,
                    operatorConversion[tableState.filters[fk].filterVal.comparator],
                    'date' in tableState.filters[fk].filterVal ? tableState.filters[fk].filterVal.date :
                        tableState.filters[fk].filterVal.number,
                ];
            }
            else {
                push = [
                    fk,
                    operatorConversion[tableState.filters[fk].comparator],
                    tableState.filters[fk].filterVal,
                ];
            }
            if (push[2] != '') {
                f.push(push);
            }
        }
    }

    if (_.isEmpty(f)) {
        f = undefined;
    }

    return removeUndefinedKeys({
        offset: tableState.page ? tableState.sizePerPage * (tableState.page - 1) : undefined,
        limit: tableState.sizePerPage,
        filter: f,
        order: (tableState.sortField && tableState.sortOrder) ? {[tableState.sortField]: tableState.sortOrder} :
            undefined,
        cancelToken,
        queryString,
    });
}

export interface ConnectorOptions {
    formConfigProps?: ConfigProps<any, any>,
    mapStateToProps?: any,
    mapDispatchToProps?: any,
    mapOptions?: any,
    connectLoaderOptions?: ConnectLoaderOptions,
    ajaxLoaderOptions?: AjaxLoaderOptions,
    eventLoaderOptions?: EventLoaderOptions,
}

export function connector(component, connectorOptions: ConnectorOptions = {}) {
    let connector = component;
    let {formConfigProps, mapStateToProps, mapDispatchToProps, connectLoaderOptions, ajaxLoaderOptions, eventLoaderOptions, mapOptions} = connectorOptions;

    if (formConfigProps) {
        connector = reduxForm(formConfigProps)(connector);
    }
    if (connectLoaderOptions) {
        connector = connectLoader(connectLoaderOptions)(connector);
    }
    if (mapStateToProps || mapDispatchToProps) {
        connector = reduxConnect(mapStateToProps, mapDispatchToProps, undefined, mapOptions)(connector);
    }
    if (ajaxLoaderOptions) {
        connector = ajaxLoader(ajaxLoaderOptions)(connector);
    }
    if (eventLoaderOptions) {
        connector = eventLoader(eventLoaderOptions)(connector);
    }

    return connector;
}

/*
 * REDUX HELPERS (LEGACY METHODS)
 */
export function connect(component,
                        mapStateToProps = null,
                        mapDispatchToProps = null,
                        dispatcherLoaders = null,
                        ajaxLoaderPromises = null): any {
    deprecated('use connector instead');
    return (
        ajaxLoader(ajaxLoaderPromises)(
            reduxConnect(mapStateToProps, mapDispatchToProps)(
                connectLoader({dispatchers: dispatcherLoaders})(
                    component
                )
            )
        )
    );
}

export function form(component,
                     formConfigProps,
                     mapStateToProps = null,
                     mapDispatchToProps = null,
                     dispatcherLoaders = null,
                     ajaxLoaderPromises = null): any {
    deprecated('use connector instead');
    return (
        ajaxLoader(ajaxLoaderPromises)(
            reduxConnect(mapStateToProps, mapDispatchToProps)(
                connectLoader({dispatchers: dispatcherLoaders})(
                    reduxForm(formConfigProps)(
                        component
                    ) as any
                )
            )
        )
    );
}

export function simpleForm(component,
                           formConfigProps,
                           ajaxLoaderPromises = null): any {
    deprecated('use connector instead');
    return (
        ajaxLoader(ajaxLoaderPromises)(
            reduxForm(formConfigProps)(
                component
            )
        )
    );
}

export function simpleLoader(component,
                             ajaxLoaderPromises = null): any {
    deprecated('use connector instead');
    return ajaxLoader(ajaxLoaderPromises)(component);
}