export function writeToHash(object: any, clear: boolean = false) {
    if (clear) {
        window.location.hash = JSON.stringify(object);
    }
    else {
        let oldObject = readFromHash(null, {});
        let newObject = {...oldObject, ...object};
        window.location.hash = JSON.stringify(newObject);
    }
}

export function readFromHash(key: string = null, defaultValue = null): any {
    try {
        let parsed = JSON.parse(decodeURI(window.location.hash.substr(1)));
        if (key !== null) {
            return key in parsed ? parsed[key] : defaultValue;
        }
        else {
            return parsed;
        }
    }
    catch {
        return defaultValue;
    }
}

