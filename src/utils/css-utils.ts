export default class CssUtils {

    private static cached = {};

    public static get(selector) {
        if (CssUtils.cached[selector]) {
            return CssUtils.cached[selector];
        }

        for (let i = 0; i < document.styleSheets.length; i++) {
            try {
                let styleSheet = document.styleSheets[i] as any;
                for (let j = 0; j < styleSheet.cssRules.length; j++) {
                    let cssRules = styleSheet.cssRules[j];
                    if (cssRules.selectorText == selector) {
                        let theRules = {};
                        for (let k = 0; k < cssRules.style.length; k++) {
                            theRules[cssRules.style[k]] = cssRules.style[cssRules.style[k]];
                        }
                        CssUtils.cached[selector] = theRules;
                        return theRules;
                    }
                }
            } catch {
            }
        }
        return null;
    }

    public static set(selector, rule) {
        if (CssUtils.cached[selector]) {
            delete CssUtils.cached[selector];
        }

        for (let i = 0; i < document.styleSheets.length; i++) {
            try {
                let styleSheet = document.styleSheets[i] as any;
                for (let j = 0; j < styleSheet.cssRules.length; j++) {
                    let cssRules = styleSheet.cssRules[j];
                    if (cssRules.selectorText == selector) {
                        styleSheet.insertRule(selector + ' { ' + rule + ' }', 0)
                    }
                }
            } catch {
            }
        }

        console.warn("Warning: CSS " + selector + " not found");
    }

    public static rgb2hex(rgb) {
        let r = rgb.match(/^\s*rgb\((\d+)\s*(\d+)\s*(\d+)\s*\)\s*,$/i);
        if (r) {
            rgb = parseInt(r[1]) | (parseInt(r[2]) << 8) | (parseInt(r[3]) << 16);
            return '#' + (0x1000000 + rgb).toString(16).slice(1)
        } else {
            return rgb
        }
    }

    public static hex2rgb(hex) {
        // long version
        let r = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
        if (r) {
            return r.slice(1, 4).map(function (x) {
                return parseInt(x, 16);
            });
        }
        // short version
        r = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
        if (r) {
            return r.slice(1, 4).map(function (x) {
                return 0x11 * parseInt(x, 16);
            });
        }
        return hex;
    }

}

// document.styleSheets[6].insertRule('.bg-primary { color: rgb(255, 255, 255); background-color: red; }', 0)
//
// for (var i =0; i < document.styleSheets.length; i++) {
//     try {
//         document.styleSheets[i].cssRules[0];
//         for (var j = 0; j < document.styleSheets[i].cssRules.length; j++) {
//             if (document.styleSheets[i].cssRules[j].selectorText == '.bg-primary') {
//                 console.log(document.styleSheets[i].cssRules[j]);
//             }
//         }
//     } catch {
//     }
// }