export function deprecated(message: string = '') {
    console.trace('DEPRECATED', message);
}