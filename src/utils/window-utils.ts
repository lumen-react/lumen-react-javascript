import {dispatch} from './event-utils';

export const ON_KEY_DOWN = 'onKeyDown';
export const ON_KEY_UP = 'onKeyUp';

export interface Window {
    storedCancelTokens?: Array<any>;
    defaultTableDateFormatter?: (date: Date) => string;
    keyRegister?: { [key: string]: boolean };
    globalLoader?: ((content?: any) => JSX.Element) | JSX.Element | string;
    storage?: {};
}

function getWindow<T extends Window>(): T {
    return (window as any).rs;
}

function setWindow<T extends Window>(properties: Partial<T>) {
    if (!(window as any).rs) {
        (window as any).rs = {};
    }
    for (let key in properties) {
        (window as any).rs[key] = properties[key];
    }
}

export function setGlobal<T extends Window = Window>(key: keyof T, value: any) {
    setWindow<T>({[key]: value} as Partial<T>);
}

export function getGlobal<T extends Window = Window>(key: keyof T = null, defaultValue: any = null) {
    let windowObject = getWindow<T>();
    if (windowObject) {
        if (key) {
            return key in windowObject ? windowObject[key] : defaultValue;
        }
        else {
            return windowObject;
        }
    }
    else {
        return defaultValue;
    }
}

export const SHIFT_KEY = -1;
export const ALT_KEY = -2;
export const CTRL_KEY = -3;
export const META_KEY = -4;
export const OPT_KEY = -5;

export function isKeyDown(key) {
    return getKeyRegister(key);
}

export function setupKeyRegister() {
    window.onkeydown = (e) => {
        registerKey(e, true);
        dispatch(ON_KEY_DOWN, {key: e.key, shift: e.shiftKey, alt: e.altKey, ctrl: e.ctrlKey, meta: e.metaKey});
    };
    window.onkeyup = (e) => {
        registerKey(e, false);
        dispatch(ON_KEY_UP, {key: e.key, shift: e.shiftKey, alt: e.altKey, ctrl: e.ctrlKey, meta: e.metaKey});
    };
}

export function getKeyRegister(key) {
    let register = getGlobal('keyRegister', {});
    return key in register ? register[key] : false;
}

export function setKeyRegister(key, value: boolean) {
    let register = getGlobal('keyRegister', {});
    register[key] = value;
    setGlobal('keyRegister', register);
}

export function registerKey(e, down: boolean) {
    if (!e) {
        e = window.event;
    }

    setKeyRegister(SHIFT_KEY, e.shiftKey);
    setKeyRegister(ALT_KEY, e.altKey);
    setKeyRegister(CTRL_KEY, e.ctrlKey);
    setKeyRegister(OPT_KEY, e.ctrlKey);
    setKeyRegister(META_KEY, e.metaKey);
    setKeyRegister(OPT_KEY, e.metaKey);

    if (e.key) {
        setKeyRegister(e.key, down);
    }
}