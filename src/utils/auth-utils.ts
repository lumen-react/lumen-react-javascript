import StorageUtils, {StorageDriverId} from "./storage-utils";

const COOKIE_TOKEN = 'AUTH_COOKIE';

export default class AuthUtils {

    public static storageDriverId: StorageDriverId;
    private static token = null;
    private static user = null;

    public static boot(storageDriverId) {
        AuthUtils.storageDriverId = storageDriverId;
        let data = StorageUtils.get(COOKIE_TOKEN, AuthUtils.storageDriverId);
        if (data) {
            AuthUtils.token = data.token;
            AuthUtils.user = data.user;
        }
    }

    public static login(token, user): void {
        AuthUtils.token = token;
        AuthUtils.user = user;
        StorageUtils.set(COOKIE_TOKEN, {token, user}, AuthUtils.storageDriverId);
    }

    public static logout(): void {
        AuthUtils.token = null;
        AuthUtils.user = null;
        StorageUtils.remove(COOKIE_TOKEN, AuthUtils.storageDriverId);
    }

    public static isLoggedIn(): boolean {
        return AuthUtils.token != null && this.user != null;
    }

    public static authenticatedUser<T>(): T {
        return AuthUtils.user as T;
    }

    public static getToken(): string {
        return AuthUtils.token;
    }

}