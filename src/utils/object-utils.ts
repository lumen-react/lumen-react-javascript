export function removeUndefinedKeys(object: { [key: string]: any }): { [key: string]: any } {
    let o = {};
    for (let k in object) {
        if (object[k] != undefined) {
            o[k] = object[k];
        }
    }
    return o;
}

export function swap(object: { [key: string]: any }): { [key: string]: any } {
    let ret = {};
    for (let key in object) {
        ret[object[key]] = key;
    }
    return ret;
}