import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import {Provider} from 'react-redux';
import * as ReduxPromise from 'redux-promise';
import {applyMiddleware, createStore} from 'redux';
import {addLocaleData, IntlProvider} from 'react-intl';
import axios from 'axios';
import * as _ from 'lodash';
import AuthUtils from './auth-utils';
import Empty from '../components/empty';
import {getGlobal, setGlobal} from './window-utils';
import StorageUtils, {StorageDriverId} from './storage-utils';

export interface BootOptions {
    App: any;
    languages: { supported: { [key: string]: Array<string> }, default: string };
    localeCallback?: Promise<string> | string;
    appName?: string;
    className?: string;
    module?: any;
    reducers?: any;
    postBoot?: (locale?: string) => void;
    ErrorBoundary?: any;
    globalLoader?: ((content?: any) => JSX.Element) | JSX.Element | string;
    preRenderCallback?: () => void;
    postRenderCallback?: () => void;
    hotLoaderPath?: string;
    defaultStorageDriverId?: StorageDriverId;
    authStorageDriverId?: StorageDriverId;
}

export function run(Entry, options: Partial<BootOptions> = {}) {
    options = {
        ...{
            appName: 'web',
            className: null,
            languages: {supported: {en: ['en']}, default: 'en'},
            reducers: [],
            module: null,
            App: null,
            postBoot: null,
            ErrorBoundary: Empty,
            globalLoader: '...',
            preRenderCallback: null,
            postRenderCallback: null,
            hotLoaderPath: './components/app',
            defaultStorageDriverId: StorageDriverId.LOCAL_STORAGE,
        }, ...options
    };

    // Boot options
    let {appName, className, module, reducers, languages, localeCallback, App, postBoot, ErrorBoundary, globalLoader, preRenderCallback, postRenderCallback, hotLoaderPath, defaultStorageDriverId, authStorageDriverId} = options;

    let runWithLocale = (locale) => {
        if (postBoot) {
            postBoot(locale);
        }

        let localeExists = false;
        for (let language in languages.supported) {
            if (_.includes(languages.supported[language], locale)) {
                localeExists = true;
                locale = language;
                break;
            }
        }

        locale = localeExists ? locale : languages.default;

        // Store
        const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
        const axiosStaticInstance = axios.create({baseURL: '/'});

        const render = () => {
            axiosStaticInstance.get(`/locale/${locale}.json`).then((data) => {
                let dom = document.getElementById('root');
                dom.className = (dom.className + ` app-${appName}` + (className ? ` ${className}` : '')).trim();
                preRenderCallback && preRenderCallback();
                ReactDOM.render(
                    <AppContainer>
                        <ErrorBoundary>
                            <IntlProvider locale={locale} messages={data.data}>
                                <Provider store={createStoreWithMiddleware(reducers)}>
                                    <App><Entry/></App>
                                </Provider>
                            </IntlProvider>
                        </ErrorBoundary>
                    </AppContainer>,
                    dom
                );
                postRenderCallback && postRenderCallback();
            });
        };

        import (`react-intl/locale-data/${locale}`).then(data => {
            addLocaleData(data);
            render();
        });

        // Webpack Hot Module Replacement API
        if (module && module.hot) {
            module.hot.accept(hotLoaderPath, () => render());
        }
    };

    // Initialize Storage
    StorageUtils.defaultStorageDriverId = defaultStorageDriverId;

    // Global loader
    setGlobal('globalLoader', globalLoader);

    // Auth
    AuthUtils.boot(authStorageDriverId || defaultStorageDriverId);

    // Locale
    let locale = languages.default;

    if (localeCallback) {
        if (typeof localeCallback === 'string') {
            runWithLocale(localeCallback);
        }
        else {
            (localeCallback as Promise<string>).then(l => runWithLocale(l));
        }
    }
    else {
        runWithLocale(locale);
    }

}

export function resolveLoader(content?: any) {
    const loader = getGlobal('globalLoader');
    if (loader) {
        if (_.isFunction(loader)) {
            return loader(content);
        } else {
            return loader;
        }
    } else {
        return null;
    }
}