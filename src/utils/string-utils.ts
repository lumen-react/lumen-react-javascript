export function makeId(length = 32) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

export function lcfirst(string: string) {
    return string.charAt(0).toLowerCase() + string.substr(1);
}

export function ucfirst(string: string) {
    return string.charAt(0).toUpperCase() + string.substr(1);
}

export function snakeCaseToNormal(string: string) {
    return string.replace(/_/g, ' ').replace(/\w\S*/g, function (word) {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
}