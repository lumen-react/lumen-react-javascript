export function addFullListener<T = any>(event: string, listener: (e: CustomEvent<T>) => void,
                                         options?: boolean | AddEventListenerOptions) {
    window.addEventListener(event, listener, options);
}

export function addListener<T = any>(event: string, listener: (e: T) => void,
                                     options?: boolean | AddEventListenerOptions) {
    window.addEventListener(event, (e: CustomEvent) => listener(e.detail), options);
}


export function dispatch<T = { [key: string]: any }>(event: string, detail?: T) {
    window.dispatchEvent(new CustomEvent(event, {detail}));
}

export function removeListener(event: string, listener, options?: boolean | AddEventListenerOptions) {
    window.removeEventListener(event, listener, options);
}