export function classNames(...classMap: Array<string | { [key: string]: boolean | string }>): string {
    let classes = [];
    for (let i = 0; i < classMap.length; i++) {
        const className = classMap[i];
        if (typeof className === 'string') {
            classes.push(className);
        } else {
            for (let key in className) {
                if (className[key] === true) {
                    if (className[key]) {
                        classes.push(key);
                    }
                } else if (className[key] !== false) {
                    let splitClasses = (className[key] as string).split(/ +/);
                    for (let j = 0; j < splitClasses.length; j++) {
                        classes.push(`${splitClasses[j]}-${key}`);
                    }
                }
            }
        }
    }
    return classes.join(" ");
}