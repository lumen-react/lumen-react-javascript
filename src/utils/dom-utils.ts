import {isKeyDown, OPT_KEY} from './window-utils';
import {History} from 'history';

export function goto(href: string, target: string | History = '_top', e?) {
    if (e) {
        e.preventDefault();
        e.stopPropagation();
    }
    if (typeof target === 'string') {
        let node = document.createElement('a');
        node.href = href;
        node.target = target;
        let a = document.body.appendChild(node);
        a.click();
        document.body.removeChild(a);
    }
    else {
        target.push(href);
    }
    return false;
}

export function gotoAlt(href: string, blankTarget: string | History = '_top', e?) {
    return goto(href, isKeyDown(OPT_KEY) ? '_blank' : blankTarget, e);
}