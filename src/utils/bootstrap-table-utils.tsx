import * as React from 'react';
import filterFactory, {dateFilter, numberFilter, selectFilter, textFilter} from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {FormattedMessage, InjectedIntl, Messages} from 'react-intl';
import * as _ from 'lodash';
import {readFromHash, writeToHash} from './router-utils';
import {removeUndefinedKeys} from '..';
import {getGlobal} from './window-utils';
import MessageDescriptor = FormattedMessage.MessageDescriptor;

const EMPTY_FILTER_VALUE = 'EMPTY_FILTER_VALUE';

export const paginationOptions = {
    paginationSize: 5,
    sizePerPage: 25,
    pageStartIndex: 1,
    alwaysShowAllBtns: true, // Always show next and previous button
    withFirstAndLast: true, // Hide the going to First and Last page button
    hideSizePerPage: false, // Hide the sizePerPage dropdown always
    hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    firstPageText: 'First',
    prePageText: 'Back',
    nextPageText: 'Next',
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
};

export function messagesToColumns(messages: Messages, intl: InjectedIntl, keyMap?) {
    let columns = [];
    for (let i in messages) {
        let message: MessageDescriptor = messages[i];
        let column: any = {
            dataField: (keyMap && typeof keyMap[message.id] === 'string') ? keyMap[message.id] : message.id,
            text: intl.formatMessage(message),
        };
        if (keyMap && typeof keyMap[message.id] !== 'string') {
            column.formatter = keyMap[message.id];
        }
        columns.push(column);
    }

    return columns;
}

export enum TableDefinitionType {
    TEXT = 'TEXT',
    SELECT = 'SELECT',
    NUMBER = 'NUMBER',
    DATE = 'DATE',
}

export enum TableFilterOperator {
    LIKE = 'LIKE',
    EQ = '=',
    NE = '!=',
    GT = '>',
    GE = '>=',
    LT = '<',
    LE = '<=',
}

export interface TableRowDefinition<T> {
    id: string;
    text?: string;
    dataField?: string;
    formatter?: (cell: any, row: T, rowIndex: number, extraData: any) => string | JSX.Element | Array<JSX.Element>;
    type?: TableDefinitionType;
    filterValue?: (cell: any, row: T) => any;
    filterOptions?: { [key: string]: any };
    sort?: boolean;
    onSort?: (sortField: string, sortOrder: 'asc' | 'desc') => void;
}

export interface TableState {
    totalSize?: number;
    page?: number;
    sizePerPage?: number;
    sortField?: string;
    sortOrder?: 'asc' | 'desc';
    filters?: { [key: string]: { filterVal: any, comparator: TableFilterOperator } };
}

export interface TableStateChanged<T> extends TableState {
    data?: Array<T>;
    cellEdit?: {
        rowId?: any;
        dataField?: any;
        newValue?: any;
    };
}

export interface TableDefinition<T> {
    id: string;
    rowDefinitions: Array<TableRowDefinition<T>>;
    state?: TableState;
    remote?: boolean;
    keyField?: string;
    onTableChange?: (type: 'filter' | 'pagination' | 'sort' | 'cellEdit', newState: TableStateChanged<T>) => void;
    paginationOptions?: {
        page?: number;
        sizePerPage?: number;
        paginationSize?: number;
        pageStartIndex?: number;
        alwaysShowAllBtns?: boolean; // Always show next and previous button
        withFirstAndLast?: boolean; // Hide the going to First and Last page button
        hideSizePerPage?: boolean; // Hide the sizePerPage dropdown always
        hidePageListOnlyOnePage?: boolean; // Hide the pagination list when only one page
        firstPageText?: string;
        prePageText?: string;
        nextPageText?: string;
        lastPageText?: string;
        nextPageTitle?: string;
        prePageTitle?: string;
        firstPageTitle?: string;
        lastPageTitle?: string;
        showTotal?: boolean;
        paginationTotalRenderer?: (from: number, to: number, size: number) => JSX.Element;
        sizePerPageList?: Array<{ text: string; value: number }>;
        objectPluralMessageKey?: string;
        totalSize?: number;
    };
    dateFormatter?: (date: Date) => string | JSX.Element;
}

export function tableColumnSelectArrayToObject(array: Array<any>, value: string = 'name', key: string = 'id') {
    let object = {};
    for (let i = 0; i < array.length; i++) {
        object[array[i][key]] = array[i][value];
    }
    return object;
}

export function createTableProps<T>(data: Array<T>, tableDefinition: TableDefinition<T>, messages: Messages,
                                    intl: InjectedIntl, useWrapper: boolean = true): { [key: string]: any } {
    let defaultOptions: Partial<TableDefinition<T>> = {
        rowDefinitions: [],
        state: {},
        remote: false,
        keyField: 'id',
        paginationOptions: {
            paginationSize: 5,
            pageStartIndex: 1,
            alwaysShowAllBtns: true,
            withFirstAndLast: true,
            hideSizePerPage: false,
            hidePageListOnlyOnePage: true,
            firstPageText: 'first' in messages ? intl.formatMessage(messages.first) : 'First',
            prePageText: 'previous' in messages ? intl.formatMessage(messages.previous) : 'Previous',
            nextPageText: 'next' in messages ? intl.formatMessage(messages.next) : 'Next',
            lastPageText: 'last' in messages ? intl.formatMessage(messages.last) : 'Last',
            nextPageTitle: 'next' in messages ? intl.formatMessage(messages.next) : 'Next',
            prePageTitle: 'previous' in messages ? intl.formatMessage(messages.previous) : 'Previous',
            firstPageTitle: 'first' in messages ? intl.formatMessage(messages.first) : 'First',
            lastPageTitle: 'last' in messages ? intl.formatMessage(messages.last) : 'Last',
            showTotal: true,
            paginationTotalRenderer: (from: number, to: number, size: number) => (
                <div className="react-bootstrap-table-pagination-footer">
                    {(tableDefinition.paginationOptions && tableDefinition.paginationOptions.objectPluralMessageKey) ?
                        ('tableShowingFooterWithObject' in messages ?
                            intl.formatMessage(messages.tableShowingFooterWithObject, {
                                from, to: to + 1, size,
                                objects: tableDefinition.paginationOptions.objectPluralMessageKey
                            }) : `Showing ${from} to ${(to +
                                1)} of ${size} ${tableDefinition.paginationOptions.objectPluralMessageKey}`)
                        :
                        ('tableShowingFooter' in messages ? intl.formatMessage(messages.tableShowingFooter, {
                            from, to: to + 1, size,
                        }) : `Showing ${from} to ${(to + 1)} of ${size} results`)
                    }
                </div>
            ),
            sizePerPageList: getDefaultSizePerPageList(),
        },
        dateFormatter: getGlobal('defaultTableDateFormatter', (date: Date) => date.toISOString()),
    };

    let options = {...defaultOptions, ...tableDefinition};

    let columns = [];

    for (let i = 0; i < options.rowDefinitions.length; i++) {
        columns.push(getRowOptions(options, options.rowDefinitions[i], messages, intl, !options.remote, useWrapper));
    }

    let paginationOptions: any = {...options.paginationOptions};

    let defaultSorted = [];

    if (options.state.totalSize) {
        paginationOptions.totalSize = options.state.totalSize;
    }

    if (options.state.page) {
        paginationOptions.page = options.state.page;
    }

    if (options.state.sizePerPage) {
        paginationOptions.sizePerPage = options.state.sizePerPage;
    }

    if (options.state.sortField) {
        let dataField = columns.find(c => c.id == options.state.sortField).dataField;
        let order = options.state.sortOrder ? options.state.sortOrder : 'desc';
        defaultSorted = [{dataField, order}];
    }

    if (options.remote) {
        return {
            state: options.state,
            data,
            filter: filterFactory(),
            pagination: paginationFactory(paginationOptions),
            keyField: options.keyField,
            columns,
            defaultSorted,
            remote: {
                filter: true,
                pagination: true,
                sort: true,
                cellEdit: true,
            },
            onTableChange: options.onTableChange,
        };
    }
    else {
        let onPaginationChange = (onTableChange) => {
            return (page, sizePerPage) => {
                if (onTableChange) {
                    onTableChange('pagination', {
                        page,
                        sizePerPage,
                    });
                }
            };
        };

        let onFilterChange = (onTableChange) => {
            return (ref) => {
                for (let i = 0; i < columns.length; i++) {
                    let dom: HTMLElement = (document.getElementsByClassName(`${options.id}-${columns[i].id}`)
                        .item(0) as any);
                    dom.onchange = dom.onkeyup = dom.onblur =
                        (e: any) => {
                            if (onTableChange) {
                                let filterVal = e.target.value;
                                let comparator = null;
                                let potentialNumberComparator: HTMLSelectElement = dom.getElementsByClassName(
                                    'number-filter-comparator').item(0) as any;
                                if (potentialNumberComparator) {
                                    filterVal =
                                        (dom.getElementsByClassName('number-filter-input').item(0) as any).value;
                                    comparator = potentialNumberComparator.value;
                                }
                                else {
                                    let potentialDateComparator: HTMLSelectElement = dom.getElementsByClassName(
                                        'date-filter-comparator').item(0) as any;
                                    if (potentialDateComparator) {
                                        filterVal =
                                            (dom.getElementsByClassName('date-filter-input').item(0) as any).value;
                                        comparator = potentialDateComparator.value;
                                    }
                                    else {
                                        if (dom.classList.contains('text-filter')) {
                                            comparator = TableFilterOperator.LIKE;
                                        }
                                        else {
                                            comparator = TableFilterOperator.EQ;
                                        }
                                    }
                                }
                                onTableChange('filter', {
                                    filters: {
                                        [columns[i].id]: (filterVal != '' && comparator != '') ? {
                                            filterVal,
                                            comparator,
                                        } : null
                                    }
                                });
                            }
                        };
                }
            };
        };

        if (useWrapper) {
            return {
                state: options.state,
                data,
                filter: filterFactory(),
                paginationOptions,
                keyField: options.keyField,
                columns,
                defaultSorted,
                onPaginationChange,
                onFilterChange,
                onTableChange: options.onTableChange,
            };
        }
        else {
            paginationOptions.onPageChange = onPaginationChange(options.onTableChange);
            paginationOptions.onSizePerPageChange = onPaginationChange(options.onTableChange);

            return {
                state: options.state,
                data,
                filter: filterFactory(),
                pagination: paginationFactory(paginationOptions),
                keyField: options.keyField,
                columns,
                defaultSorted,
                ref: onFilterChange(options.onTableChange),
            };
        }
    }
}


export function getRowOptions<T>(tableDefinition: TableDefinition<T>,
                                 rowDefinition: TableRowDefinition<T>, messages: Messages,
                                 intl: InjectedIntl, isLocal: boolean, useWrapper: boolean): TableRowDefinition<T> {
    let onSortChange = (onTableChange) => {
        return (sortField, sortOrder) => {
            if (rowDefinition.onSort) {
                rowDefinition.onSort(sortField, sortOrder);
            }

            if (onTableChange) {
                onTableChange('sort', {
                    sortField,
                    sortOrder,
                });
            }
        };
    };

    let ret: any = {
        ...{
            text: rowDefinition.id in messages ? intl.formatMessage(messages[rowDefinition.id]) : rowDefinition.id,
            dataField: rowDefinition.id,
            type: TableDefinitionType.TEXT,
            sort: isLocal,
            onSort: isLocal ? (useWrapper ? onSortChange : onSortChange(tableDefinition.onTableChange)) :
                rowDefinition.onSort,
        }, ..._.omit(rowDefinition, 'filterValue', 'filterOptions', 'onSort', 'formatter')
    } as TableRowDefinition<T>;

    ret.filter = ret.filter ? ret.filter :
        defaultFilterTypeObjectCreator(tableDefinition, ret, rowDefinition.filterOptions, messages, intl, isLocal);

    ret.formatter = (cell: any, row: T, rowIndex: number, extraData: any) => {
        let r = rowDefinition.formatter ? rowDefinition.formatter(cell, row, rowIndex, extraData) : cell;
        if (rowDefinition.filterOptions && rowDefinition.filterOptions.translate &&
            (!('skipFormatterTranslate' in rowDefinition.filterOptions) ||
                !rowDefinition.filterOptions.skipFormatterTranslate)) {
            r = r in messages ? intl.formatMessage(messages[r]) : r;
        }
        if (!rowDefinition.formatter && ret.type == TableDefinitionType.DATE) {
            r = tableDefinition.dateFormatter(_.isDate(r) ? r : new Date(r));
        }
        return r;
    };

    ret.filterValue = (cell: any, row: T) => {
        let r = rowDefinition.filterValue ? rowDefinition.filterValue(cell, row) : cell;
        if (ret.type == TableDefinitionType.DATE) {
            let d = _.isDate(r) ? r : new Date(r);
            r = (d.getTime() / 1000).toFixed(0).toString();
        }
        if (isLocal && _.isBoolean(r)) {
            r = r ? 1 : 0;
        }
        return r;
    };

    return ret;
}

export function defaultFilterTypeObjectCreator<T>(tableDefinition: TableDefinition<T>,
                                                  rowDefinition: TableRowDefinition<T>,
                                                  filterOptions: { [key: string]: any },
                                                  messages: Messages,
                                                  intl: InjectedIntl,
                                                  isLocal: boolean) {
    if (!rowDefinition.type && !isLocal) {
        return undefined;
    }

    let placeholder = rowDefinition.id in messages ? intl.formatMessage(messages[rowDefinition.id]) : rowDefinition.id;
    let filter = tableDefinition.state && tableDefinition.state.filters && rowDefinition.dataField in
    tableDefinition.state.filters ? tableDefinition.state.filters[rowDefinition.dataField] : null;
    let defaultValue = filter ? filter.filterVal : undefined;
    let comparator = filter ? filter.comparator : undefined;

    if (_.isBoolean(defaultValue)) {
        defaultValue = defaultValue ? 1 : 0;
    }

    switch (rowDefinition.type) {
        case TableDefinitionType.NUMBER:
            return numberFilter({
                ...{
                    placeholder,
                    defaultValue: defaultValue ? {number: defaultValue, comparator} :
                        {comparator: TableFilterOperator.EQ},
                    className: `${tableDefinition.id}-${rowDefinition.id}`,
                }, ...filterOptions
            });
        case TableDefinitionType.SELECT:
            if (filterOptions.translate) {
                let translated = {};
                filterOptions = {...filterOptions};
                for (let key in filterOptions.options) {
                    translated[key] = filterOptions.options[key] in messages ?
                        intl.formatMessage(messages[filterOptions.options[key]]) : filterOptions.options[key];
                }
                filterOptions.options = translated;
            }

            if (!isLocal) {
                filterOptions.options = {...{[EMPTY_FILTER_VALUE]: placeholder}, ...filterOptions.options};
                defaultValue = defaultValue || EMPTY_FILTER_VALUE;
            }

            return selectFilter({
                ...{
                    withoutEmptyOption: !isLocal,
                    placeholder,
                    defaultValue,
                    className: `${tableDefinition.id}-${rowDefinition.id}`,
                }, ..._.omit(filterOptions, 'translate')
            });
        case TableDefinitionType.DATE:
            return dateFilter({
                ...{
                    placeholder,
                    defaultValue: defaultValue ?
                        {
                            date: _.isDate(defaultValue) ? defaultValue :
                                new Date(/^[0-9]+$/.test(defaultValue) ? defaultValue * 1000 : defaultValue),
                            comparator: comparator ? comparator : TableFilterOperator.GT,
                        } : {comparator: TableFilterOperator.GT},
                    className: `${tableDefinition.id}-${rowDefinition.id}`,
                }, ...filterOptions
            });
        default:
            return textFilter({
                ...{
                    placeholder,
                    defaultValue,
                    className: `${tableDefinition.id}-${rowDefinition.id}`,
                }, ...filterOptions
            });
    }
}

export function mergeTableState(oldState: TableState, newState: TableState, remote: boolean) {
    let filters = {...oldState.filters, ...newState.filters};
    for (let key in filters) {
        if (filters[key] === null || (remote && !(key in newState.filters)) ||
            (
                (_.isObject(filters[key].filterVal) && 'comparator' in filters[key].filterVal &&
                    (filters[key].filterVal.comparator == '' ||
                        (
                            ('date' in filters[key].filterVal && filters[key].filterVal.date === EMPTY_FILTER_VALUE) ||
                            ('number' in filters[key].filterVal && filters[key].filterVal.number === EMPTY_FILTER_VALUE)
                        )
                    )
                )
            ) || (_.isString(filters[key].filterVal) && filters[key].filterVal === EMPTY_FILTER_VALUE)
        ) {
            delete filters[key];
        }
        else {
            if (_.isObject(filters[key].filterVal) && 'date' in filters[key].filterVal &&
                _.isDate(filters[key].filterVal.date)) {
                filters[key].filterVal.date = filters[key].filterVal.date.toISOString();
            }
        }
    }
    return removeUndefinedKeys({...oldState, ...newState, ...{filters: _.isEmpty(filters) ? undefined : filters}});
}


export function writeTableStateToUrl(name: string, tableState: TableState) {
    writeToHash({
        [name]: _.omit(tableState, 'totalSize'),
    });
}

export function readTableStateFromUrl(name: string, defaultTableState: TableState = null): Partial<TableState> {
    let object = readFromHash(null, {});
    if (name in object) {
        return object[name];
    }

    if (defaultTableState) {
        setTimeout(() => writeTableStateToUrl(name, defaultTableState));
        return defaultTableState;
    }
}

export function getDefaultSizePerPageList(): Array<{ text: string; value: number }> {
    return [
        {text: '25', value: 25},
        {text: '50', value: 50},
        {text: '100', value: 100}
    ];
}

export function resourcesToOptions(data: Array<any>, label: string = 'name', key: string = 'id',
                                   labelCallback: (label: string, row: any) => string = null,
                                   keyCallback: (key: string, row: any) => string = null) {
    let collection = {};
    data.forEach(v => {
        let l = v[name];
        if (labelCallback) {
            l = labelCallback(l, v);
        }
        let k = v[key];
        if (keyCallback) {
            k = keyCallback(k, v);
        }
        collection[k] = l;
    });
    return collection;
}