import * as React from 'react';
import * as PropTypes from "prop-types";
import {BrowserRouter, BrowserRouterProps} from "react-router-dom";
import {RouteComponentProps, withRouter} from "react-router";

export default class AppRouter extends React.Component<BrowserRouterProps> {

    render() {
        const {children, ...props} = this.props;
        const AppRouterInjectorComponent = withRouter(AppRouterInjector);
        return (
            <BrowserRouter {...props}>
                <AppRouterInjectorComponent>
                    {children}
                </AppRouterInjectorComponent>
            </BrowserRouter>
        );
    }

}

class AppRouterInjector extends React.Component<RouteComponentProps<any>> {

    static childContextTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };

    getChildContext() {
        return {
            match: this.props.match,
            location: this.props.location,
            history: this.props.history,
        }
    }

    render() {
        return this.props.children;
    }

}