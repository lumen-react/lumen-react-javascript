import * as React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {AbstractComponent, mergeTableState, removeUndefinedKeys, TableState, TableStateChanged} from '..';

export class BootstrapTableWrapperComponent<T, U> extends AbstractComponent<T, U> {

    protected tableState: TableState = {};
    protected lastTableState: TableState;

    constructor(props, context) {
        super(props, context);
        this.tableState = props.state;
    }

    shouldComponentUpdate() {
        return this.isRemote();
    }

    private isRemote(): boolean {
        return !!(this.props as any).remote;
    }

    private onTableChangeCallback(onTableChange) {
        return (type: 'filter' | 'pagination' | 'sort' | 'cellEdit', newState: TableStateChanged<any>) => {
            newState = removeUndefinedKeys(newState);
            this.lastTableState = newState;
            this.tableState = mergeTableState(this.tableState, newState, (this.props as any).remote);
            if (onTableChange) {
                onTableChange(type, this.tableState);
            }
        };
    }

    render() {
        let props = {...this.props as any};
        props.onTableChange = this.onTableChangeCallback(props.onTableChange);
        if (!this.isRemote()) {
            // Switch out pagination
            props.paginationOptions.onPageChange = props.onPaginationChange(props.onTableChange);
            props.paginationOptions.onSizePerPageChange = props.onPaginationChange(props.onTableChange);
            props.pagination = paginationFactory(props.paginationOptions);
            // Switch out sorting
            for (let i = 0; i < props.columns.length; i++) {
                props.columns[i].onSort = props.columns[i].onSort(props.onTableChange);
            }
            // Switch out filters
            props.ref = props.onFilterChange(props.onTableChange);
        }

        let rowEvents: any = {};
        ['Click', 'DoubleClick', 'MouseEnter', 'MouseLeave'].forEach(event => {
            let rowEvent = `onRow${event}`;
            if (props[rowEvent]) {
                rowEvents[`on${event}`] = props[rowEvent];
                delete props[rowEvent];
            }
        });
        props.rowEvents = rowEvents;

        return (
            <BootstrapTable {...props}/>
        );
    }

}

export class BootstrapTableWrapper extends BootstrapTableWrapperComponent<any, any> {

}

export class BootstrapTableWrapperStateless<T> extends BootstrapTableWrapperComponent<T, any> {

}