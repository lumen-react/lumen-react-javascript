import * as React from 'react';
import * as _ from 'lodash';
import {dateFilter, numberFilter, selectFilter, textFilter} from 'react-bootstrap-table2-filter';
import {
    AbstractComponent, AjaxLoaderInjectedProps, AxiosRequestConfiguration, connector, createTableProps,
    readTableStateFromUrl, TableDefinition, TableRowDefinition, TableState, TableStateChanged,
    tableStateToRequestConfig, writeTableStateToUrl
} from '..';
import {BootstrapTableWrapper} from './bootstrap-table-wrapper';
import {TableProps as BootstrapTableProps} from 'react-bootstrap';
import {compose, defaultProps, withProps} from 'recompose';
import {History} from 'history';
import {default as axios} from 'axios';

export interface TableComponentProps<T = any, U = any> extends BootstrapTableProps, AjaxLoaderInjectedProps {
    id?: string;
    remote?: boolean;
    tableData?: Array<T>;
    tableState?: TableState;
    messages?: { [key: string]: { id: string, defaultMessage: string } };
    rowDefinitions?: Array<TableRowDefinition<T>> | ((props: TableComponentProps<T, U>) => Array<TableRowDefinition<T>>);
    onRowClick?: (rowEvent: { id: any, history: History, e: any, row: any, rowIndex: number, props: TableComponentProps<T, U> }) => void;
    onTableChange?: (type: string, remoteProps: TableStateChanged<T>) => void;
    tableInitializer?: { tableData: Array<T>, tableState?: TableState };
    requestConfigCallback?: (requestConfig: Partial<AxiosRequestConfiguration>) => Promise<{ tableData: Array<T>, tableState?: TableState }>;
    tableDefinition?: TableDefinition<T>;
    writeToUrl?: boolean;
    /* DEFAULTS */
    store?: any;
    classes?: any;
    noDataIndication?: any;
    caption?: any;
    rowStyle?: any;
    rowClasses?: any;
    wrapperClasses?: any;
    extra?: U;
}

export interface TableComponentState<T = any> {
    tableData: Array<T>;
    tableState?: TableState;
}

export class TableComponent<T = any, U extends TableComponentProps<T> = TableComponentProps<T>, V extends TableComponentState<T> = TableComponentState<T>> extends AbstractComponent<U, V> {

    static defaultProps = {
        id: 'default',
        messages: {},
        rowDefinitions: [],
        remote: true,
        tableData: [],
        tableState: {},
        writeToUrl: true,
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            tableData: 'tableInitializer' in this.props && this.props.tableInitializer && 'tableData' in
            this.props.tableInitializer ? this.props.tableInitializer.tableData : this.props.tableData,
            tableState: this.props.remote ? this.props.tableInitializer.tableState :
                this.props.writeToUrl ? readTableStateFromUrl(this.props.id, this.props.tableState) :
                    this.props.tableState,
        } as any;
    }

    renderTable() {
        let onTableChange = (type, remoteProps) => {
            if (this.props.requestConfigCallback) {
                if (this.props.remote) {
                    this.props.requestConfigCallback(
                        tableStateToRequestConfig(
                            remoteProps,
                            {},
                            this.props.id
                        )
                    ).then(data => this.setState(
                        data,
                        () => {
                            if (this.props.writeToUrl) {
                                writeTableStateToUrl(
                                    this.props.id,
                                    this.state.tableState
                                );
                            }
                            if (this.props.onTableChange) {
                                this.props.onTableChange(type, remoteProps);
                            }
                        }
                    )).catch(error => {
                        if (!axios.isCancel(error)) {
                            return Promise.reject(error);
                        }
                    });
                }
                else {
                    if (this.props.writeToUrl) {
                        setTimeout(() => {
                            writeTableStateToUrl(this.props.id, remoteProps);
                            if (this.props.onTableChange) {
                                this.props.onTableChange(type, remoteProps);
                            }
                        });
                    }
                    else {
                        if (this.props.onTableChange) {
                            this.props.onTableChange(type, remoteProps);
                        }
                    }
                }
            }
            else {
                this.props.remote && console.warn('No request config callback set');
            }
        };

        let tableDefinition: TableDefinition<T> = {
            ...this.props.tableDefinition as any, ...{
                id: this.props.id,
                state: this.state.tableState,
                remote: this.props.remote,
                onTableChange,
                rowDefinitions: _.isFunction(this.props.rowDefinitions) ? this.props.rowDefinitions(this.props) :
                    this.props.rowDefinitions,
            }
        };

        let tableProps = createTableProps<T>(
            this.state.tableData,
            tableDefinition,
            this.props.messages,
            this.intl
        );

        let onRowClick = (e: any, row: any, rowIndex: number) => {
            if (this.props.onRowClick) {
                this.props.onRowClick({
                    id: row[tableProps.keyField],
                    history: this.history,
                    e,
                    row,
                    rowIndex,
                    props: this.props,
                });
            }
        };

        let props = {
            ..._.omit(this.props as any, 'id', 'remote', 'tableData', 'tableState', 'messages', 'rowDefinitions',
                'onTableChange', 'tableInitializer', 'requestConfigCallback', 'tableDefinition',
                'writeToUrl'), ...{onRowClick}
        };

        return <BootstrapTableWrapper {...tableProps} {...props}/>;
    }

    render() {
        return this.renderTable();
    }

}

export class TableComponentTable<T extends TableComponentProps = TableComponentProps, U extends TableComponentState = TableComponentState> extends TableComponent<any, T, U> {
}

export function tableLoader<T extends TableComponentProps = TableComponentProps>(remoteTable,
                                                                                 tableProps: TableComponentProps = {id: 'default'}) {
    return compose<T, T>(
        defaultProps({
            messages: {},
            rowDefinitions: [],
            remote: true,
            tableState: {},
            writeToUrl: true,
        }),
        withProps(tableProps)
    )(
        connector(
            remoteTable,
            {
                ajaxLoaderOptions: {
                    promises: {
                        tableInitializer: (props) =>
                            props.requestConfigCallback ? props.requestConfigCallback(
                                props.remote ? tableStateToRequestConfig(
                                    props.writeToUrl ? readTableStateFromUrl(props.id, props.tableState) :
                                        props.tableState,
                                    {},
                                    props.id
                                ) : {},
                            ) : props.remote && console.warn('No request config callback set')
                    }
                }
            }
        )
    );
}