import * as React from 'react';
import {AbstractComponent} from './abstract-component';
import {resolveLoader} from '..';
import Empty from './empty';
import * as _ from 'lodash';

export interface LoaderWrapperProps {
    showLoader: boolean | (() => Promise<any>);
    asOverlay?: boolean;
    children?: (Array<JSX.Element> | JSX.Element | string) | ((renderer: () => Promise<any>) => (Array<JSX.Element> | JSX.Element | string));
}

export interface LoaderWrapperState {
    loadingPromise: boolean;
}

export default class LoaderWrapper extends AbstractComponent<LoaderWrapperProps, LoaderWrapperState> {

    static defaultProps = {
        asOverlay: false,
    };

    state = {
        loadingPromise: false,
    };
    render() {
        let {showLoader, asOverlay, children} = this.props;
        if (_.isBoolean(showLoader)) {
            return (
                <Empty>
                    {(!showLoader || asOverlay) && children}
                    {showLoader && this.renderLoader()}
                </Empty>
            );
        }
        else if (_.isFunction(showLoader)) {
            let promiseCallback = () => {
                return new Promise((resolve, reject) => {
                    this.setState({loadingPromise: true}, () => {
                        let passedPromise = (showLoader as () => Promise<any>)();
                        passedPromise.then(d => {
                            this.setState({loadingPromise: false}, () => resolve(d));
                        }).catch(e => {
                            this.setState({loadingPromise: false}, () => reject(e));
                        });
                    });
                });
            };
            return (
                <Empty>
                    {(!this.state.loadingPromise || asOverlay) && (children as (renderer: () => Promise<any>) => (Array<JSX.Element> | JSX.Element | string))(
                        promiseCallback
                    )}
                    {this.state.loadingPromise && this.renderLoader()}
                </Empty>
            );
        }
        else {
            return _.isFunction(children) ? children(() => undefined) : children;
        }
    }

    private renderLoader() {
        return (
            <div style={{
                zIndex: 1,
                position: 'absolute',
                left: 0,
                top: 0,
                display: 'flex',
                width: '100%',
                height: '100%',
            }}>
                {resolveLoader(this)}
            </div>
        );
    }

}