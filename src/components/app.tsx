import * as React from 'react';
import * as PropTypes from 'prop-types';
import {InjectedIntlProps, injectIntl} from 'react-intl';
import {RouteProps} from 'react-router';
import {compose} from 'recompose';
import {AxiosInstance, AxiosRequestConfig} from 'axios';
import {setupAxiosInstance} from '../utils/axios-utils';
import {setupKeyRegister} from '../utils/window-utils';

interface Props extends InjectedIntlProps, RouteProps {
    axios: AxiosInstance;
    axiosDefaultRequestConfig?: AxiosRequestConfig;
}

class App extends React.Component<Props> {

    static childContextTypes = {
        intl: PropTypes.object.isRequired,
    };

    constructor(props, context) {
        super(props, context);
        setupKeyRegister();
        setupAxiosInstance(this.props.axios, this.props.axiosDefaultRequestConfig);
    }

    getChildContext() {
        return {
            intl: this.props.intl,
        };
    }

    render() {
        return this.props.children;
    }
};

export function appComposer(...functions) {
    return compose(injectIntl, ...functions)(App);
}