import * as React from 'react';
import {Tab, TabProps, Tabs, TabsProps} from 'react-bootstrap';
import {AbstractComponent} from './abstract-component';
import * as _ from 'lodash';
import {readFromHash, writeToHash} from '..';

export interface TabDefinition extends TabProps {
    mountOnEnter?: boolean;
    cache?: boolean;
    loader?: JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string> | (() => JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string>);
    contentRenderer: JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string> | (() => JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string>);
    isDefault?: boolean;
    nullOnDisabled?: boolean;
    preloadTab?: boolean;
    promise?: boolean;
    enabled?: boolean;
    loaderPromise?: boolean;
}

export interface TabsComponentProps extends TabsProps {
    writeToUrl?: boolean;
    defaultOpen?: boolean;
    tabsDefinition: Array<TabDefinition>;
    preloadTabs?: boolean;
}

export interface TabsComponentState {
    key: any;
}

export class TabsComponent<T extends TabsComponentProps = TabsComponentProps, U extends TabsComponentState = TabsComponentState> extends AbstractComponent<T, U> {

    static defaultProps = {
        writeToUrl: true,
        defaultOpen: true,
    };

    private initialRenderers = {};
    private contentRenderers = {};
    private loaders = {};

    private renderedContent = {};
    private renderedLoader = {};

    constructor(props, context) {
        super(props, context);
        this.handleSelect = this.handleSelect.bind(this);
        this.state = {key: null} as U;
    }

    componentDidMount() {
        let defaultKey = null;

        // First check URL
        if ((this.props as T).writeToUrl) {
            defaultKey = readFromHash((this.props as T).id);
        }

        // Then check tableDefs
        let defaultTabDef = (this.props as T).tabsDefinition.find(tabDefinition => tabDefinition.isDefault);
        if (defaultTabDef) {
            defaultKey = defaultTabDef.eventKey as string;
        }

        // Lastly check if it's default open, if so take the first tab
        if (!defaultKey && (this.props as T).defaultOpen && (this.props as T).tabsDefinition[0]) {
            defaultKey = (this.props as T).tabsDefinition[0].eventKey as string;
        }

        (this.props as T).tabsDefinition
            .filter(tabDefinition => !('enabled' in tabDefinition) || tabDefinition.enabled)
            .forEach(tabDefinition => {
                const eventKey = tabDefinition.eventKey as string;
                this.initialRenderers[eventKey] = tabDefinition.contentRenderer;
                this.contentRenderers[eventKey] = tabDefinition.contentRenderer;
                this.loaders[eventKey] = tabDefinition.loader;

                if (defaultKey != eventKey) {
                    let preload = 'preloadTab' in tabDefinition ? tabDefinition.preloadTab :
                        ('preloadTabs' in this.props ? this.props.preloadTabs : false);
                    if (preload) {
                        this.loadTab(tabDefinition);
                    }
                }
            });

        let defaultTab = (this.props as T).tabsDefinition
            .filter(tabDefinition => (tabDefinition.eventKey as string) == defaultKey)[0];

        this.handleSelect(defaultTab.eventKey);
    }

    componentDidUpdate() {
        (this.props as T).tabsDefinition
            .filter(tabDefinition => !('enabled' in tabDefinition) || tabDefinition.enabled)
            .forEach(tabDefinition => {
                const eventKey = tabDefinition.eventKey as string;
                if (!this.initialRenderers[eventKey]) {
                    this.initialRenderers[eventKey] = tabDefinition.contentRenderer;
                }
                if (!this.contentRenderers[eventKey]) {
                    this.contentRenderers[eventKey] = tabDefinition.contentRenderer;
                }
                if (!this.loaders[eventKey]) {
                    this.loaders[eventKey] = tabDefinition.loader;
                }

                let preload = 'preloadTab' in tabDefinition ? tabDefinition.preloadTab :
                    ('preloadTabs' in this.props ? this.props.preloadTabs : false);
                if (preload) {
                    this.loadTab(tabDefinition);
                }
            });
    }

    private handleSelect(key) {
        let tabDefinition = (this.props as T).tabsDefinition.find(t => t.eventKey == key);

        this.loadTab(tabDefinition);

        this.setState({key}, () => {
            if ((this.props as T).writeToUrl) {
                writeToHash({[(this.props as T).id]: tabDefinition.eventKey as string});
            }
        });
    }

    private loadTab(tabDefinition, fromRender = false) {
        let key = tabDefinition.eventKey;
        let cache = !('cache' in tabDefinition) ? false : tabDefinition.cache;

        let contentPromise = !('promise' in tabDefinition) ? false : tabDefinition.promise;
        let loaderPromise = !('loaderPromise' in tabDefinition) ? false : tabDefinition.loaderPromise;
        let nullOnDisabled = !('nullOnDisabled' in tabDefinition) ? true : tabDefinition.nullOnDisabled;

        if (tabDefinition.disabled && nullOnDisabled) {
            this.renderedContent[key] = null;
            return;
        }

        let content = this.contentRenderers[key];
        let loader = this.loaders[key];

        if (_.isFunction(content)) {
            if (!contentPromise || (contentPromise && !fromRender)) {
                content = content();

                if (_.isObject(content) && 'then' in content) {
                    this.renderedContent[key] = null;
                    this.contentRenderers[key] = null;
                    content.then(renderer => {
                        this.contentRenderers[key] = renderer;
                        setTimeout(() => this.loadTab(tabDefinition, fromRender));
                    });
                }
                else {
                    if (!_.isEqual(this.renderedContent[key], content)) {
                        this.renderedContent[key] = content;
                        if (!fromRender) {
                            this.forceUpdate(() => {
                                if (!cache) {
                                    this.contentRenderers[key] = this.initialRenderers[key];
                                }
                            });
                        }
                    }
                }
            }
        }
        else if (!(_.isObject(content) && 'then' in content)) {
            if (!_.isEqual(this.renderedContent[key], content)) {
                this.renderedContent[key] = content;
                if (!fromRender) {
                    this.forceUpdate(() => {
                        if (!cache) {
                            this.contentRenderers[key] = this.initialRenderers[key];
                        }
                    });
                }
            }
        }

        if (_.isFunction(loader)) {
            if (!loaderPromise || (loaderPromise && !fromRender)) {
                loader = loader();

                if (_.isObject(loader) && 'then' in loader) {
                    this.renderedLoader[key] = null;
                    this.loaders[key] = null;
                    loader.then(renderer => {
                        this.loaders[key] = renderer;
                        setTimeout(() => this.loadTab(tabDefinition, fromRender));
                    });
                }
                else {
                    if (!_.isEqual(this.renderedLoader[key], loader)) {
                        this.renderedLoader[key] = loader;
                        if (!fromRender) {
                            this.forceUpdate();
                        }
                    }
                }
            }
        }
        else if (!(_.isObject(loader) && 'then' in loader)) {
            if (!_.isEqual(this.renderedLoader[key], loader)) {
                this.renderedLoader[key] = loader;
                if (!fromRender) {
                    this.forceUpdate();
                }
            }
        }
    }

    render() {
        return this.renderTabs();
    }

    renderTabs() {
        let props = _.omit(this.props, 'children', 'tabsDefinition', 'writeToUrl', 'defaultOpen', 'preloadTabs');
        let children = [];

        (this.props as T).tabsDefinition
            .filter(tabDefinition => !('enabled' in tabDefinition) || tabDefinition.enabled)
            .forEach(tabDefinition => {
                const eventKey = tabDefinition.eventKey as string;
                let tabProps:TabProps = _.omit(tabDefinition, 'cache', 'loader', 'contentRenderer', 'isDefault',
                    'nullOnDisabled', 'preloadTab', 'promise', 'loaderPromise', 'enabled');

                this.loadTab(tabDefinition, true);

                let content = this.renderedContent[eventKey] ||
                    this.renderedLoader[eventKey];

                children.push(React.cloneElement(
                    <Tab key={eventKey} {...tabProps as any}>
                        {content}
                    </Tab>
                ));
            });

        return (
            <Tabs {...props} activeKey={this.state.key} onSelect={this.handleSelect}>
                {children}
            </Tabs>
        );
    }

}
