import * as React from 'react';
import * as PropTypes from "prop-types";
import {InjectedIntl} from 'react-intl';
import {match} from "react-router";
import {History, Location} from "history";

export abstract class AbstractComponent<T = {}, U = {}> extends React.Component<T, U> {

    protected intl: InjectedIntl;
    protected match: match<any>;
    protected location: Location;
    protected history: History;

    static contextTypes = {
        intl: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };

    constructor(props: T, context: any) {
        super(props, context);
        this.intl = this.context.intl;
        this.match = this.context.match;
        this.location = this.context.location;
        this.history  = this.context.history;
        let preparedState = this.prepareState();
        if (preparedState) {
            this.state = preparedState;
        }
    }

    protected prepareState(): U {
        return null;
    }

    public setDelayedState(clearState = {}) {
        let currentState = {...this.state as any};
        this.state = {...currentState, ...clearState};
        setTimeout(() => this.setState(currentState));
    }
}

export abstract class AbstractStatelessComponent<T = {}> extends AbstractComponent<T, {}> {
}

export abstract class AbstractSimpleComponent extends AbstractComponent<{}, {}> {
}

export abstract class AbstractPureComponent<T = {}, U = {}> extends React.PureComponent<T, U> {

    protected intl: InjectedIntl;
    protected match: match<any>;
    protected location: Location;
    protected history: History;

    static contextTypes = {
        intl: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    };

    constructor(props: T, context: any) {
        super(props, context);
        this.intl = this.context.intl;
        this.match = this.context.match;
        this.location = this.context.location;
        this.history  = this.context.history;
        let preparedState = this.prepareState();
        if (preparedState) {
            this.state = preparedState;
        }
    }

    protected prepareState(): U {
        return null;
    }

    public setDelayedState(clearState = {}) {
        let currentState = {...this.state as any};
        this.state = {...currentState, ...clearState};
        setTimeout(() => this.setState(currentState));
    }
}

export abstract class AbstractStatelessPureComponent<T> extends AbstractPureComponent<T, {}> {
}

export abstract class AbstractSimplePureComponent extends AbstractPureComponent<{}, {}> {
}