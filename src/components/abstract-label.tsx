import * as React from 'react';
import {AbstractComponent} from "./abstract-component";

export enum LabelType {
    SIMPLE,
    SMALL,
    MEDIUM,
    LARGE,
    SEMIFULL,
    FULL,
}

export interface LabelProps extends React.HTMLProps<HTMLSpanElement> {
    labelType?: LabelType;
    thumbnailOnly?: boolean;
    avatarProps?: React.HTMLProps<HTMLSpanElement>;
    descriptionProps?: React.HTMLProps<HTMLSpanElement>;
    renderThumbnail?: boolean;
}

export default abstract class AbstractLabel<T extends LabelProps, U> extends AbstractComponent<T, any> {

    static defaultProps = {
        labelType: LabelType.SIMPLE,
        thumbnailOnly: false,
        renderThumbnail: true,
    };

    public static sizeKeyMap = {
        [LabelType.SMALL]: '20x20',
        [LabelType.MEDIUM]: '40x40',
        [LabelType.LARGE]: '60x60',
        [LabelType.SEMIFULL]: '100x100',
        [LabelType.FULL]: '100x100',
    };

    protected abstract getName(): string;

    protected abstract hasThumbnail(): boolean;

    protected abstract getThumbnail(): string;

    protected abstract getGlyph(): string;

    protected abstract getSub(): string | JSX.Element;

    protected abstract getContent(): string | JSX.Element;

    protected abstract renderMissingThumbnal(): string | JSX.Element;

    protected getCleanedProps(): Partial<T> {
        return this.props;
    }

    private renderThumbnail() {
        return this.hasThumbnail() ?
            <img src={this.getThumbnail()}
                 title={this.getName()}/> :
            this.renderMissingThumbnal();
    }

    protected renderSimpleLabel(elementProps, avatarProps, descriptionProps) {
        return this.props.thumbnailOnly ? null : <div {...elementProps}>{this.getName()}</div>;
    }

    protected renderSmallLabel(elementProps, avatarProps, descriptionProps) {
        return (
            <div {...elementProps}>
                {this.props.renderThumbnail && (
                    <div {...avatarProps}>
                        {this.renderThumbnail()}
                    </div>
                )}
                {this.props.thumbnailOnly ? null : (
                    <div {...descriptionProps}>
                        <div>{this.getName()}</div>
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }

    protected renderMediumLabel(elementProps, avatarProps, descriptionProps) {
        return (
            <div {...elementProps}>
                {this.props.renderThumbnail && (
                    <div {...avatarProps}>
                        {this.renderThumbnail()}
                    </div>
                )}
                {this.props.thumbnailOnly ? null : (
                    <div {...descriptionProps}>
                        <div>{this.getName()}</div>
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }

    protected renderLargeLabel(elementProps, avatarProps, descriptionProps) {
        return (
            <div {...elementProps}>
                {this.props.renderThumbnail && (
                    <div className="avatar-container">
                        <div {...avatarProps}>
                            {this.renderThumbnail()}
                        </div>
                    </div>
                )}
                {this.props.thumbnailOnly ? null : (
                    <div className="description-container">
                        <div {...descriptionProps}>
                            <div>{this.getName()}</div>
                            <div className="sub">{this.getSub()}</div>
                        </div>
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }

    protected renderSemiFullLabel(elementProps, avatarProps, descriptionProps) {
        return (
            <div {...elementProps}>
                {this.props.renderThumbnail && (
                    <div className="avatar-container">
                        <div {...avatarProps}>
                            {this.renderThumbnail()}
                        </div>
                    </div>
                )}
                {this.props.thumbnailOnly ? null : (
                    <div className="description-container">
                        <div {...descriptionProps}>
                            <h4>{this.getName()}</h4>
                            <div className="content">{this.getContent()}</div>
                        </div>
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }

    protected renderFullLabel(elementProps, avatarProps, descriptionProps) {
        return (
            <div {...elementProps}>
                {this.props.renderThumbnail && (
                    <div className="avatar-container">
                        <div {...avatarProps}>
                            {this.renderThumbnail()}
                        </div>
                    </div>
                )}
                {this.props.thumbnailOnly ? null : (
                    <div className="description-container">
                        <div {...descriptionProps}>
                            <h4>{this.getName()}</h4>
                            <div className="sub">{this.getSub()}</div>
                            <div className="content">{this.getContent()}</div>
                        </div>
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }

    render() {
        let cleanedProps = this.getCleanedProps();
        const {labelType, avatarProps, descriptionProps, thumbnailOnly, renderThumbnail, ...elementProps} = cleanedProps as any;
        elementProps.className = elementProps.className ? `${elementProps.className} object-label` : 'object-label';

        let avatarObject = {...avatarProps};
        let descriptionObject = {...descriptionProps};

        let sizeClassName = "";
        switch (labelType) {
            case LabelType.FULL:
            case LabelType.SEMIFULL:
                sizeClassName = "full";
                break;
            case LabelType.LARGE:
                sizeClassName = "large";
                break;
            case LabelType.MEDIUM:
                sizeClassName = "medium";
                break;
            case LabelType.SMALL:
                sizeClassName = "small";
                break;
            case LabelType.SIMPLE:
            default:
                sizeClassName = "simple";
                break;
        }

        avatarObject.className = avatarObject.className ? `${avatarObject.className} thumbnail avatar avatar-${sizeClassName}` : `thumbnail avatar avatar-${sizeClassName}`;
        descriptionObject.className = descriptionObject.className ? `${descriptionObject.className} description description-${sizeClassName}${renderThumbnail ? '' : ' no-avatar'}` : `description description-${sizeClassName}${renderThumbnail ? '' : ' no-avatar'}`;

        switch (labelType) {
            case LabelType.FULL:
                return this.renderFullLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.SEMIFULL:
                return this.renderSemiFullLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.LARGE:
                return this.renderLargeLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.MEDIUM:
                return this.renderMediumLabel(elementProps, avatarObject, descriptionObject);
            case LabelType.SMALL:
                return this.renderSmallLabel(elementProps, avatarObject, descriptionObject);
            default:
                return this.renderSimpleLabel(elementProps, avatarObject, descriptionObject);
        }
    }

}