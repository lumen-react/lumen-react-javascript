import * as React from 'react';
import {AbstractSimpleComponent} from './abstract-component';

export default class Empty extends AbstractSimpleComponent {

    render() {
        return this.props.children;
    }

}