import * as React from 'react';
export interface AjaxLoaderState {
    ready: boolean;
    isRefresh: boolean;
}
export interface AjaxLoaderExternalProps {
}
export interface AjaxLoaderInjectedProps {
    refresh?: () => void;
}
export interface AjaxLoaderOptions {
    promises: {
        [key: string]: (props: any) => PromiseLike<any>;
    };
    loader?: any;
}
export declare const ajaxLoader: (options: AjaxLoaderOptions) => <T extends {}>(Component: React.ComponentType<T & AjaxLoaderInjectedProps>) => {
    new (props: T & AjaxLoaderExternalProps): {
        promises: {
            [key: string]: {
                promise: PromiseLike<any>;
                result: any;
                done: boolean;
            };
        };
        promiseResults: {
            [key: string]: any;
        };
        readonly loader: any;
        refresh(): void;
        render(): any;
        context: any;
        setState<K extends "ready" | "isRefresh">(state: AjaxLoaderState | ((prevState: Readonly<AjaxLoaderState>, props: Readonly<T & AjaxLoaderExternalProps>) => AjaxLoaderState | Pick<AjaxLoaderState, K>) | Pick<AjaxLoaderState, K>, callback?: () => void): void;
        forceUpdate(callBack?: () => void): void;
        readonly props: Readonly<T & AjaxLoaderExternalProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<AjaxLoaderState>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<T & AjaxLoaderExternalProps>, nextState: Readonly<AjaxLoaderState>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<T & AjaxLoaderExternalProps>, prevState: Readonly<AjaxLoaderState>): any;
        componentDidUpdate?(prevProps: Readonly<T & AjaxLoaderExternalProps>, prevState: Readonly<AjaxLoaderState>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<T & AjaxLoaderExternalProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<T & AjaxLoaderExternalProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<T & AjaxLoaderExternalProps>, nextState: Readonly<AjaxLoaderState>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<T & AjaxLoaderExternalProps>, nextState: Readonly<AjaxLoaderState>, nextContext: any): void;
    };
    displayName: string;
    contextType?: React.Context<any>;
};
