import * as React from 'react';
export interface EventLoaderState {
    ready: boolean;
    passedProps: {};
}
export interface EventLoaderExternalProps {
}
export interface EventLoaderInjectedProps {
}
export interface EventLoaderOptions {
    requireTriggers?: boolean | Array<string> | string;
    types: Array<string> | string;
    mapEventDetailsToProps: <T = {
        [key: string]: any;
    }>(detail: T, type: string) => {
        [key: string]: any;
    };
    loader?: any;
}
export declare const eventLoader: (options: EventLoaderOptions) => <T extends {}>(Component: React.ComponentType<T & EventLoaderInjectedProps>) => {
    new (props: T & EventLoaderExternalProps): {
        readonly ready: any[];
        readonly loader: any;
        render(): any;
        context: any;
        setState<K extends "ready" | "passedProps">(state: EventLoaderState | ((prevState: Readonly<EventLoaderState>, props: Readonly<T & EventLoaderExternalProps>) => EventLoaderState | Pick<EventLoaderState, K>) | Pick<EventLoaderState, K>, callback?: () => void): void;
        forceUpdate(callBack?: () => void): void;
        readonly props: Readonly<T & EventLoaderExternalProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<EventLoaderState>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<T & EventLoaderExternalProps>, nextState: Readonly<EventLoaderState>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<T & EventLoaderExternalProps>, prevState: Readonly<EventLoaderState>): any;
        componentDidUpdate?(prevProps: Readonly<T & EventLoaderExternalProps>, prevState: Readonly<EventLoaderState>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<T & EventLoaderExternalProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<T & EventLoaderExternalProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<T & EventLoaderExternalProps>, nextState: Readonly<EventLoaderState>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<T & EventLoaderExternalProps>, nextState: Readonly<EventLoaderState>, nextContext: any): void;
    };
    displayName: string;
    contextType?: React.Context<any>;
};
