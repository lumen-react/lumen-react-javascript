import * as React from 'react';
import { GeolocatedProps } from "react-geolocated";
export interface LocationLoaderState {
}
export interface LocationLoaderExternalProps extends LocationLoaderOptions, GeolocatedProps {
}
export interface LocationLoaderInjectedProps {
    locationReady?: boolean;
    hasLocation?: boolean;
    handleLocationCallbacks?: (callbacks: LocationLoaderOptions) => void;
}
export interface LocationLoaderOptions {
    onLocationEnabled?: (coords: Coordinates) => void;
    onLocationUpdated?: (coords: Coordinates) => void;
    onLocationDisabled?: () => void;
    onLocationReady?: (enabled: boolean, coords: Coordinates) => void;
}
export declare const locationLoader: (options?: LocationLoaderOptions) => <T extends {}>(Component: React.ComponentType<T & LocationLoaderInjectedProps>) => {
    new (props: Readonly<T & LocationLoaderExternalProps>): {
        locationReady: boolean;
        hasLocation: boolean;
        init: boolean;
        lastCoords: any;
        triggerCoords: boolean;
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: LocationLoaderState | ((prevState: Readonly<LocationLoaderState>, props: Readonly<T & LocationLoaderExternalProps>) => LocationLoaderState | Pick<LocationLoaderState, K>) | Pick<LocationLoaderState, K>, callback?: () => void): void;
        forceUpdate(callBack?: () => void): void;
        readonly props: Readonly<T & LocationLoaderExternalProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<LocationLoaderState>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<T & LocationLoaderExternalProps>, prevState: Readonly<LocationLoaderState>): any;
        componentDidUpdate?(prevProps: Readonly<T & LocationLoaderExternalProps>, prevState: Readonly<LocationLoaderState>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): void;
    };
    new (props: T & LocationLoaderExternalProps, context?: any): {
        locationReady: boolean;
        hasLocation: boolean;
        init: boolean;
        lastCoords: any;
        triggerCoords: boolean;
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: LocationLoaderState | ((prevState: Readonly<LocationLoaderState>, props: Readonly<T & LocationLoaderExternalProps>) => LocationLoaderState | Pick<LocationLoaderState, K>) | Pick<LocationLoaderState, K>, callback?: () => void): void;
        forceUpdate(callBack?: () => void): void;
        readonly props: Readonly<T & LocationLoaderExternalProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<LocationLoaderState>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<T & LocationLoaderExternalProps>, prevState: Readonly<LocationLoaderState>): any;
        componentDidUpdate?(prevProps: Readonly<T & LocationLoaderExternalProps>, prevState: Readonly<LocationLoaderState>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<T & LocationLoaderExternalProps>, nextState: Readonly<LocationLoaderState>, nextContext: any): void;
    };
    displayName: string;
    contextType?: React.Context<any>;
};
