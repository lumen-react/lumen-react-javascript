import * as React from 'react';
export interface ConnectLoaderState {
    ready: boolean;
}
export interface ConnectLoaderExternalProps {
}
export interface ConnectLoaderInjectedProps {
}
export interface ConnectLoaderOptions {
    dispatchers: {
        [key: string]: (props: any, advance: () => void) => any;
    };
    loader?: any;
}
export declare const connectLoader: (options: ConnectLoaderOptions) => <T extends {}>(Component: React.ComponentType<T & ConnectLoaderInjectedProps>) => {
    new (props: T & ConnectLoaderExternalProps): {
        readonly dispatchers: {
            [key: string]: any;
        };
        readonly loader: any;
        advance(key: any): void;
        render(): any;
        context: any;
        setState<K extends "ready">(state: ConnectLoaderState | ((prevState: Readonly<ConnectLoaderState>, props: Readonly<T & ConnectLoaderExternalProps>) => ConnectLoaderState | Pick<ConnectLoaderState, K>) | Pick<ConnectLoaderState, K>, callback?: () => void): void;
        forceUpdate(callBack?: () => void): void;
        readonly props: Readonly<T & ConnectLoaderExternalProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<ConnectLoaderState>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<T & ConnectLoaderExternalProps>, nextState: Readonly<ConnectLoaderState>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<T & ConnectLoaderExternalProps>, prevState: Readonly<ConnectLoaderState>): any;
        componentDidUpdate?(prevProps: Readonly<T & ConnectLoaderExternalProps>, prevState: Readonly<ConnectLoaderState>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<T & ConnectLoaderExternalProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<T & ConnectLoaderExternalProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<T & ConnectLoaderExternalProps>, nextState: Readonly<ConnectLoaderState>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<T & ConnectLoaderExternalProps>, nextState: Readonly<ConnectLoaderState>, nextContext: any): void;
    };
    displayName: string;
    contextType?: React.Context<any>;
};
