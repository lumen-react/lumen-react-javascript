import { AbstractStatelessComponent } from '..';
export interface AnalyticsProps {
    location: {
        pathname: string;
        search: string;
    };
    reactGa: {
        set: ({ page: string }: {
            page: any;
        }) => void;
        pageview: (page: string) => void;
    };
}
export default class Analytics extends AbstractStatelessComponent<AnalyticsProps> {
    constructor(props: any, context: any);
    componentWillReceiveProps(nextProps: any): void;
    sendPageChange(pathname: any, search?: string): void;
    render(): any;
}
