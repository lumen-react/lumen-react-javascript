/// <reference types="react" />
import { AbstractComponent } from './abstract-component';
export interface LoaderWrapperProps {
    showLoader: boolean | (() => Promise<any>);
    asOverlay?: boolean;
    children?: (Array<JSX.Element> | JSX.Element | string) | ((renderer: () => Promise<any>) => (Array<JSX.Element> | JSX.Element | string));
}
export interface LoaderWrapperState {
    loadingPromise: boolean;
}
export default class LoaderWrapper extends AbstractComponent<LoaderWrapperProps, LoaderWrapperState> {
    static defaultProps: {
        asOverlay: boolean;
    };
    state: {
        loadingPromise: boolean;
    };
    render(): string | JSX.Element | JSX.Element[];
    private renderLoader;
}
