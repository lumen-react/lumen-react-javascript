import * as React from 'react';
import { AbstractComponent, AjaxLoaderInjectedProps, AxiosRequestConfiguration, TableDefinition, TableRowDefinition, TableState, TableStateChanged } from '..';
import { TableProps as BootstrapTableProps } from 'react-bootstrap';
import { History } from 'history';
export interface TableComponentProps<T = any, U = any> extends BootstrapTableProps, AjaxLoaderInjectedProps {
    id?: string;
    remote?: boolean;
    tableData?: Array<T>;
    tableState?: TableState;
    messages?: {
        [key: string]: {
            id: string;
            defaultMessage: string;
        };
    };
    rowDefinitions?: Array<TableRowDefinition<T>> | ((props: TableComponentProps<T, U>) => Array<TableRowDefinition<T>>);
    onRowClick?: (rowEvent: {
        id: any;
        history: History;
        e: any;
        row: any;
        rowIndex: number;
        props: TableComponentProps<T, U>;
    }) => void;
    onTableChange?: (type: string, remoteProps: TableStateChanged<T>) => void;
    tableInitializer?: {
        tableData: Array<T>;
        tableState?: TableState;
    };
    requestConfigCallback?: (requestConfig: Partial<AxiosRequestConfiguration>) => Promise<{
        tableData: Array<T>;
        tableState?: TableState;
    }>;
    tableDefinition?: TableDefinition<T>;
    writeToUrl?: boolean;
    store?: any;
    classes?: any;
    noDataIndication?: any;
    caption?: any;
    rowStyle?: any;
    rowClasses?: any;
    wrapperClasses?: any;
    extra?: U;
}
export interface TableComponentState<T = any> {
    tableData: Array<T>;
    tableState?: TableState;
}
export declare class TableComponent<T = any, U extends TableComponentProps<T> = TableComponentProps<T>, V extends TableComponentState<T> = TableComponentState<T>> extends AbstractComponent<U, V> {
    static defaultProps: {
        id: string;
        messages: {};
        rowDefinitions: any[];
        remote: boolean;
        tableData: any[];
        tableState: {};
        writeToUrl: boolean;
    };
    constructor(props: any, context: any);
    renderTable(): JSX.Element;
    render(): JSX.Element;
}
export declare class TableComponentTable<T extends TableComponentProps = TableComponentProps, U extends TableComponentState = TableComponentState> extends TableComponent<any, T, U> {
}
export declare function tableLoader<T extends TableComponentProps = TableComponentProps>(remoteTable: any, tableProps?: TableComponentProps): React.ComponentClass<T, any>;
