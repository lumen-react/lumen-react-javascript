/// <reference types="react" />
import { TabProps, TabsProps } from 'react-bootstrap';
import { AbstractComponent } from './abstract-component';
export interface TabDefinition extends TabProps {
    mountOnEnter?: boolean;
    cache?: boolean;
    loader?: JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string> | (() => JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string>);
    contentRenderer: JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string> | (() => JSX.Element | Array<JSX.Element> | string | PromiseLike<JSX.Element | Array<JSX.Element> | string>);
    isDefault?: boolean;
    nullOnDisabled?: boolean;
    preloadTab?: boolean;
    promise?: boolean;
    enabled?: boolean;
    loaderPromise?: boolean;
}
export interface TabsComponentProps extends TabsProps {
    writeToUrl?: boolean;
    defaultOpen?: boolean;
    tabsDefinition: Array<TabDefinition>;
    preloadTabs?: boolean;
}
export interface TabsComponentState {
    key: any;
}
export declare class TabsComponent<T extends TabsComponentProps = TabsComponentProps, U extends TabsComponentState = TabsComponentState> extends AbstractComponent<T, U> {
    static defaultProps: {
        writeToUrl: boolean;
        defaultOpen: boolean;
    };
    private initialRenderers;
    private contentRenderers;
    private loaders;
    private renderedContent;
    private renderedLoader;
    constructor(props: any, context: any);
    componentDidMount(): void;
    componentDidUpdate(): void;
    private handleSelect;
    private loadTab;
    render(): JSX.Element;
    renderTabs(): JSX.Element;
}
