import * as React from 'react';
import { BrowserRouterProps } from "react-router-dom";
export default class AppRouter extends React.Component<BrowserRouterProps> {
    render(): JSX.Element;
}
