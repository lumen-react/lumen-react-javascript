import * as React from 'react';
import * as PropTypes from "prop-types";
import { InjectedIntl } from 'react-intl';
import { match } from "react-router";
import { History, Location } from "history";
export declare abstract class AbstractComponent<T = {}, U = {}> extends React.Component<T, U> {
    protected intl: InjectedIntl;
    protected match: match<any>;
    protected location: Location;
    protected history: History;
    static contextTypes: {
        intl: PropTypes.Validator<any>;
        match: PropTypes.Validator<any>;
        location: PropTypes.Validator<any>;
        history: PropTypes.Validator<any>;
    };
    constructor(props: T, context: any);
    protected prepareState(): U;
    setDelayedState(clearState?: {}): void;
}
export declare abstract class AbstractStatelessComponent<T = {}> extends AbstractComponent<T, {}> {
}
export declare abstract class AbstractSimpleComponent extends AbstractComponent<{}, {}> {
}
export declare abstract class AbstractPureComponent<T = {}, U = {}> extends React.PureComponent<T, U> {
    protected intl: InjectedIntl;
    protected match: match<any>;
    protected location: Location;
    protected history: History;
    static contextTypes: {
        intl: PropTypes.Validator<any>;
        match: PropTypes.Validator<any>;
        location: PropTypes.Validator<any>;
        history: PropTypes.Validator<any>;
    };
    constructor(props: T, context: any);
    protected prepareState(): U;
    setDelayedState(clearState?: {}): void;
}
export declare abstract class AbstractStatelessPureComponent<T> extends AbstractPureComponent<T, {}> {
}
export declare abstract class AbstractSimplePureComponent extends AbstractPureComponent<{}, {}> {
}
