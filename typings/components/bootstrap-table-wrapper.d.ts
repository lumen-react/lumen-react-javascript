/// <reference types="react" />
import { AbstractComponent, TableState } from '..';
export declare class BootstrapTableWrapperComponent<T, U> extends AbstractComponent<T, U> {
    protected tableState: TableState;
    protected lastTableState: TableState;
    constructor(props: any, context: any);
    shouldComponentUpdate(): boolean;
    private isRemote;
    private onTableChangeCallback;
    render(): JSX.Element;
}
export declare class BootstrapTableWrapper extends BootstrapTableWrapperComponent<any, any> {
}
export declare class BootstrapTableWrapperStateless<T> extends BootstrapTableWrapperComponent<T, any> {
}
