import * as React from 'react';
import { AbstractComponent } from "./abstract-component";
export declare enum LabelType {
    SIMPLE = 0,
    SMALL = 1,
    MEDIUM = 2,
    LARGE = 3,
    SEMIFULL = 4,
    FULL = 5
}
export interface LabelProps extends React.HTMLProps<HTMLSpanElement> {
    labelType?: LabelType;
    thumbnailOnly?: boolean;
    avatarProps?: React.HTMLProps<HTMLSpanElement>;
    descriptionProps?: React.HTMLProps<HTMLSpanElement>;
    renderThumbnail?: boolean;
}
export default abstract class AbstractLabel<T extends LabelProps, U> extends AbstractComponent<T, any> {
    static defaultProps: {
        labelType: LabelType;
        thumbnailOnly: boolean;
        renderThumbnail: boolean;
    };
    static sizeKeyMap: {
        [LabelType.SMALL]: string;
        [LabelType.MEDIUM]: string;
        [LabelType.LARGE]: string;
        [LabelType.SEMIFULL]: string;
        [LabelType.FULL]: string;
    };
    protected abstract getName(): string;
    protected abstract hasThumbnail(): boolean;
    protected abstract getThumbnail(): string;
    protected abstract getGlyph(): string;
    protected abstract getSub(): string | JSX.Element;
    protected abstract getContent(): string | JSX.Element;
    protected abstract renderMissingThumbnal(): string | JSX.Element;
    protected getCleanedProps(): Partial<T>;
    private renderThumbnail;
    protected renderSimpleLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    protected renderSmallLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    protected renderMediumLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    protected renderLargeLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    protected renderSemiFullLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    protected renderFullLabel(elementProps: any, avatarProps: any, descriptionProps: any): JSX.Element;
    render(): JSX.Element;
}
