import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { AbstractValidatorRule } from './validator-rule/abstract-validator-rule';
import { NumberValidatorRuleOptions } from './validator-rule/number-validator-rule';
export declare function formValuesTransformer(handler: (formProps: any) => Promise<any>, transformer: (input: {
    [key: string]: any;
}) => {
    [key: string]: any;
}): (values: any) => Promise<any>;
export declare function submitFormError(data: {
    [key: string]: string;
}): void;
export declare function submitFormErrorHandler(handler: (formProps: any) => Promise<any>): (values: any) => Promise<any>;
export default class FormValidator {
    private readonly values;
    private readonly props;
    private validators;
    static make(values: any, props: any, ...validators: Array<Validator>): FormValidator;
    constructor(values: any, props: any, ...validators: Array<Validator>);
    validate(): {
        [key: string]: React.ReactElement<any>;
    };
    asyncValidate(): Promise<Array<ErrorMessage>>;
    custom(name: string, callback: (value: any, name: any, values: any, props: any) => boolean): this;
    email(name: string): FormValidator;
    equalsField(name: string, other: string): FormValidator;
    number(name: string, options?: NumberValidatorRuleOptions): this;
    oneOf(name: string, pool: Array<any>): FormValidator;
    required(name: string): FormValidator;
}
export declare class Validator {
    private _name;
    private _validatorRule;
    constructor(name: string, validatorRule: AbstractValidatorRule);
    name: string;
    validatorRule: AbstractValidatorRule;
    private getMessageValues;
    validate(value: any, values: any, props: any): ErrorMessage;
    asyncValidate(value: any, values: any, props: any): Promise<ErrorMessage>;
}
export declare class ErrorMessage {
    message: FormattedMessage.MessageDescriptor;
    values: any;
    constructor(message: ReactIntl.FormattedMessage.MessageDescriptor, values: any);
}
