/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldDropdownProps extends FormFieldBaseProps {
    data?: any[];
    valueField?: string;
    textField?: string | ((dataItem: any) => string);
    useObjectAsValue?: boolean;
    dropdownProps?: any;
}
export default class FormFieldDropdown extends AbstractFormField<FormFieldDropdownProps> {
    static defaultProps: {
        variant: string;
        placeholder: JSX.Element;
        pure: boolean;
        dropdownProps: {};
        useObjectAsValue: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
