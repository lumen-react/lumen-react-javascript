/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldCalendarProps extends FormFieldBaseProps {
    calendarProps?: any;
}
export default class FormFieldCalendar extends AbstractFormField<FormFieldCalendarProps> {
    static defaultProps: {
        calendarProps: {};
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
