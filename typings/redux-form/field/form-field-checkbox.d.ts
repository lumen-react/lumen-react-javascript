/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldCheckboxProps extends FormFieldBaseProps {
    placeholder?: string;
}
export default class FormFieldCheckbox extends AbstractFormField<FormFieldCheckboxProps> {
    static defaultProps: {
        renderLabel: any;
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
