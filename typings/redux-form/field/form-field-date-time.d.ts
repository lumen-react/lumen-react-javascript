/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldDateTimeProps extends FormFieldBaseProps {
    dateTimeProps?: any;
}
export default class FormFieldDateTime extends AbstractFormField<FormFieldDateTimeProps> {
    static defaultProps: {
        dateTimeProps: {};
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
