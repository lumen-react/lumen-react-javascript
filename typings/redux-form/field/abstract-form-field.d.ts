/// <reference types="react" />
import { BaseFieldProps } from "redux-form";
import { GenericFieldHTMLAttributes } from "redux-form/lib/Field";
import { AbstractStatelessComponent } from "../../components/abstract-component";
export interface FormFieldBaseProps {
    renderLabel?: (field: any) => JSX.Element;
    pure?: boolean;
    label?: string;
}
export declare type FormFieldProps<T extends FormFieldBaseProps> = T & ((BaseFieldProps<GenericFieldHTMLAttributes | BaseFieldProps> & GenericFieldHTMLAttributes) | BaseFieldProps);
declare abstract class AbstractFormField<T extends FormFieldBaseProps> extends AbstractStatelessComponent<FormFieldProps<T>> {
    static defaultProps: {
        pure: boolean;
    };
    protected abstract renderInput(field: any): JSX.Element;
    protected getInternalFormFieldRenderer(): string;
    render(): JSX.Element;
}
export default AbstractFormField;
