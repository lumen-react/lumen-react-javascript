/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from "./abstract-form-field";
export interface FormFieldInputProps extends FormFieldBaseProps {
    placeholder?: string;
    type?: string;
}
export default class FormFieldInput extends AbstractFormField<FormFieldInputProps> {
    static defaultProps: {
        type: string;
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
