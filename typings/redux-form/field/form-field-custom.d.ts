/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldCustomProps extends FormFieldBaseProps {
    children: (field: any) => JSX.Element;
}
export default class FormFieldCustom extends AbstractFormField<FormFieldCustomProps> {
    protected renderInput(field: any): JSX.Element;
}
