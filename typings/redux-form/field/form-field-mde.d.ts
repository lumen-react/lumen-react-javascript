/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldMdeProps extends FormFieldBaseProps {
    textAreaStyle?: CSSStyleDeclaration;
    buttonStyle?: CSSStyleDeclaration;
    buttonContainerStyle?: CSSStyleDeclaration;
    iconSize?: number;
}
export default class FormFieldMde extends AbstractFormField<FormFieldMdeProps> {
    protected renderInput(field: any): JSX.Element;
}
