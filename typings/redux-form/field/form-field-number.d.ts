/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldNumberProps extends FormFieldBaseProps {
    numberProps?: any;
}
export default class FormFieldNumber extends AbstractFormField<FormFieldNumberProps> {
    static defaultProps: {
        numberProps: {};
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
