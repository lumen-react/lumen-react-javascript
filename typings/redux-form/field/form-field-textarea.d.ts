/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldTextareaProps extends FormFieldBaseProps {
    placeholder?: string;
}
export default class FormFieldTextarea extends AbstractFormField<FormFieldTextareaProps> {
    static defaultProps: {
        pure: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
