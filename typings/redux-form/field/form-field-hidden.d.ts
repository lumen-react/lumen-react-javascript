/// <reference types="react" />
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldHiddenProps extends FormFieldBaseProps {
}
export default class FormFieldHidden extends AbstractFormField<FormFieldHiddenProps> {
    static defaultProps: {
        renderLabel: any;
        pure: boolean;
    };
    protected getInternalFormFieldRenderer(): string;
    protected renderInput(field: any): JSX.Element;
}
