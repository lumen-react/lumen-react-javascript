import { SyntheticEvent } from 'react';
import AbstractFormField, { FormFieldBaseProps } from './abstract-form-field';
export interface FormFieldMultiselectProps extends FormFieldBaseProps {
    data?: any[];
    valueField?: string;
    textField?: string | ((dataItem: any) => string);
    useObjectAsValue?: boolean;
    multiselectProps?: any;
    onChange?: (dataItems?: any[], metadata?: {
        dataItem: any;
        action: 'insert' | 'remove';
        originalEvent: SyntheticEvent<any>;
        lastValue?: any[];
        searchTerm?: string;
    }) => void;
}
export default class FormFieldMultiselect extends AbstractFormField<FormFieldMultiselectProps> {
    private newlyGeneratedId;
    static defaultProps: {
        variant: string;
        placeholder: JSX.Element;
        pure: boolean;
        multiselectProps: {};
        useObjectAsValue: boolean;
    };
    protected renderInput(field: any): JSX.Element;
}
