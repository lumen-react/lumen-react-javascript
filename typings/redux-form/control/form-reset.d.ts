import * as React from 'react';
import { InjectedFormProps } from "redux-form";
export interface FormResetProps {
    label?: string | JSX.Element;
}
export interface FormResetPropsImpl extends FormResetProps, InjectedFormProps<{}> {
}
export default class FormReset extends React.Component<FormResetPropsImpl> {
    static defaultProps: {
        label: JSX.Element;
    };
    render(): JSX.Element;
}
