import * as React from 'react';
import { InjectedFormProps } from "redux-form";
import { History } from "history";
export interface FormBackProps {
    history: History;
    label?: string | JSX.Element;
}
export interface FormBackPropsImpl extends FormBackProps, InjectedFormProps<{}> {
}
export default class FormBack extends React.Component<FormBackPropsImpl> {
    static defaultProps: {
        label: JSX.Element;
    };
    render(): JSX.Element;
}
