import * as React from 'react';
import { FormCancelProps } from "./form-cancel";
import { FormSubmitProps } from "./form-submit";
import { FormSecondaryProps } from "./form-secondary";
import { FormResetProps } from "./form-reset";
import { InjectedFormProps } from "redux-form";
import { FormBackProps } from "./form-back";
export interface FormControlProps extends InjectedFormProps<{}> {
    submitControl?: FormSubmitProps | boolean;
    secondaryControl?: FormSecondaryProps | boolean;
    resetControl?: FormResetProps | boolean;
    cancelControl?: FormCancelProps | boolean;
    backControl?: FormBackProps | boolean;
    showError?: ((error: string) => any) | boolean;
}
export default class FormControl extends React.Component<FormControlProps> {
    static defaultProps: {
        submitControl: boolean;
        secondaryControl: boolean;
        resetControl: boolean;
        cancelControl: boolean;
        backControl: boolean;
        showError: boolean;
    };
    private renderError;
    render(): JSX.Element;
}
