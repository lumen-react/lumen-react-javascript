import * as React from 'react';
import { InjectedFormProps } from "redux-form";
export interface FormCancelProps {
    to?: string;
    label?: string | JSX.Element;
}
export interface FormCancelPropsImpl extends FormCancelProps, InjectedFormProps<{}> {
}
export default class FormCancel extends React.Component<FormCancelPropsImpl> {
    static defaultProps: {
        to: string;
        label: JSX.Element;
    };
    render(): JSX.Element;
}
