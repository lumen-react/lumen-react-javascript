import * as React from 'react';
import { InjectedFormProps } from "redux-form";
export interface FormSubmitProps {
    label?: string | JSX.Element;
}
export interface FormSubmitPropsImpl extends FormSubmitProps, InjectedFormProps<{}> {
}
export default class FormSubmit extends React.Component<FormSubmitPropsImpl> {
    static defaultProps: {
        label: JSX.Element;
    };
    render(): JSX.Element;
}
