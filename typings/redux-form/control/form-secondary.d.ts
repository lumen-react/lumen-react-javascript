import * as React from 'react';
import { InjectedFormProps } from "redux-form";
export interface FormSecondaryProps {
    onClick?: () => void;
    confirm?: string;
    label?: string | JSX.Element;
    disabled?: boolean;
}
export interface FormSecondaryPropsImpl extends FormSecondaryProps, InjectedFormProps<{}> {
}
export default class FormSecondary extends React.Component<FormSecondaryPropsImpl> {
    static defaultProps: {
        label: JSX.Element;
        disabled: boolean;
    };
    render(): JSX.Element;
}
