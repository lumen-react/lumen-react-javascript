/// <reference types="react-intl" />
import { AbstractValidatorRule } from './abstract-validator-rule';
export declare class EmailValidatorRule extends AbstractValidatorRule {
    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor;
    isValid(value: any): boolean;
}
