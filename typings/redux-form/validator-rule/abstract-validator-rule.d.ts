import { FormattedMessage } from 'react-intl';
export declare abstract class AbstractValidatorRule {
    private _message;
    constructor(message?: FormattedMessage.MessageDescriptor);
    message: FormattedMessage.MessageDescriptor;
    protected abstract getDefaultMessage(): FormattedMessage.MessageDescriptor;
    getMessageValues(): {};
    abstract isValid(value: any, name: any, values: any, props: any): boolean;
}
