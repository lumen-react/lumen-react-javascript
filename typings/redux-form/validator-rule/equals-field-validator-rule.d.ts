import { FormattedMessage } from 'react-intl';
import { AbstractValidatorRule } from './abstract-validator-rule';
export declare class EqualsFieldValidatorRule extends AbstractValidatorRule {
    private otherField;
    constructor(otherField: string, message?: FormattedMessage.MessageDescriptor);
    protected getDefaultMessage(): FormattedMessage.MessageDescriptor;
    getMessageValues(): {
        otherField: string;
    };
    isValid(value: any, name: any, values: any, props: any): boolean;
}
