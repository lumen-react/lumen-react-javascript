/// <reference types="react-intl" />
import { AbstractValidatorRule } from './abstract-validator-rule';
export declare class CustomRule extends AbstractValidatorRule {
    private callback;
    constructor(callback: any);
    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor;
    isValid(value: any, name: any, values: any, props: any): boolean;
}
