/// <reference types="react-intl" />
import { AbstractValidatorRule } from './abstract-validator-rule';
export declare class OneOfValidatorRule extends AbstractValidatorRule {
    private pool;
    constructor(pool: Array<any>);
    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor;
    isValid(value: any): boolean;
    getMessageValues(): {
        values: string;
    };
}
