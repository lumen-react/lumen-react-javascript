/// <reference types="react-intl" />
import { AbstractValidatorRule } from './abstract-validator-rule';
export interface NumberValidatorRuleOptions {
    strict?: boolean;
    integer?: boolean;
    min?: number;
    max?: number;
}
export declare class NumberValidatorRule extends AbstractValidatorRule {
    private options;
    constructor(options?: NumberValidatorRuleOptions);
    protected getDefaultMessage(): ReactIntl.FormattedMessage.MessageDescriptor;
    isValid(value: any): boolean;
}
