import { FormattedMessage } from 'react-intl';
import { AbstractValidatorRule } from './abstract-validator-rule';
export declare class RequiredValidatorRule extends AbstractValidatorRule {
    protected getDefaultMessage(): FormattedMessage.MessageDescriptor;
    isValid(value: any, name: any, values: any, props: any): boolean;
}
