import { Action, Dispatch } from 'redux';
import { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ConfigProps } from 'redux-form';
import { ConnectLoaderOptions } from '../hoc/connect-loader';
import { AjaxLoaderOptions } from '../hoc/ajax-loader';
import { EventLoaderOptions, TableState } from '..';
import { TableFilterOperator } from '../utils/bootstrap-table-utils';
export interface GenericCallbackOptions<T, U> {
    type?: T;
    empty?: boolean;
    emptyValue?: any;
    promise?: (promise: Promise<U>) => any;
    then?: (data: U) => any;
    catch?: (e: any) => any;
    requestConfig?: AxiosRequestConfiguration;
    meta?: any;
}
export interface BaseAction<T> extends Action {
    payload: T;
    meta: any;
}
export interface ApiResponseHeaders {
    user?: number;
    count?: number;
    unscoped?: Array<string>;
    filter?: Array<Array<string>>;
    limit?: number;
    offset?: number;
    order?: {
        [key: string]: 'desc' | 'asc';
    };
    with?: Array<string>;
    expired?: number;
}
export interface ExtendedResponseData<T> {
    data: T;
    apiResponseHeaders: ApiResponseHeaders;
    allHeaders: {
        [key: string]: string;
    };
    code: number;
}
export declare function parseHeadersToApiResponseHeaders(headers: {
    [key: string]: string;
}): ApiResponseHeaders;
export declare function callDispatcher<T, U extends GenericCallbackOptions<V, T>, V>(dispatch: Dispatch<Promise<T>>, defaultAction: V, promiseCallback: Promise<T>, options?: U): any;
export declare type AxiosRequestConfiguration = {
    axios?: AxiosInstance;
    with?: string | Array<string>;
    filter?: string | Array<string | Array<string>>;
    queryString?: string | {
        [key: string]: any;
    };
    limit?: number;
    offset?: number;
    order?: string | {
        [key: string]: 'desc' | 'asc';
    };
    skipDateConversion?: boolean;
    bypassGlobalResponseHandlers?: (status: number, response: any, expired: boolean) => boolean | boolean | Array<number>;
} & AxiosRequestConfig;
export declare function getQueryString(queryString: string | {
    [key: string]: any;
}): string;
export declare function getUrl(base: string, requestConfig: AxiosRequestConfiguration): string;
export declare function writeRequestConfigToUrl(name: string, config: Partial<AxiosRequestConfiguration>): void;
export declare function readRequestConfigFromUrl(name: string, defaultConfig?: Partial<AxiosRequestConfiguration>): Partial<AxiosRequestConfiguration>;
export declare const operatorConversion: {
    [TableFilterOperator.LIKE]: string;
    [TableFilterOperator.EQ]: string;
    [TableFilterOperator.NE]: string;
    [TableFilterOperator.GT]: string;
    [TableFilterOperator.GE]: string;
    [TableFilterOperator.LT]: string;
    [TableFilterOperator.LE]: string;
};
export declare function apiResponseHeadersToTableState(apiResponseHeaders: ApiResponseHeaders): TableState;
export declare function requestConfigMerge(base: Partial<AxiosRequestConfiguration>, ...merges: Array<Partial<AxiosRequestConfiguration>>): any;
export declare function apiResponseHeadersToRequestConfig(apiResponseHeaders: ApiResponseHeaders, queryString?: any): Partial<AxiosRequestConfiguration>;
export declare function requestConfigToTableState(requestConfig: AxiosRequestConfiguration): TableState;
export declare function tableStateToRequestConfig(tableState: TableState, queryString?: any, cancelToken?: string): Partial<AxiosRequestConfiguration>;
export interface ConnectorOptions {
    formConfigProps?: ConfigProps<any, any>;
    mapStateToProps?: any;
    mapDispatchToProps?: any;
    mapOptions?: any;
    connectLoaderOptions?: ConnectLoaderOptions;
    ajaxLoaderOptions?: AjaxLoaderOptions;
    eventLoaderOptions?: EventLoaderOptions;
}
export declare function connector(component: any, connectorOptions?: ConnectorOptions): any;
export declare function connect(component: any, mapStateToProps?: any, mapDispatchToProps?: any, dispatcherLoaders?: any, ajaxLoaderPromises?: any): any;
export declare function form(component: any, formConfigProps: any, mapStateToProps?: any, mapDispatchToProps?: any, dispatcherLoaders?: any, ajaxLoaderPromises?: any): any;
export declare function simpleForm(component: any, formConfigProps: any, ajaxLoaderPromises?: any): any;
export declare function simpleLoader(component: any, ajaxLoaderPromises?: any): any;
