export declare function removeUndefinedKeys(object: {
    [key: string]: any;
}): {
    [key: string]: any;
};
export declare function swap(object: {
    [key: string]: any;
}): {
    [key: string]: any;
};
