/// <reference types="react" />
import { StorageDriverId } from './storage-utils';
export interface BootOptions {
    App: any;
    languages: {
        supported: {
            [key: string]: Array<string>;
        };
        default: string;
    };
    localeCallback?: Promise<string> | string;
    appName?: string;
    className?: string;
    module?: any;
    reducers?: any;
    postBoot?: (locale?: string) => void;
    ErrorBoundary?: any;
    globalLoader?: ((content?: any) => JSX.Element) | JSX.Element | string;
    preRenderCallback?: () => void;
    postRenderCallback?: () => void;
    hotLoaderPath?: string;
    defaultStorageDriverId?: StorageDriverId;
    authStorageDriverId?: StorageDriverId;
}
export declare function run(Entry: any, options?: Partial<BootOptions>): void;
export declare function resolveLoader(content?: any): any;
