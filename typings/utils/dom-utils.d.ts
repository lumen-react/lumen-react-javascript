import { History } from 'history';
export declare function goto(href: string, target?: string | History, e?: any): boolean;
export declare function gotoAlt(href: string, blankTarget?: string | History, e?: any): boolean;
