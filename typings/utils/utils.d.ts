export declare function classNames(...classMap: Array<string | {
    [key: string]: boolean | string;
}>): string;
