export declare enum StorageDriverId {
    COOKIES = 0,
    LOCAL_STORAGE = 1,
    SESSION_STORAGE = 2,
    WINDOW = 3
}
export default class StorageUtils {
    static defaultStorageDriverId: StorageDriverId;
    private static getStorageDriver;
    static setWithOptions<T = any>(key: string, value: any, options?: any, driverId?: StorageDriverId): boolean;
    static set<T = any>(key: string, value: any, driverId?: StorageDriverId): boolean;
    static get<T = any>(key: string, driverId?: StorageDriverId): T;
    static removeWithOptions(key: string, options?: any, driverId?: StorageDriverId): boolean;
    static remove(key: string, driverId?: StorageDriverId): boolean;
    static clear(driverId?: StorageDriverId): boolean;
    static all(driverId?: StorageDriverId): {
        [key: string]: any;
    };
}
