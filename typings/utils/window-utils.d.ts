/// <reference types="react" />
export declare const ON_KEY_DOWN = "onKeyDown";
export declare const ON_KEY_UP = "onKeyUp";
export interface Window {
    storedCancelTokens?: Array<any>;
    defaultTableDateFormatter?: (date: Date) => string;
    keyRegister?: {
        [key: string]: boolean;
    };
    globalLoader?: ((content?: any) => JSX.Element) | JSX.Element | string;
    storage?: {};
}
export declare function setGlobal<T extends Window = Window>(key: keyof T, value: any): void;
export declare function getGlobal<T extends Window = Window>(key?: keyof T, defaultValue?: any): any;
export declare const SHIFT_KEY = -1;
export declare const ALT_KEY = -2;
export declare const CTRL_KEY = -3;
export declare const META_KEY = -4;
export declare const OPT_KEY = -5;
export declare function isKeyDown(key: any): any;
export declare function setupKeyRegister(): void;
export declare function getKeyRegister(key: any): any;
export declare function setKeyRegister(key: any, value: boolean): void;
export declare function registerKey(e: any, down: boolean): void;
