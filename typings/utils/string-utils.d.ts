export declare function makeId(length?: number): string;
export declare function lcfirst(string: string): string;
export declare function ucfirst(string: string): string;
export declare function snakeCaseToNormal(string: string): string;
