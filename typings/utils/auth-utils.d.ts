import { StorageDriverId } from "./storage-utils";
export default class AuthUtils {
    static storageDriverId: StorageDriverId;
    private static token;
    private static user;
    static boot(storageDriverId: any): void;
    static login(token: any, user: any): void;
    static logout(): void;
    static isLoggedIn(): boolean;
    static authenticatedUser<T>(): T;
    static getToken(): string;
}
