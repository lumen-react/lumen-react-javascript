/// <reference types="react" />
import { InjectedIntl, Messages } from 'react-intl';
export declare const paginationOptions: {
    paginationSize: number;
    sizePerPage: number;
    pageStartIndex: number;
    alwaysShowAllBtns: boolean;
    withFirstAndLast: boolean;
    hideSizePerPage: boolean;
    hidePageListOnlyOnePage: boolean;
    firstPageText: string;
    prePageText: string;
    nextPageText: string;
    lastPageText: string;
    nextPageTitle: string;
    prePageTitle: string;
    firstPageTitle: string;
    lastPageTitle: string;
};
export declare function messagesToColumns(messages: Messages, intl: InjectedIntl, keyMap?: any): any[];
export declare enum TableDefinitionType {
    TEXT = "TEXT",
    SELECT = "SELECT",
    NUMBER = "NUMBER",
    DATE = "DATE"
}
export declare enum TableFilterOperator {
    LIKE = "LIKE",
    EQ = "=",
    NE = "!=",
    GT = ">",
    GE = ">=",
    LT = "<",
    LE = "<="
}
export interface TableRowDefinition<T> {
    id: string;
    text?: string;
    dataField?: string;
    formatter?: (cell: any, row: T, rowIndex: number, extraData: any) => string | JSX.Element | Array<JSX.Element>;
    type?: TableDefinitionType;
    filterValue?: (cell: any, row: T) => any;
    filterOptions?: {
        [key: string]: any;
    };
    sort?: boolean;
    onSort?: (sortField: string, sortOrder: 'asc' | 'desc') => void;
}
export interface TableState {
    totalSize?: number;
    page?: number;
    sizePerPage?: number;
    sortField?: string;
    sortOrder?: 'asc' | 'desc';
    filters?: {
        [key: string]: {
            filterVal: any;
            comparator: TableFilterOperator;
        };
    };
}
export interface TableStateChanged<T> extends TableState {
    data?: Array<T>;
    cellEdit?: {
        rowId?: any;
        dataField?: any;
        newValue?: any;
    };
}
export interface TableDefinition<T> {
    id: string;
    rowDefinitions: Array<TableRowDefinition<T>>;
    state?: TableState;
    remote?: boolean;
    keyField?: string;
    onTableChange?: (type: 'filter' | 'pagination' | 'sort' | 'cellEdit', newState: TableStateChanged<T>) => void;
    paginationOptions?: {
        page?: number;
        sizePerPage?: number;
        paginationSize?: number;
        pageStartIndex?: number;
        alwaysShowAllBtns?: boolean;
        withFirstAndLast?: boolean;
        hideSizePerPage?: boolean;
        hidePageListOnlyOnePage?: boolean;
        firstPageText?: string;
        prePageText?: string;
        nextPageText?: string;
        lastPageText?: string;
        nextPageTitle?: string;
        prePageTitle?: string;
        firstPageTitle?: string;
        lastPageTitle?: string;
        showTotal?: boolean;
        paginationTotalRenderer?: (from: number, to: number, size: number) => JSX.Element;
        sizePerPageList?: Array<{
            text: string;
            value: number;
        }>;
        objectPluralMessageKey?: string;
        totalSize?: number;
    };
    dateFormatter?: (date: Date) => string | JSX.Element;
}
export declare function tableColumnSelectArrayToObject(array: Array<any>, value?: string, key?: string): {};
export declare function createTableProps<T>(data: Array<T>, tableDefinition: TableDefinition<T>, messages: Messages, intl: InjectedIntl, useWrapper?: boolean): {
    [key: string]: any;
};
export declare function getRowOptions<T>(tableDefinition: TableDefinition<T>, rowDefinition: TableRowDefinition<T>, messages: Messages, intl: InjectedIntl, isLocal: boolean, useWrapper: boolean): TableRowDefinition<T>;
export declare function defaultFilterTypeObjectCreator<T>(tableDefinition: TableDefinition<T>, rowDefinition: TableRowDefinition<T>, filterOptions: {
    [key: string]: any;
}, messages: Messages, intl: InjectedIntl, isLocal: boolean): any;
export declare function mergeTableState(oldState: TableState, newState: TableState, remote: boolean): {
    [key: string]: any;
};
export declare function writeTableStateToUrl(name: string, tableState: TableState): void;
export declare function readTableStateFromUrl(name: string, defaultTableState?: TableState): Partial<TableState>;
export declare function getDefaultSizePerPageList(): Array<{
    text: string;
    value: number;
}>;
export declare function resourcesToOptions(data: Array<any>, label?: string, key?: string, labelCallback?: (label: string, row: any) => string, keyCallback?: (key: string, row: any) => string): {};
