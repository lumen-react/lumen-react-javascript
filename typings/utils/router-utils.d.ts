export declare function writeToHash(object: any, clear?: boolean): void;
export declare function readFromHash(key?: string, defaultValue?: any): any;
