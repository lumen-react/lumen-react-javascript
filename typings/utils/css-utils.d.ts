export default class CssUtils {
    private static cached;
    static get(selector: any): any;
    static set(selector: any, rule: any): void;
    static rgb2hex(rgb: any): any;
    static hex2rgb(hex: any): any;
}
