export declare function addFullListener<T = any>(event: string, listener: (e: CustomEvent<T>) => void, options?: boolean | AddEventListenerOptions): void;
export declare function addListener<T = any>(event: string, listener: (e: T) => void, options?: boolean | AddEventListenerOptions): void;
export declare function dispatch<T = {
    [key: string]: any;
}>(event: string, detail?: T): void;
export declare function removeListener(event: string, listener: any, options?: boolean | AddEventListenerOptions): void;
