import { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse, Cancel } from 'axios';
import { History } from 'history';
export declare function createCancelToken(id: string, config?: any): any;
export declare function makeCancelToken(): any;
export declare function storeCancelToken(id: string, source: any, config?: any): void;
export declare function cancelRequest(tokenOrId: string, message?: string): void;
export interface AxiosRequestConfigExtended extends AxiosRequestConfig {
    onResponse?: {
        [code: number]: () => void;
    };
    onExpired?: () => void;
    skipCancel?: boolean;
    cancelTokenId?: string;
    cancelMessage?: string;
    globalHandlers?: {
        onRequest?: (config: AxiosRequestConfigExtended, originalConfig: AxiosRequestConfigExtended) => void;
        onRequestError?: (error: AxiosError, originalConfig: AxiosRequestConfigExtended) => void;
        onResponse?: (response: AxiosResponse, originalConfig: AxiosRequestConfigExtended) => void;
        onResponseError?: (error: AxiosError | Cancel, originalConfig: AxiosRequestConfigExtended) => void;
    };
}
export declare function bypassGlobalResponseHandlers(bypassGlobalResponseHandlersOption: (status: number, response: any, expired: boolean) => boolean | boolean | Array<number>, status: number, response: any, expired: boolean): boolean | number[];
export declare function setupAxiosInstance(instance: AxiosInstance, config?: AxiosRequestConfigExtended): void;
export declare function redirectOnFailConnect(history: History, redirects?: {
    401: (history: History<any>) => void;
    403: (history: History<any>) => void;
    404: (history: History<any>) => void;
}): {
    catch: (e: any) => any;
};
export declare function redirectOnFail(history: History, redirects?: {
    401: (history: History<any>) => void;
    403: (history: History<any>) => void;
    404: (history: History<any>) => void;
}): (e: any) => any;
export declare function backOnSuccessConnect(history: History, to?: any): {
    then: (r: any) => any;
};
export declare function backOnSuccess(history: History, to?: any): (r: any) => any;
