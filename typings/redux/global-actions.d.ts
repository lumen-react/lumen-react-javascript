import { ActionCreator, Dispatch } from "redux";
export declare enum GlobalActionType {
    REFRESH_STORE = "REFRESH_STORE"
}
export interface GlobalAction {
    type: GlobalActionType;
}
export declare type GlobalActionCallback = () => GlobalAction;
export declare const refreshStore: ActionCreator<GlobalActionCallback>;
export declare function refreshStoreDispatcherBinder(dispatch: Dispatch<GlobalAction>): {
    refreshStoreDispatcher: GlobalActionCallback;
};
export interface RefreshStoreDispatcher {
    refreshStoreDispatcher: GlobalActionCallback;
}
